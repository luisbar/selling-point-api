FROM node:16.5.0-alpine
WORKDIR /app
EXPOSE 3000
COPY . .
ENV SKIP_NESTGRAPHQL_AND_ERD_GENERATION true
RUN npm i
CMD ["npm", "run", "start:dev"]