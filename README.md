## Howw to run?

1. Clone the repository with selling-point-db submodule
```bash
git clone --recursive  git@gitlab.com:luisbar/selling-point-api.git
```

2. Create `.env` file using the `.env.template` as reference

3. Install dependencies
```bash
npm i
```

4. Run the project
```bash
npm run start:dev
```