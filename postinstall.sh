#!/bin/sh

if [ "$SKIP_NESTGRAPHQL_AND_ERD_GENERATION" != "true" ];  then
    echo "Generating nestgraphql, erd and prisma code"
    npx prisma generate --schema ./selling-point-db/schema.nestgraphql.erd.prisma
    echo "Nestgraphql, erd and prisma have been generated"
fi

if [ "$SKIP_PRISMA_CLIENT_GENERATION" != "true" ]; then
    echo "Generating prisma client code"
    npx prisma generate --schema ./selling-point-db/schema.prisma
    echo "Prisma client has been generated"
fi