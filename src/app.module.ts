import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { PrismaAbility } from '@casl/prisma';
import { ConfigModule } from '@nestjs/config';
import * as joi from 'joi';
import * as path from 'path';
import { CommonModule } from './common/common.module';
import { ResolversModule } from './resolvers/resolver.module';
import { I18nModule, I18nJsonParser } from 'nestjs-i18n';
import { APP_FILTER, APP_PIPE } from '@nestjs/core';
import { GenericExceptionFilter } from './common/exception/generic.exception.filter';
import * as ramda from 'ramda';
import { BodyValidationPipe } from './common/validation/body.validation.pipe';

@Module({
  imports: [
    GraphQLModule.forRoot({
      cors: true,
      introspection: process.env.NODE_ENV !== 'production',
      autoTransformHttpErrors: false,
      debug: process.env.NODE_ENV !== 'production',
      playground: process.env.NODE_ENV !== 'production',
      path: '/api/graphql',
      autoSchemaFile: path.join(process.cwd(), './schema.gql'),
      context: ({ req, res }) => {
        const encodedToken = ramda.pathOr('e30=.e30=.e30=', ['authorization'], req.headers).replace('Bearer', '').trim();
        const decodedToken = JSON.parse(atob(encodedToken.split('.')[1]));
        const userId = decodedToken['userId'];
        const ability = new PrismaAbility(ramda.pathOr({}, ['scope'], decodedToken));

        return {
          ability,
          userId,
          language: req.headers['accept-language'],
        };
      },
    }),
    ConfigModule.forRoot({
      ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
      isGlobal: true,
      validationSchema: joi.object({
        DATABASE_URL: joi.string().required(),
        NODE_ENV: joi.string().required(),
      }),
    }),
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'),
        watch: true,
      },
    }),
    CommonModule,
    ResolversModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: GenericExceptionFilter,
    },
    {
      provide: APP_PIPE,
      useClass: BodyValidationPipe,
    },
  ],
})

// TODO: Nexus has a plugin for the datetime and json scalar types, add it
// TODO: Check why the context of the logger is the same for all modules
// TODO: Check why when a required field is missing, the error is not manage by the pipe

export class AppModule {}
