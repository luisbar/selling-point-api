import { Plugin } from '@nestjs/graphql';
import {
  ApolloServerPlugin,
  BaseContext,
  GraphQLRequestContextWillSendResponse,
  GraphQLRequestListener,
} from 'apollo-server-plugin-base';

@Plugin()
export class ErrorFormatter implements ApolloServerPlugin {

  async requestDidStart(): Promise<GraphQLRequestListener> {
    return {
      async willSendResponse(requestContext: GraphQLRequestContextWillSendResponse<BaseContext>): Promise<void> {
        if (!requestContext?.response?.errors ||
            requestContext?.response?.errors[0].extensions?.exception.stacktrace[0].includes('GraphQLError')) return;
        
        const { response, name, stacktrace, ...others } = requestContext?.response?.errors?.[0]?.extensions?.exception;

        requestContext.response.http.status = others.status;
        requestContext.response.errors = [
          {
            ...others,
          },
        ];
        
      },
    };
  }
}