import { Module } from '@nestjs/common';
import { ErrorFormatter } from './apollo/error.formatter';
import { LoggerService } from './logger/logger.service';
import { PrismaService } from './prisma/prisma.service';

@Module({
  providers: [
    PrismaService,
    LoggerService,
    ErrorFormatter,
  ],
  exports: [
    PrismaService,
    LoggerService,
  ],
})
export class CommonModule {}
