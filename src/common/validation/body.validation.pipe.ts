import { PipeTransform, Injectable, ArgumentMetadata, HttpStatus, ValidationError } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { ApiException } from '../exception/api.exception';

@Injectable()
export class BodyValidationPipe implements PipeTransform<any> {

  getFields(errors: ValidationError[]) {
    let fields = {};
    errors
    .forEach((error) => {
      const fieldErrors = new RegExp(/"constraints":{.*}/).exec(JSON.stringify(error));
      const fieldName = Array
      .from(JSON.stringify(error).matchAll(/"property":[\s\S]*?(?=,)/g))
      .map((item) => item[0].replace(/"property":"|"/g, ''))
      .join('.');
      
      Object
      .values(
        JSON
        .parse(
          JSON.parse(
            JSON.stringify(`{${fieldErrors[0].replace('}}]}', '}}')}`)
          )
        ).constraints
      )
      .forEach((error: any) => {
        fields[fieldName] = error;
      });
    });

    return fields;
  }

  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) return value;

    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length) throw new ApiException(
      'Bad Request',
      HttpStatus.BAD_REQUEST,
      'errors.body.invalid',
      this.getFields(errors),
    );

    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}