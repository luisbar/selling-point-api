import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateManyCreatedByInput } from './category-create-many-created-by.input';

@InputType()
export class CategoryCreateManyCreatedByInputEnvelope {

    @Field(() => [CategoryCreateManyCreatedByInput], {nullable:false})
    data!: Array<CategoryCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
