import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateManyUpdatedByInput } from './category-create-many-updated-by.input';

@InputType()
export class CategoryCreateManyUpdatedByInputEnvelope {

    @Field(() => [CategoryCreateManyUpdatedByInput], {nullable:false})
    data!: Array<CategoryCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
