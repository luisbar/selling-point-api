import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateWithoutCreatedByInput } from './category-create-without-created-by.input';
import { CategoryCreateOrConnectWithoutCreatedByInput } from './category-create-or-connect-without-created-by.input';
import { CategoryCreateManyCreatedByInputEnvelope } from './category-create-many-created-by-input-envelope.input';
import { CategoryWhereUniqueInput } from './category-where-unique.input';

@InputType()
export class CategoryCreateNestedManyWithoutCreatedByInput {

    @Field(() => [CategoryCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<CategoryCreateWithoutCreatedByInput>;

    @Field(() => [CategoryCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<CategoryCreateOrConnectWithoutCreatedByInput>;

    @Field(() => CategoryCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: CategoryCreateManyCreatedByInputEnvelope;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    connect?: Array<CategoryWhereUniqueInput>;
}
