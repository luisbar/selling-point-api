import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateWithoutUpdatedByInput } from './category-create-without-updated-by.input';
import { CategoryCreateOrConnectWithoutUpdatedByInput } from './category-create-or-connect-without-updated-by.input';
import { CategoryCreateManyUpdatedByInputEnvelope } from './category-create-many-updated-by-input-envelope.input';
import { CategoryWhereUniqueInput } from './category-where-unique.input';

@InputType()
export class CategoryCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [CategoryCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<CategoryCreateWithoutUpdatedByInput>;

    @Field(() => [CategoryCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<CategoryCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => CategoryCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: CategoryCreateManyUpdatedByInputEnvelope;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    connect?: Array<CategoryWhereUniqueInput>;
}
