import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateWithoutProductInput } from './category-create-without-product.input';
import { CategoryCreateOrConnectWithoutProductInput } from './category-create-or-connect-without-product.input';
import { CategoryWhereUniqueInput } from './category-where-unique.input';

@InputType()
export class CategoryCreateNestedOneWithoutProductInput {

    @Field(() => CategoryCreateWithoutProductInput, {nullable:true})
    create?: CategoryCreateWithoutProductInput;

    @Field(() => CategoryCreateOrConnectWithoutProductInput, {nullable:true})
    connectOrCreate?: CategoryCreateOrConnectWithoutProductInput;

    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    connect?: CategoryWhereUniqueInput;
}
