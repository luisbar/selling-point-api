import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryCreateWithoutCreatedByInput } from './category-create-without-created-by.input';

@InputType()
export class CategoryCreateOrConnectWithoutCreatedByInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryCreateWithoutCreatedByInput, {nullable:false})
    create!: CategoryCreateWithoutCreatedByInput;
}
