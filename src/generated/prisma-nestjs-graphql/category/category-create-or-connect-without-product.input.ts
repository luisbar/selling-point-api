import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryCreateWithoutProductInput } from './category-create-without-product.input';

@InputType()
export class CategoryCreateOrConnectWithoutProductInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryCreateWithoutProductInput, {nullable:false})
    create!: CategoryCreateWithoutProductInput;
}
