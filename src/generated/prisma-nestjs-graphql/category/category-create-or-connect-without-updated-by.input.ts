import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryCreateWithoutUpdatedByInput } from './category-create-without-updated-by.input';

@InputType()
export class CategoryCreateOrConnectWithoutUpdatedByInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryCreateWithoutUpdatedByInput, {nullable:false})
    create!: CategoryCreateWithoutUpdatedByInput;
}
