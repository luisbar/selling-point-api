import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutUpdatedByCategoryInput } from '../user/user-create-nested-one-without-updated-by-category.input';
import { ProductCreateNestedManyWithoutCategoryInput } from '../product/product-create-nested-many-without-category.input';

@InputType()
export class CategoryCreateWithoutCreatedByInput {

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutUpdatedByCategoryInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByCategoryInput;

    @Field(() => ProductCreateNestedManyWithoutCategoryInput, {nullable:true})
    product?: ProductCreateNestedManyWithoutCategoryInput;
}
