import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByCategoryInput } from '../user/user-create-nested-one-without-created-by-category.input';
import { UserCreateNestedOneWithoutUpdatedByCategoryInput } from '../user/user-create-nested-one-without-updated-by-category.input';

@InputType()
export class CategoryCreateWithoutProductInput {

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByCategoryInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByCategoryInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedByCategoryInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByCategoryInput;
}
