import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByCategoryInput } from '../user/user-create-nested-one-without-created-by-category.input';
import { ProductCreateNestedManyWithoutCategoryInput } from '../product/product-create-nested-many-without-category.input';

@InputType()
export class CategoryCreateWithoutUpdatedByInput {

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByCategoryInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByCategoryInput;

    @Field(() => ProductCreateNestedManyWithoutCategoryInput, {nullable:true})
    product?: ProductCreateNestedManyWithoutCategoryInput;
}
