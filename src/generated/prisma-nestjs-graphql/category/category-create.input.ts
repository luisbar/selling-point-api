import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByCategoryInput } from '../user/user-create-nested-one-without-created-by-category.input';
import { UserCreateNestedOneWithoutUpdatedByCategoryInput } from '../user/user-create-nested-one-without-updated-by-category.input';
import { ProductCreateNestedManyWithoutCategoryInput } from '../product/product-create-nested-many-without-category.input';
import { IsString, Matches, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

@InputType()
export class CategoryCreateInput {

    @Field(() => String, {nullable:false})
    @IsString({ message: 'errors.category.name.is.string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'errors.category.name.regex',
    })
    name!: string;

    @Field(() => String, {nullable:false})
    @IsString({ message: 'errors.category.description.is.string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'errors.category.description.regex',
    })
    description!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByCategoryInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByCategoryInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedByCategoryInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByCategoryInput;

    @Field(() => ProductCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => ProductCreateNestedManyWithoutCategoryInput)
    @ValidateNested({ each: true, message: 'errors.category.product.nested' })
    product?: ProductCreateNestedManyWithoutCategoryInput;
}
