import { registerEnumType } from '@nestjs/graphql';

export enum CategoryScalarFieldEnum {
    id = "id",
    name = "name",
    description = "description",
    enabled = "enabled",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById"
}


registerEnumType(CategoryScalarFieldEnum, { name: 'CategoryScalarFieldEnum', description: undefined })
