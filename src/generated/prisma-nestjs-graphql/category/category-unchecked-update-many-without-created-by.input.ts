import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateWithoutCreatedByInput } from './category-create-without-created-by.input';
import { CategoryCreateOrConnectWithoutCreatedByInput } from './category-create-or-connect-without-created-by.input';
import { CategoryUpsertWithWhereUniqueWithoutCreatedByInput } from './category-upsert-with-where-unique-without-created-by.input';
import { CategoryCreateManyCreatedByInputEnvelope } from './category-create-many-created-by-input-envelope.input';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithWhereUniqueWithoutCreatedByInput } from './category-update-with-where-unique-without-created-by.input';
import { CategoryUpdateManyWithWhereWithoutCreatedByInput } from './category-update-many-with-where-without-created-by.input';
import { CategoryScalarWhereInput } from './category-scalar-where.input';

@InputType()
export class CategoryUncheckedUpdateManyWithoutCreatedByInput {

    @Field(() => [CategoryCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<CategoryCreateWithoutCreatedByInput>;

    @Field(() => [CategoryCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<CategoryCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [CategoryUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<CategoryUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => CategoryCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: CategoryCreateManyCreatedByInputEnvelope;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    set?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    disconnect?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    delete?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    connect?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<CategoryUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [CategoryUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<CategoryUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [CategoryScalarWhereInput], {nullable:true})
    deleteMany?: Array<CategoryScalarWhereInput>;
}
