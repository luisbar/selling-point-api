import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryScalarWhereInput } from './category-scalar-where.input';
import { CategoryUpdateManyMutationInput } from './category-update-many-mutation.input';

@InputType()
export class CategoryUpdateManyWithWhereWithoutUpdatedByInput {

    @Field(() => CategoryScalarWhereInput, {nullable:false})
    where!: CategoryScalarWhereInput;

    @Field(() => CategoryUpdateManyMutationInput, {nullable:false})
    data!: CategoryUpdateManyMutationInput;
}
