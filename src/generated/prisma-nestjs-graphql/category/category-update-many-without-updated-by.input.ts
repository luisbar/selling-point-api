import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateWithoutUpdatedByInput } from './category-create-without-updated-by.input';
import { CategoryCreateOrConnectWithoutUpdatedByInput } from './category-create-or-connect-without-updated-by.input';
import { CategoryUpsertWithWhereUniqueWithoutUpdatedByInput } from './category-upsert-with-where-unique-without-updated-by.input';
import { CategoryCreateManyUpdatedByInputEnvelope } from './category-create-many-updated-by-input-envelope.input';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithWhereUniqueWithoutUpdatedByInput } from './category-update-with-where-unique-without-updated-by.input';
import { CategoryUpdateManyWithWhereWithoutUpdatedByInput } from './category-update-many-with-where-without-updated-by.input';
import { CategoryScalarWhereInput } from './category-scalar-where.input';

@InputType()
export class CategoryUpdateManyWithoutUpdatedByInput {

    @Field(() => [CategoryCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<CategoryCreateWithoutUpdatedByInput>;

    @Field(() => [CategoryCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<CategoryCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [CategoryUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<CategoryUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => CategoryCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: CategoryCreateManyUpdatedByInputEnvelope;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    set?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    disconnect?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    delete?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryWhereUniqueInput], {nullable:true})
    connect?: Array<CategoryWhereUniqueInput>;

    @Field(() => [CategoryUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<CategoryUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [CategoryUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<CategoryUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [CategoryScalarWhereInput], {nullable:true})
    deleteMany?: Array<CategoryScalarWhereInput>;
}
