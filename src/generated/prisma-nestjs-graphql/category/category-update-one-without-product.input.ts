import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryCreateWithoutProductInput } from './category-create-without-product.input';
import { CategoryCreateOrConnectWithoutProductInput } from './category-create-or-connect-without-product.input';
import { CategoryUpsertWithoutProductInput } from './category-upsert-without-product.input';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithoutProductInput } from './category-update-without-product.input';

@InputType()
export class CategoryUpdateOneWithoutProductInput {

    @Field(() => CategoryCreateWithoutProductInput, {nullable:true})
    create?: CategoryCreateWithoutProductInput;

    @Field(() => CategoryCreateOrConnectWithoutProductInput, {nullable:true})
    connectOrCreate?: CategoryCreateOrConnectWithoutProductInput;

    @Field(() => CategoryUpsertWithoutProductInput, {nullable:true})
    upsert?: CategoryUpsertWithoutProductInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    connect?: CategoryWhereUniqueInput;

    @Field(() => CategoryUpdateWithoutProductInput, {nullable:true})
    update?: CategoryUpdateWithoutProductInput;
}
