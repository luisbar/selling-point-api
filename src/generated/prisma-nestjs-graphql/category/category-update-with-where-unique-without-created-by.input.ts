import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithoutCreatedByInput } from './category-update-without-created-by.input';

@InputType()
export class CategoryUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryUpdateWithoutCreatedByInput, {nullable:false})
    data!: CategoryUpdateWithoutCreatedByInput;
}
