import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithoutUpdatedByInput } from './category-update-without-updated-by.input';

@InputType()
export class CategoryUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryUpdateWithoutUpdatedByInput, {nullable:false})
    data!: CategoryUpdateWithoutUpdatedByInput;
}
