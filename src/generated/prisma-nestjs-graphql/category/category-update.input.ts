import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutCreatedByCategoryInput } from '../user/user-update-one-without-created-by-category.input';
import { UserUpdateOneWithoutUpdatedByCategoryInput } from '../user/user-update-one-without-updated-by-category.input';
import { ProductUpdateManyWithoutCategoryInput } from '../product/product-update-many-without-category.input';

@InputType()
export class CategoryUpdateInput {

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    name?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    description?: StringFieldUpdateOperationsInput;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    enabled?: BoolFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutCreatedByCategoryInput, {nullable:true})
    createdBy?: UserUpdateOneWithoutCreatedByCategoryInput;

    @Field(() => UserUpdateOneWithoutUpdatedByCategoryInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedByCategoryInput;

    @Field(() => ProductUpdateManyWithoutCategoryInput, {nullable:true})
    product?: ProductUpdateManyWithoutCategoryInput;
}
