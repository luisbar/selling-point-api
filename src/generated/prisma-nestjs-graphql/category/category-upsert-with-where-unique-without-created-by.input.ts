import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithoutCreatedByInput } from './category-update-without-created-by.input';
import { CategoryCreateWithoutCreatedByInput } from './category-create-without-created-by.input';

@InputType()
export class CategoryUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryUpdateWithoutCreatedByInput, {nullable:false})
    update!: CategoryUpdateWithoutCreatedByInput;

    @Field(() => CategoryCreateWithoutCreatedByInput, {nullable:false})
    create!: CategoryCreateWithoutCreatedByInput;
}
