import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryWhereUniqueInput } from './category-where-unique.input';
import { CategoryUpdateWithoutUpdatedByInput } from './category-update-without-updated-by.input';
import { CategoryCreateWithoutUpdatedByInput } from './category-create-without-updated-by.input';

@InputType()
export class CategoryUpsertWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    where!: CategoryWhereUniqueInput;

    @Field(() => CategoryUpdateWithoutUpdatedByInput, {nullable:false})
    update!: CategoryUpdateWithoutUpdatedByInput;

    @Field(() => CategoryCreateWithoutUpdatedByInput, {nullable:false})
    create!: CategoryCreateWithoutUpdatedByInput;
}
