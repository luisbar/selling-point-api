import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CategoryUpdateWithoutProductInput } from './category-update-without-product.input';
import { CategoryCreateWithoutProductInput } from './category-create-without-product.input';

@InputType()
export class CategoryUpsertWithoutProductInput {

    @Field(() => CategoryUpdateWithoutProductInput, {nullable:false})
    update!: CategoryUpdateWithoutProductInput;

    @Field(() => CategoryCreateWithoutProductInput, {nullable:false})
    create!: CategoryCreateWithoutProductInput;
}
