import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateManyCreatedByInput } from './customer-create-many-created-by.input';

@InputType()
export class CustomerCreateManyCreatedByInputEnvelope {

    @Field(() => [CustomerCreateManyCreatedByInput], {nullable:false})
    data!: Array<CustomerCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
