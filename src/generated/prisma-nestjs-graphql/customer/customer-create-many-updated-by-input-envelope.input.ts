import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateManyUpdatedByInput } from './customer-create-many-updated-by.input';

@InputType()
export class CustomerCreateManyUpdatedByInputEnvelope {

    @Field(() => [CustomerCreateManyUpdatedByInput], {nullable:false})
    data!: Array<CustomerCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
