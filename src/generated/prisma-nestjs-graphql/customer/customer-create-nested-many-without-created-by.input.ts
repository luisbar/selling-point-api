import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateWithoutCreatedByInput } from './customer-create-without-created-by.input';
import { CustomerCreateOrConnectWithoutCreatedByInput } from './customer-create-or-connect-without-created-by.input';
import { CustomerCreateManyCreatedByInputEnvelope } from './customer-create-many-created-by-input-envelope.input';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';

@InputType()
export class CustomerCreateNestedManyWithoutCreatedByInput {

    @Field(() => [CustomerCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<CustomerCreateWithoutCreatedByInput>;

    @Field(() => [CustomerCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<CustomerCreateOrConnectWithoutCreatedByInput>;

    @Field(() => CustomerCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: CustomerCreateManyCreatedByInputEnvelope;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    connect?: Array<CustomerWhereUniqueInput>;
}
