import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateWithoutUpdatedByInput } from './customer-create-without-updated-by.input';
import { CustomerCreateOrConnectWithoutUpdatedByInput } from './customer-create-or-connect-without-updated-by.input';
import { CustomerCreateManyUpdatedByInputEnvelope } from './customer-create-many-updated-by-input-envelope.input';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';

@InputType()
export class CustomerCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [CustomerCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<CustomerCreateWithoutUpdatedByInput>;

    @Field(() => [CustomerCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<CustomerCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => CustomerCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: CustomerCreateManyUpdatedByInputEnvelope;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    connect?: Array<CustomerWhereUniqueInput>;
}
