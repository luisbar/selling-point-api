import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateWithoutSaleInput } from './customer-create-without-sale.input';
import { CustomerCreateOrConnectWithoutSaleInput } from './customer-create-or-connect-without-sale.input';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';

@InputType()
export class CustomerCreateNestedOneWithoutSaleInput {

    @Field(() => CustomerCreateWithoutSaleInput, {nullable:true})
    create?: CustomerCreateWithoutSaleInput;

    @Field(() => CustomerCreateOrConnectWithoutSaleInput, {nullable:true})
    connectOrCreate?: CustomerCreateOrConnectWithoutSaleInput;

    @Field(() => CustomerWhereUniqueInput, {nullable:true})
    connect?: CustomerWhereUniqueInput;
}
