import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerCreateWithoutCreatedByInput } from './customer-create-without-created-by.input';

@InputType()
export class CustomerCreateOrConnectWithoutCreatedByInput {

    @Field(() => CustomerWhereUniqueInput, {nullable:false})
    where!: CustomerWhereUniqueInput;

    @Field(() => CustomerCreateWithoutCreatedByInput, {nullable:false})
    create!: CustomerCreateWithoutCreatedByInput;
}
