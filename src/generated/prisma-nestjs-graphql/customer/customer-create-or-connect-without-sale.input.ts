import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerCreateWithoutSaleInput } from './customer-create-without-sale.input';

@InputType()
export class CustomerCreateOrConnectWithoutSaleInput {

    @Field(() => CustomerWhereUniqueInput, {nullable:false})
    where!: CustomerWhereUniqueInput;

    @Field(() => CustomerCreateWithoutSaleInput, {nullable:false})
    create!: CustomerCreateWithoutSaleInput;
}
