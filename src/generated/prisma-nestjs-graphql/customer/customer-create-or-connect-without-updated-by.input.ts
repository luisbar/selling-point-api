import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerCreateWithoutUpdatedByInput } from './customer-create-without-updated-by.input';

@InputType()
export class CustomerCreateOrConnectWithoutUpdatedByInput {

    @Field(() => CustomerWhereUniqueInput, {nullable:false})
    where!: CustomerWhereUniqueInput;

    @Field(() => CustomerCreateWithoutUpdatedByInput, {nullable:false})
    create!: CustomerCreateWithoutUpdatedByInput;
}
