import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByCustomerInput } from '../user/user-create-nested-one-without-created-by-customer.input';
import { UserCreateNestedOneWithoutUpdatedByCustomerInput } from '../user/user-create-nested-one-without-updated-by-customer.input';

@InputType()
export class CustomerCreateWithoutSaleInput {

    @Field(() => String, {nullable:false})
    firstName!: string;

    @Field(() => String, {nullable:false})
    lastName!: string;

    @Field(() => String, {nullable:false})
    email!: string;

    @Field(() => String, {nullable:false})
    nit!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByCustomerInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByCustomerInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedByCustomerInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByCustomerInput;
}
