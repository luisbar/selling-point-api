import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByCustomerInput } from '../user/user-create-nested-one-without-created-by-customer.input';
import { SaleCreateNestedManyWithoutCustomerInput } from '../sale/sale-create-nested-many-without-customer.input';

@InputType()
export class CustomerCreateWithoutUpdatedByInput {

    @Field(() => String, {nullable:false})
    firstName!: string;

    @Field(() => String, {nullable:false})
    lastName!: string;

    @Field(() => String, {nullable:false})
    email!: string;

    @Field(() => String, {nullable:false})
    nit!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByCustomerInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByCustomerInput;

    @Field(() => SaleCreateNestedManyWithoutCustomerInput, {nullable:true})
    sale?: SaleCreateNestedManyWithoutCustomerInput;
}
