import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { SaleUncheckedCreateNestedManyWithoutCustomerInput } from '../sale/sale-unchecked-create-nested-many-without-customer.input';

@InputType()
export class CustomerUncheckedCreateWithoutCreatedByInput {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => String, {nullable:false})
    firstName!: string;

    @Field(() => String, {nullable:false})
    lastName!: string;

    @Field(() => String, {nullable:false})
    email!: string;

    @Field(() => String, {nullable:false})
    nit!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => SaleUncheckedCreateNestedManyWithoutCustomerInput, {nullable:true})
    sale?: SaleUncheckedCreateNestedManyWithoutCustomerInput;
}
