import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateWithoutCreatedByInput } from './customer-create-without-created-by.input';
import { CustomerCreateOrConnectWithoutCreatedByInput } from './customer-create-or-connect-without-created-by.input';
import { CustomerUpsertWithWhereUniqueWithoutCreatedByInput } from './customer-upsert-with-where-unique-without-created-by.input';
import { CustomerCreateManyCreatedByInputEnvelope } from './customer-create-many-created-by-input-envelope.input';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerUpdateWithWhereUniqueWithoutCreatedByInput } from './customer-update-with-where-unique-without-created-by.input';
import { CustomerUpdateManyWithWhereWithoutCreatedByInput } from './customer-update-many-with-where-without-created-by.input';
import { CustomerScalarWhereInput } from './customer-scalar-where.input';

@InputType()
export class CustomerUncheckedUpdateManyWithoutCreatedByInput {

    @Field(() => [CustomerCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<CustomerCreateWithoutCreatedByInput>;

    @Field(() => [CustomerCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<CustomerCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [CustomerUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<CustomerUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => CustomerCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: CustomerCreateManyCreatedByInputEnvelope;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    set?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    disconnect?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    delete?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    connect?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<CustomerUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [CustomerUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<CustomerUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [CustomerScalarWhereInput], {nullable:true})
    deleteMany?: Array<CustomerScalarWhereInput>;
}
