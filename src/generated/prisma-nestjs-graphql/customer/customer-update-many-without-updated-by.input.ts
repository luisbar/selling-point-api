import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateWithoutUpdatedByInput } from './customer-create-without-updated-by.input';
import { CustomerCreateOrConnectWithoutUpdatedByInput } from './customer-create-or-connect-without-updated-by.input';
import { CustomerUpsertWithWhereUniqueWithoutUpdatedByInput } from './customer-upsert-with-where-unique-without-updated-by.input';
import { CustomerCreateManyUpdatedByInputEnvelope } from './customer-create-many-updated-by-input-envelope.input';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerUpdateWithWhereUniqueWithoutUpdatedByInput } from './customer-update-with-where-unique-without-updated-by.input';
import { CustomerUpdateManyWithWhereWithoutUpdatedByInput } from './customer-update-many-with-where-without-updated-by.input';
import { CustomerScalarWhereInput } from './customer-scalar-where.input';

@InputType()
export class CustomerUpdateManyWithoutUpdatedByInput {

    @Field(() => [CustomerCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<CustomerCreateWithoutUpdatedByInput>;

    @Field(() => [CustomerCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<CustomerCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [CustomerUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<CustomerUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => CustomerCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: CustomerCreateManyUpdatedByInputEnvelope;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    set?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    disconnect?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    delete?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerWhereUniqueInput], {nullable:true})
    connect?: Array<CustomerWhereUniqueInput>;

    @Field(() => [CustomerUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<CustomerUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [CustomerUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<CustomerUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [CustomerScalarWhereInput], {nullable:true})
    deleteMany?: Array<CustomerScalarWhereInput>;
}
