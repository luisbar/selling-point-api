import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerCreateWithoutSaleInput } from './customer-create-without-sale.input';
import { CustomerCreateOrConnectWithoutSaleInput } from './customer-create-or-connect-without-sale.input';
import { CustomerUpsertWithoutSaleInput } from './customer-upsert-without-sale.input';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerUpdateWithoutSaleInput } from './customer-update-without-sale.input';

@InputType()
export class CustomerUpdateOneWithoutSaleInput {

    @Field(() => CustomerCreateWithoutSaleInput, {nullable:true})
    create?: CustomerCreateWithoutSaleInput;

    @Field(() => CustomerCreateOrConnectWithoutSaleInput, {nullable:true})
    connectOrCreate?: CustomerCreateOrConnectWithoutSaleInput;

    @Field(() => CustomerUpsertWithoutSaleInput, {nullable:true})
    upsert?: CustomerUpsertWithoutSaleInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => CustomerWhereUniqueInput, {nullable:true})
    connect?: CustomerWhereUniqueInput;

    @Field(() => CustomerUpdateWithoutSaleInput, {nullable:true})
    update?: CustomerUpdateWithoutSaleInput;
}
