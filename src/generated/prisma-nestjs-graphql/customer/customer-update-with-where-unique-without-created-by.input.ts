import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerUpdateWithoutCreatedByInput } from './customer-update-without-created-by.input';

@InputType()
export class CustomerUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => CustomerWhereUniqueInput, {nullable:false})
    where!: CustomerWhereUniqueInput;

    @Field(() => CustomerUpdateWithoutCreatedByInput, {nullable:false})
    data!: CustomerUpdateWithoutCreatedByInput;
}
