import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerUpdateWithoutUpdatedByInput } from './customer-update-without-updated-by.input';

@InputType()
export class CustomerUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => CustomerWhereUniqueInput, {nullable:false})
    where!: CustomerWhereUniqueInput;

    @Field(() => CustomerUpdateWithoutUpdatedByInput, {nullable:false})
    data!: CustomerUpdateWithoutUpdatedByInput;
}
