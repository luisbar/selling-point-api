import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutCreatedByCustomerInput } from '../user/user-update-one-without-created-by-customer.input';
import { UserUpdateOneWithoutUpdatedByCustomerInput } from '../user/user-update-one-without-updated-by-customer.input';
import { SaleUpdateManyWithoutCustomerInput } from '../sale/sale-update-many-without-customer.input';

@InputType()
export class CustomerUpdateInput {

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    firstName?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    lastName?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    email?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    nit?: StringFieldUpdateOperationsInput;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    enabled?: BoolFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutCreatedByCustomerInput, {nullable:true})
    createdBy?: UserUpdateOneWithoutCreatedByCustomerInput;

    @Field(() => UserUpdateOneWithoutUpdatedByCustomerInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedByCustomerInput;

    @Field(() => SaleUpdateManyWithoutCustomerInput, {nullable:true})
    sale?: SaleUpdateManyWithoutCustomerInput;
}
