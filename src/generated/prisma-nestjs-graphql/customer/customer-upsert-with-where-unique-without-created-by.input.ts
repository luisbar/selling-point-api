import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerWhereUniqueInput } from './customer-where-unique.input';
import { CustomerUpdateWithoutCreatedByInput } from './customer-update-without-created-by.input';
import { CustomerCreateWithoutCreatedByInput } from './customer-create-without-created-by.input';

@InputType()
export class CustomerUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => CustomerWhereUniqueInput, {nullable:false})
    where!: CustomerWhereUniqueInput;

    @Field(() => CustomerUpdateWithoutCreatedByInput, {nullable:false})
    update!: CustomerUpdateWithoutCreatedByInput;

    @Field(() => CustomerCreateWithoutCreatedByInput, {nullable:false})
    create!: CustomerCreateWithoutCreatedByInput;
}
