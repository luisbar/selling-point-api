import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { CustomerUpdateWithoutSaleInput } from './customer-update-without-sale.input';
import { CustomerCreateWithoutSaleInput } from './customer-create-without-sale.input';

@InputType()
export class CustomerUpsertWithoutSaleInput {

    @Field(() => CustomerUpdateWithoutSaleInput, {nullable:false})
    update!: CustomerUpdateWithoutSaleInput;

    @Field(() => CustomerCreateWithoutSaleInput, {nullable:false})
    create!: CustomerCreateWithoutSaleInput;
}
