import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { CustomerFirstNameLastNameCompoundUniqueInput } from './customer-first-name-last-name-compound-unique.input';

@InputType()
export class CustomerWhereUniqueInput {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => CustomerFirstNameLastNameCompoundUniqueInput, {nullable:true})
    firstName_lastName?: CustomerFirstNameLastNameCompoundUniqueInput;
}
