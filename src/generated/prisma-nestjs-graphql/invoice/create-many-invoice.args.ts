import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceCreateManyInput } from './invoice-create-many.input';

@ArgsType()
export class CreateManyInvoiceArgs {

    @Field(() => [InvoiceCreateManyInput], {nullable:false})
    data!: Array<InvoiceCreateManyInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
