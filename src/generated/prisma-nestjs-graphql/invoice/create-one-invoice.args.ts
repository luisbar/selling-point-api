import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceCreateInput } from './invoice-create.input';

@ArgsType()
export class CreateOneInvoiceArgs {

    @Field(() => InvoiceCreateInput, {nullable:false})
    data!: InvoiceCreateInput;
}
