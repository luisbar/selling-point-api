import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceWhereInput } from './invoice-where.input';

@ArgsType()
export class DeleteManyInvoiceArgs {

    @Field(() => InvoiceWhereInput, {nullable:true})
    where?: InvoiceWhereInput;
}
