import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';

@ArgsType()
export class DeleteOneInvoiceArgs {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;
}
