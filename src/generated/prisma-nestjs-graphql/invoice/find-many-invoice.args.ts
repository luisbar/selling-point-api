import { Field, InputType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceWhereInput } from './invoice-where.input';
import { InvoiceOrderByWithRelationInput } from './invoice-order-by-with-relation.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { Int } from '@nestjs/graphql';
import { InvoiceScalarFieldEnum } from './invoice-scalar-field.enum';

@InputType()
@ArgsType()
export class FindManyInvoiceArgs {

    @Field(() => InvoiceWhereInput, {nullable:true})
    where?: InvoiceWhereInput;

    @Field(() => [InvoiceOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<InvoiceOrderByWithRelationInput>;

    @Field(() => InvoiceWhereUniqueInput, {nullable:true})
    cursor?: InvoiceWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => [InvoiceScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof InvoiceScalarFieldEnum>;
}
