import { Field, InputType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';

@InputType()
@ArgsType()
export class FindUniqueInvoiceArgs {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;
}
