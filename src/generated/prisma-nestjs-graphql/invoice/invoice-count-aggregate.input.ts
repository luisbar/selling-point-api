import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';

@InputType()
export class InvoiceCountAggregateInput {

    @Field(() => Boolean, {nullable:true})
    id?: true;

    @Field(() => Boolean, {nullable:true})
    sfc?: true;

    @Field(() => Boolean, {nullable:true})
    nit?: true;

    @Field(() => Boolean, {nullable:true})
    authorizationNumber?: true;

    @Field(() => Boolean, {nullable:true})
    invoiceNumber?: true;

    @Field(() => Boolean, {nullable:true})
    total?: true;

    @Field(() => Boolean, {nullable:true})
    emittedDate?: true;

    @Field(() => Boolean, {nullable:true})
    limitDate?: true;

    @Field(() => Boolean, {nullable:true})
    controlCode?: true;

    @Field(() => Boolean, {nullable:true})
    customerNit?: true;

    @Field(() => Boolean, {nullable:true})
    customerName?: true;

    @Field(() => Boolean, {nullable:true})
    canceled?: true;

    @Field(() => Boolean, {nullable:true})
    createdAt?: true;

    @Field(() => Boolean, {nullable:true})
    updatedAt?: true;

    @Field(() => Boolean, {nullable:true})
    createdById?: true;

    @Field(() => Boolean, {nullable:true})
    updatedById?: true;

    @Field(() => Boolean, {nullable:true})
    _all?: true;
}
