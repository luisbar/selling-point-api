import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';

@ObjectType()
export class InvoiceCountAggregate {

    @Field(() => Int, {nullable:false})
    id!: number;

    @Field(() => Int, {nullable:false})
    sfc!: number;

    @Field(() => Int, {nullable:false})
    nit!: number;

    @Field(() => Int, {nullable:false})
    authorizationNumber!: number;

    @Field(() => Int, {nullable:false})
    invoiceNumber!: number;

    @Field(() => Int, {nullable:false})
    total!: number;

    @Field(() => Int, {nullable:false})
    emittedDate!: number;

    @Field(() => Int, {nullable:false})
    limitDate!: number;

    @Field(() => Int, {nullable:false})
    controlCode!: number;

    @Field(() => Int, {nullable:false})
    customerNit!: number;

    @Field(() => Int, {nullable:false})
    customerName!: number;

    @Field(() => Int, {nullable:false})
    canceled!: number;

    @Field(() => Int, {nullable:false})
    createdAt!: number;

    @Field(() => Int, {nullable:false})
    updatedAt!: number;

    @Field(() => Int, {nullable:false})
    createdById!: number;

    @Field(() => Int, {nullable:false})
    updatedById!: number;

    @Field(() => Int, {nullable:false})
    _all!: number;
}
