import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';

@InputType()
export class InvoiceCountOrderByAggregateInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    sfc?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    nit?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    authorizationNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    invoiceNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    total?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    emittedDate?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    limitDate?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    controlCode?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerNit?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    canceled?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;
}
