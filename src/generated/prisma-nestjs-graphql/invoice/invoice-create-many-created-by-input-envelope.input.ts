import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateManyCreatedByInput } from './invoice-create-many-created-by.input';

@InputType()
export class InvoiceCreateManyCreatedByInputEnvelope {

    @Field(() => [InvoiceCreateManyCreatedByInput], {nullable:false})
    data!: Array<InvoiceCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
