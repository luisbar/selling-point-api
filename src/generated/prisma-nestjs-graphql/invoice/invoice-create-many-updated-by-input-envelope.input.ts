import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateManyUpdatedByInput } from './invoice-create-many-updated-by.input';

@InputType()
export class InvoiceCreateManyUpdatedByInputEnvelope {

    @Field(() => [InvoiceCreateManyUpdatedByInput], {nullable:false})
    data!: Array<InvoiceCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
