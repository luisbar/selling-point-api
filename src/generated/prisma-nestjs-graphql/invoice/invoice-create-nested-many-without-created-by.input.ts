import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateWithoutCreatedByInput } from './invoice-create-without-created-by.input';
import { InvoiceCreateOrConnectWithoutCreatedByInput } from './invoice-create-or-connect-without-created-by.input';
import { InvoiceCreateManyCreatedByInputEnvelope } from './invoice-create-many-created-by-input-envelope.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';

@InputType()
export class InvoiceCreateNestedManyWithoutCreatedByInput {

    @Field(() => [InvoiceCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<InvoiceCreateWithoutCreatedByInput>;

    @Field(() => [InvoiceCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<InvoiceCreateOrConnectWithoutCreatedByInput>;

    @Field(() => InvoiceCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: InvoiceCreateManyCreatedByInputEnvelope;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    connect?: Array<InvoiceWhereUniqueInput>;
}
