import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateWithoutUpdatedByInput } from './invoice-create-without-updated-by.input';
import { InvoiceCreateOrConnectWithoutUpdatedByInput } from './invoice-create-or-connect-without-updated-by.input';
import { InvoiceCreateManyUpdatedByInputEnvelope } from './invoice-create-many-updated-by-input-envelope.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';

@InputType()
export class InvoiceCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [InvoiceCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<InvoiceCreateWithoutUpdatedByInput>;

    @Field(() => [InvoiceCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<InvoiceCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => InvoiceCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: InvoiceCreateManyUpdatedByInputEnvelope;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    connect?: Array<InvoiceWhereUniqueInput>;
}
