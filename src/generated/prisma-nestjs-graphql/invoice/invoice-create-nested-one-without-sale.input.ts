import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateWithoutSaleInput } from './invoice-create-without-sale.input';
import { InvoiceCreateOrConnectWithoutSaleInput } from './invoice-create-or-connect-without-sale.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';

@InputType()
export class InvoiceCreateNestedOneWithoutSaleInput {

    @Field(() => InvoiceCreateWithoutSaleInput, {nullable:true})
    create?: InvoiceCreateWithoutSaleInput;

    @Field(() => InvoiceCreateOrConnectWithoutSaleInput, {nullable:true})
    connectOrCreate?: InvoiceCreateOrConnectWithoutSaleInput;

    @Field(() => InvoiceWhereUniqueInput, {nullable:true})
    connect?: InvoiceWhereUniqueInput;
}
