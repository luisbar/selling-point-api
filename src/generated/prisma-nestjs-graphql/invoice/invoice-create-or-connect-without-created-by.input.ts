import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceCreateWithoutCreatedByInput } from './invoice-create-without-created-by.input';

@InputType()
export class InvoiceCreateOrConnectWithoutCreatedByInput {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceCreateWithoutCreatedByInput, {nullable:false})
    create!: InvoiceCreateWithoutCreatedByInput;
}
