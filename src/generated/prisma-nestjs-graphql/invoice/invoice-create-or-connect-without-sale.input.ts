import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceCreateWithoutSaleInput } from './invoice-create-without-sale.input';

@InputType()
export class InvoiceCreateOrConnectWithoutSaleInput {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceCreateWithoutSaleInput, {nullable:false})
    create!: InvoiceCreateWithoutSaleInput;
}
