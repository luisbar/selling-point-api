import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceCreateWithoutUpdatedByInput } from './invoice-create-without-updated-by.input';

@InputType()
export class InvoiceCreateOrConnectWithoutUpdatedByInput {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceCreateWithoutUpdatedByInput, {nullable:false})
    create!: InvoiceCreateWithoutUpdatedByInput;
}
