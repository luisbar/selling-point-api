import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceWhereInput } from './invoice-where.input';
import { InvoiceOrderByWithAggregationInput } from './invoice-order-by-with-aggregation.input';
import { InvoiceScalarFieldEnum } from './invoice-scalar-field.enum';
import { InvoiceScalarWhereWithAggregatesInput } from './invoice-scalar-where-with-aggregates.input';
import { Int } from '@nestjs/graphql';
import { InvoiceCountAggregateInput } from './invoice-count-aggregate.input';
import { InvoiceAvgAggregateInput } from './invoice-avg-aggregate.input';
import { InvoiceSumAggregateInput } from './invoice-sum-aggregate.input';
import { InvoiceMinAggregateInput } from './invoice-min-aggregate.input';
import { InvoiceMaxAggregateInput } from './invoice-max-aggregate.input';

@ArgsType()
export class InvoiceGroupByArgs {

    @Field(() => InvoiceWhereInput, {nullable:true})
    where?: InvoiceWhereInput;

    @Field(() => [InvoiceOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<InvoiceOrderByWithAggregationInput>;

    @Field(() => [InvoiceScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof InvoiceScalarFieldEnum>;

    @Field(() => InvoiceScalarWhereWithAggregatesInput, {nullable:true})
    having?: InvoiceScalarWhereWithAggregatesInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => InvoiceCountAggregateInput, {nullable:true})
    _count?: InvoiceCountAggregateInput;

    @Field(() => InvoiceAvgAggregateInput, {nullable:true})
    _avg?: InvoiceAvgAggregateInput;

    @Field(() => InvoiceSumAggregateInput, {nullable:true})
    _sum?: InvoiceSumAggregateInput;

    @Field(() => InvoiceMinAggregateInput, {nullable:true})
    _min?: InvoiceMinAggregateInput;

    @Field(() => InvoiceMaxAggregateInput, {nullable:true})
    _max?: InvoiceMaxAggregateInput;
}
