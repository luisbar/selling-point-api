import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { InvoiceCountAggregate } from './invoice-count-aggregate.output';
import { InvoiceAvgAggregate } from './invoice-avg-aggregate.output';
import { InvoiceSumAggregate } from './invoice-sum-aggregate.output';
import { InvoiceMinAggregate } from './invoice-min-aggregate.output';
import { InvoiceMaxAggregate } from './invoice-max-aggregate.output';

@ObjectType()
export class InvoiceGroupBy {

    @Field(() => Int, {nullable:false})
    id!: number;

    @Field(() => String, {nullable:false})
    sfc!: string;

    @Field(() => String, {nullable:false})
    nit!: string;

    @Field(() => String, {nullable:false})
    authorizationNumber!: string;

    @Field(() => String, {nullable:false})
    invoiceNumber!: string;

    @Field(() => Float, {nullable:false})
    total!: number;

    @Field(() => Date, {nullable:false})
    emittedDate!: Date | string;

    @Field(() => Date, {nullable:false})
    limitDate!: Date | string;

    @Field(() => String, {nullable:false})
    controlCode!: string;

    @Field(() => String, {nullable:false})
    customerNit!: string;

    @Field(() => String, {nullable:false})
    customerName!: string;

    @Field(() => Boolean, {nullable:false})
    canceled!: boolean;

    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => InvoiceCountAggregate, {nullable:true})
    _count?: InvoiceCountAggregate;

    @Field(() => InvoiceAvgAggregate, {nullable:true})
    _avg?: InvoiceAvgAggregate;

    @Field(() => InvoiceSumAggregate, {nullable:true})
    _sum?: InvoiceSumAggregate;

    @Field(() => InvoiceMinAggregate, {nullable:true})
    _min?: InvoiceMinAggregate;

    @Field(() => InvoiceMaxAggregate, {nullable:true})
    _max?: InvoiceMaxAggregate;
}
