import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';

@ObjectType()
export class InvoiceMinAggregate {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => String, {nullable:true})
    sfc?: string;

    @Field(() => String, {nullable:true})
    nit?: string;

    @Field(() => String, {nullable:true})
    authorizationNumber?: string;

    @Field(() => String, {nullable:true})
    invoiceNumber?: string;

    @Field(() => Float, {nullable:true})
    total?: number;

    @Field(() => Date, {nullable:true})
    emittedDate?: Date | string;

    @Field(() => Date, {nullable:true})
    limitDate?: Date | string;

    @Field(() => String, {nullable:true})
    controlCode?: string;

    @Field(() => String, {nullable:true})
    customerNit?: string;

    @Field(() => String, {nullable:true})
    customerName?: string;

    @Field(() => Boolean, {nullable:true})
    canceled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;
}
