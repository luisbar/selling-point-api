import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { InvoiceCountOrderByAggregateInput } from './invoice-count-order-by-aggregate.input';
import { InvoiceAvgOrderByAggregateInput } from './invoice-avg-order-by-aggregate.input';
import { InvoiceMaxOrderByAggregateInput } from './invoice-max-order-by-aggregate.input';
import { InvoiceMinOrderByAggregateInput } from './invoice-min-order-by-aggregate.input';
import { InvoiceSumOrderByAggregateInput } from './invoice-sum-order-by-aggregate.input';

@InputType()
export class InvoiceOrderByWithAggregationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    sfc?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    nit?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    authorizationNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    invoiceNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    total?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    emittedDate?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    limitDate?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    controlCode?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerNit?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    canceled?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => InvoiceCountOrderByAggregateInput, {nullable:true})
    _count?: InvoiceCountOrderByAggregateInput;

    @Field(() => InvoiceAvgOrderByAggregateInput, {nullable:true})
    _avg?: InvoiceAvgOrderByAggregateInput;

    @Field(() => InvoiceMaxOrderByAggregateInput, {nullable:true})
    _max?: InvoiceMaxOrderByAggregateInput;

    @Field(() => InvoiceMinOrderByAggregateInput, {nullable:true})
    _min?: InvoiceMinOrderByAggregateInput;

    @Field(() => InvoiceSumOrderByAggregateInput, {nullable:true})
    _sum?: InvoiceSumOrderByAggregateInput;
}
