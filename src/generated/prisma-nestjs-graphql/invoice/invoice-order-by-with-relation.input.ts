import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { UserOrderByWithRelationInput } from '../user/user-order-by-with-relation.input';
import { SaleOrderByWithRelationInput } from '../sale/sale-order-by-with-relation.input';

@InputType()
export class InvoiceOrderByWithRelationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    sfc?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    nit?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    authorizationNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    invoiceNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    total?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    emittedDate?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    limitDate?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    controlCode?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerNit?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    canceled?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    createdBy?: UserOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    updatedBy?: UserOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => SaleOrderByWithRelationInput, {nullable:true})
    sale?: SaleOrderByWithRelationInput;
}
