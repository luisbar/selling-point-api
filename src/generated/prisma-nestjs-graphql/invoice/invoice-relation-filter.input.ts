import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereInput } from './invoice-where.input';

@InputType()
export class InvoiceRelationFilter {

    @Field(() => InvoiceWhereInput, {nullable:true})
    is?: InvoiceWhereInput;

    @Field(() => InvoiceWhereInput, {nullable:true})
    isNot?: InvoiceWhereInput;
}
