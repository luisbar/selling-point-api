import { registerEnumType } from '@nestjs/graphql';

export enum InvoiceScalarFieldEnum {
    id = "id",
    sfc = "sfc",
    nit = "nit",
    authorizationNumber = "authorizationNumber",
    invoiceNumber = "invoiceNumber",
    total = "total",
    emittedDate = "emittedDate",
    limitDate = "limitDate",
    controlCode = "controlCode",
    customerNit = "customerNit",
    customerName = "customerName",
    canceled = "canceled",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById"
}


registerEnumType(InvoiceScalarFieldEnum, { name: 'InvoiceScalarFieldEnum', description: undefined })
