import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { StringFilter } from '../prisma/string-filter.input';
import { FloatFilter } from '../prisma/float-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';

@InputType()
export class InvoiceScalarWhereInput {

    @Field(() => [InvoiceScalarWhereInput], {nullable:true})
    AND?: Array<InvoiceScalarWhereInput>;

    @Field(() => [InvoiceScalarWhereInput], {nullable:true})
    OR?: Array<InvoiceScalarWhereInput>;

    @Field(() => [InvoiceScalarWhereInput], {nullable:true})
    NOT?: Array<InvoiceScalarWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => StringFilter, {nullable:true})
    sfc?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    nit?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    authorizationNumber?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    invoiceNumber?: StringFilter;

    @Field(() => FloatFilter, {nullable:true})
    total?: FloatFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    emittedDate?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    limitDate?: DateTimeFilter;

    @Field(() => StringFilter, {nullable:true})
    controlCode?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    customerNit?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    customerName?: StringFilter;

    @Field(() => BoolFilter, {nullable:true})
    canceled?: BoolFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;
}
