import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { SaleUncheckedCreateNestedOneWithoutInvoiceInput } from '../sale/sale-unchecked-create-nested-one-without-invoice.input';

@InputType()
export class InvoiceUncheckedCreateWithoutUpdatedByInput {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => String, {nullable:false})
    sfc!: string;

    @Field(() => String, {nullable:false})
    nit!: string;

    @Field(() => String, {nullable:false})
    authorizationNumber!: string;

    @Field(() => String, {nullable:false})
    invoiceNumber!: string;

    @Field(() => Float, {nullable:false})
    total!: number;

    @Field(() => Date, {nullable:true})
    emittedDate?: Date | string;

    @Field(() => Date, {nullable:false})
    limitDate!: Date | string;

    @Field(() => String, {nullable:false})
    controlCode!: string;

    @Field(() => String, {nullable:false})
    customerNit!: string;

    @Field(() => String, {nullable:false})
    customerName!: string;

    @Field(() => Boolean, {nullable:false})
    canceled!: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => SaleUncheckedCreateNestedOneWithoutInvoiceInput, {nullable:true})
    sale?: SaleUncheckedCreateNestedOneWithoutInvoiceInput;
}
