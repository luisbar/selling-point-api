import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateWithoutUpdatedByInput } from './invoice-create-without-updated-by.input';
import { InvoiceCreateOrConnectWithoutUpdatedByInput } from './invoice-create-or-connect-without-updated-by.input';
import { InvoiceUpsertWithWhereUniqueWithoutUpdatedByInput } from './invoice-upsert-with-where-unique-without-updated-by.input';
import { InvoiceCreateManyUpdatedByInputEnvelope } from './invoice-create-many-updated-by-input-envelope.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceUpdateWithWhereUniqueWithoutUpdatedByInput } from './invoice-update-with-where-unique-without-updated-by.input';
import { InvoiceUpdateManyWithWhereWithoutUpdatedByInput } from './invoice-update-many-with-where-without-updated-by.input';
import { InvoiceScalarWhereInput } from './invoice-scalar-where.input';

@InputType()
export class InvoiceUncheckedUpdateManyWithoutUpdatedByInput {

    @Field(() => [InvoiceCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<InvoiceCreateWithoutUpdatedByInput>;

    @Field(() => [InvoiceCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<InvoiceCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [InvoiceUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<InvoiceUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => InvoiceCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: InvoiceCreateManyUpdatedByInputEnvelope;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    set?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    disconnect?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    delete?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    connect?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<InvoiceUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [InvoiceUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<InvoiceUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [InvoiceScalarWhereInput], {nullable:true})
    deleteMany?: Array<InvoiceScalarWhereInput>;
}
