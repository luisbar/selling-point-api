import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceScalarWhereInput } from './invoice-scalar-where.input';
import { InvoiceUpdateManyMutationInput } from './invoice-update-many-mutation.input';

@InputType()
export class InvoiceUpdateManyWithWhereWithoutUpdatedByInput {

    @Field(() => InvoiceScalarWhereInput, {nullable:false})
    where!: InvoiceScalarWhereInput;

    @Field(() => InvoiceUpdateManyMutationInput, {nullable:false})
    data!: InvoiceUpdateManyMutationInput;
}
