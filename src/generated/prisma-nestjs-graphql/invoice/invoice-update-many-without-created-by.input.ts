import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateWithoutCreatedByInput } from './invoice-create-without-created-by.input';
import { InvoiceCreateOrConnectWithoutCreatedByInput } from './invoice-create-or-connect-without-created-by.input';
import { InvoiceUpsertWithWhereUniqueWithoutCreatedByInput } from './invoice-upsert-with-where-unique-without-created-by.input';
import { InvoiceCreateManyCreatedByInputEnvelope } from './invoice-create-many-created-by-input-envelope.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceUpdateWithWhereUniqueWithoutCreatedByInput } from './invoice-update-with-where-unique-without-created-by.input';
import { InvoiceUpdateManyWithWhereWithoutCreatedByInput } from './invoice-update-many-with-where-without-created-by.input';
import { InvoiceScalarWhereInput } from './invoice-scalar-where.input';

@InputType()
export class InvoiceUpdateManyWithoutCreatedByInput {

    @Field(() => [InvoiceCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<InvoiceCreateWithoutCreatedByInput>;

    @Field(() => [InvoiceCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<InvoiceCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [InvoiceUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<InvoiceUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => InvoiceCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: InvoiceCreateManyCreatedByInputEnvelope;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    set?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    disconnect?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    delete?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceWhereUniqueInput], {nullable:true})
    connect?: Array<InvoiceWhereUniqueInput>;

    @Field(() => [InvoiceUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<InvoiceUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [InvoiceUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<InvoiceUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [InvoiceScalarWhereInput], {nullable:true})
    deleteMany?: Array<InvoiceScalarWhereInput>;
}
