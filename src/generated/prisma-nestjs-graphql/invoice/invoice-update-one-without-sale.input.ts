import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceCreateWithoutSaleInput } from './invoice-create-without-sale.input';
import { InvoiceCreateOrConnectWithoutSaleInput } from './invoice-create-or-connect-without-sale.input';
import { InvoiceUpsertWithoutSaleInput } from './invoice-upsert-without-sale.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceUpdateWithoutSaleInput } from './invoice-update-without-sale.input';

@InputType()
export class InvoiceUpdateOneWithoutSaleInput {

    @Field(() => InvoiceCreateWithoutSaleInput, {nullable:true})
    create?: InvoiceCreateWithoutSaleInput;

    @Field(() => InvoiceCreateOrConnectWithoutSaleInput, {nullable:true})
    connectOrCreate?: InvoiceCreateOrConnectWithoutSaleInput;

    @Field(() => InvoiceUpsertWithoutSaleInput, {nullable:true})
    upsert?: InvoiceUpsertWithoutSaleInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => InvoiceWhereUniqueInput, {nullable:true})
    connect?: InvoiceWhereUniqueInput;

    @Field(() => InvoiceUpdateWithoutSaleInput, {nullable:true})
    update?: InvoiceUpdateWithoutSaleInput;
}
