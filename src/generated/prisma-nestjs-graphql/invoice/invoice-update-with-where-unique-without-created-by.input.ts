import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceUpdateWithoutCreatedByInput } from './invoice-update-without-created-by.input';

@InputType()
export class InvoiceUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceUpdateWithoutCreatedByInput, {nullable:false})
    data!: InvoiceUpdateWithoutCreatedByInput;
}
