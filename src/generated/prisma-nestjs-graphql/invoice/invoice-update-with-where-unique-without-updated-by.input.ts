import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceUpdateWithoutUpdatedByInput } from './invoice-update-without-updated-by.input';

@InputType()
export class InvoiceUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceUpdateWithoutUpdatedByInput, {nullable:false})
    data!: InvoiceUpdateWithoutUpdatedByInput;
}
