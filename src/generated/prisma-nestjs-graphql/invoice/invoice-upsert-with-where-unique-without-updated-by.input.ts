import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceUpdateWithoutUpdatedByInput } from './invoice-update-without-updated-by.input';
import { InvoiceCreateWithoutUpdatedByInput } from './invoice-create-without-updated-by.input';

@InputType()
export class InvoiceUpsertWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceUpdateWithoutUpdatedByInput, {nullable:false})
    update!: InvoiceUpdateWithoutUpdatedByInput;

    @Field(() => InvoiceCreateWithoutUpdatedByInput, {nullable:false})
    create!: InvoiceCreateWithoutUpdatedByInput;
}
