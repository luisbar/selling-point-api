import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { InvoiceUpdateWithoutSaleInput } from './invoice-update-without-sale.input';
import { InvoiceCreateWithoutSaleInput } from './invoice-create-without-sale.input';

@InputType()
export class InvoiceUpsertWithoutSaleInput {

    @Field(() => InvoiceUpdateWithoutSaleInput, {nullable:false})
    update!: InvoiceUpdateWithoutSaleInput;

    @Field(() => InvoiceCreateWithoutSaleInput, {nullable:false})
    create!: InvoiceCreateWithoutSaleInput;
}
