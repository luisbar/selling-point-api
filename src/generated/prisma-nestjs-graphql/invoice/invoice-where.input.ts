import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { StringFilter } from '../prisma/string-filter.input';
import { FloatFilter } from '../prisma/float-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { UserRelationFilter } from '../user/user-relation-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';
import { SaleRelationFilter } from '../sale/sale-relation-filter.input';

@InputType()
export class InvoiceWhereInput {

    @Field(() => [InvoiceWhereInput], {nullable:true})
    AND?: Array<InvoiceWhereInput>;

    @Field(() => [InvoiceWhereInput], {nullable:true})
    OR?: Array<InvoiceWhereInput>;

    @Field(() => [InvoiceWhereInput], {nullable:true})
    NOT?: Array<InvoiceWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => StringFilter, {nullable:true})
    sfc?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    nit?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    authorizationNumber?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    invoiceNumber?: StringFilter;

    @Field(() => FloatFilter, {nullable:true})
    total?: FloatFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    emittedDate?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    limitDate?: DateTimeFilter;

    @Field(() => StringFilter, {nullable:true})
    controlCode?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    customerNit?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    customerName?: StringFilter;

    @Field(() => BoolFilter, {nullable:true})
    canceled?: BoolFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    createdBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    updatedBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => SaleRelationFilter, {nullable:true})
    sale?: SaleRelationFilter;
}
