import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { User } from '../user/user.model';
import { Int } from '@nestjs/graphql';
import { Sale } from '../sale/sale.model';

@ObjectType()
export class Invoice {

    @Field(() => ID, {nullable:false})
    id!: number;

    @Field(() => String, {nullable:false})
    sfc!: string;

    @Field(() => String, {nullable:false})
    nit!: string;

    @Field(() => String, {nullable:false})
    authorizationNumber!: string;

    @Field(() => String, {nullable:false})
    invoiceNumber!: string;

    @Field(() => Float, {nullable:false})
    total!: number;

    @Field(() => Date, {nullable:false})
    emittedDate!: Date;

    @Field(() => Date, {nullable:false})
    limitDate!: Date;

    @Field(() => String, {nullable:false})
    controlCode!: string;

    @Field(() => String, {nullable:false})
    customerNit!: string;

    @Field(() => String, {nullable:false})
    customerName!: string;

    @Field(() => Boolean, {nullable:false})
    canceled!: boolean;

    @Field(() => Date, {nullable:false})
    createdAt!: Date;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date;

    @Field(() => User, {nullable:true})
    createdBy?: User | null;

    @Field(() => Int, {nullable:true})
    createdById!: number | null;

    @Field(() => User, {nullable:true})
    updatedBy?: User | null;

    @Field(() => Int, {nullable:true})
    updatedById!: number | null;

    @Field(() => Sale, {nullable:true})
    sale?: Sale | null;
}
