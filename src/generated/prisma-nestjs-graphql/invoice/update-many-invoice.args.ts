import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceUpdateManyMutationInput } from './invoice-update-many-mutation.input';
import { InvoiceWhereInput } from './invoice-where.input';

@ArgsType()
export class UpdateManyInvoiceArgs {

    @Field(() => InvoiceUpdateManyMutationInput, {nullable:false})
    data!: InvoiceUpdateManyMutationInput;

    @Field(() => InvoiceWhereInput, {nullable:true})
    where?: InvoiceWhereInput;
}
