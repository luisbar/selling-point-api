import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceUpdateInput } from './invoice-update.input';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';

@ArgsType()
export class UpdateOneInvoiceArgs {

    @Field(() => InvoiceUpdateInput, {nullable:false})
    data!: InvoiceUpdateInput;

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;
}
