import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { InvoiceWhereUniqueInput } from './invoice-where-unique.input';
import { InvoiceCreateInput } from './invoice-create.input';
import { InvoiceUpdateInput } from './invoice-update.input';

@ArgsType()
export class UpsertOneInvoiceArgs {

    @Field(() => InvoiceWhereUniqueInput, {nullable:false})
    where!: InvoiceWhereUniqueInput;

    @Field(() => InvoiceCreateInput, {nullable:false})
    create!: InvoiceCreateInput;

    @Field(() => InvoiceUpdateInput, {nullable:false})
    update!: InvoiceUpdateInput;
}
