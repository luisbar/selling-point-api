import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Statuses } from './statuses.enum';

@InputType()
export class EnumStatusesFieldUpdateOperationsInput {

    @Field(() => Statuses, {nullable:true})
    set?: keyof typeof Statuses;
}
