import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Statuses } from './statuses.enum';
import { NestedEnumStatusesFilter } from './nested-enum-statuses-filter.input';

@InputType()
export class EnumStatusesFilter {

    @Field(() => Statuses, {nullable:true})
    equals?: keyof typeof Statuses;

    @Field(() => [Statuses], {nullable:true})
    in?: Array<keyof typeof Statuses>;

    @Field(() => [Statuses], {nullable:true})
    notIn?: Array<keyof typeof Statuses>;

    @Field(() => NestedEnumStatusesFilter, {nullable:true})
    not?: NestedEnumStatusesFilter;
}
