import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Statuses } from './statuses.enum';

@InputType()
export class NestedEnumStatusesFilter {

    @Field(() => Statuses, {nullable:true})
    equals?: keyof typeof Statuses;

    @Field(() => [Statuses], {nullable:true})
    in?: Array<keyof typeof Statuses>;

    @Field(() => [Statuses], {nullable:true})
    notIn?: Array<keyof typeof Statuses>;

    @Field(() => NestedEnumStatusesFilter, {nullable:true})
    not?: NestedEnumStatusesFilter;
}
