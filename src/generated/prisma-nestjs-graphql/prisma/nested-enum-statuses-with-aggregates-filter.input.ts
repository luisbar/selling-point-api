import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Statuses } from './statuses.enum';
import { NestedIntFilter } from './nested-int-filter.input';
import { NestedEnumStatusesFilter } from './nested-enum-statuses-filter.input';

@InputType()
export class NestedEnumStatusesWithAggregatesFilter {

    @Field(() => Statuses, {nullable:true})
    equals?: keyof typeof Statuses;

    @Field(() => [Statuses], {nullable:true})
    in?: Array<keyof typeof Statuses>;

    @Field(() => [Statuses], {nullable:true})
    notIn?: Array<keyof typeof Statuses>;

    @Field(() => NestedEnumStatusesWithAggregatesFilter, {nullable:true})
    not?: NestedEnumStatusesWithAggregatesFilter;

    @Field(() => NestedIntFilter, {nullable:true})
    _count?: NestedIntFilter;

    @Field(() => NestedEnumStatusesFilter, {nullable:true})
    _min?: NestedEnumStatusesFilter;

    @Field(() => NestedEnumStatusesFilter, {nullable:true})
    _max?: NestedEnumStatusesFilter;
}
