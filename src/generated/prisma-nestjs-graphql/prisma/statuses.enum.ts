import { registerEnumType } from '@nestjs/graphql';

export enum Statuses {
    pending = "pending",
    completed = "completed",
    canceled = "canceled"
}


registerEnumType(Statuses, { name: 'Statuses', description: undefined })
