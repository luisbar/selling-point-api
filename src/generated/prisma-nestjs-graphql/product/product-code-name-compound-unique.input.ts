import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';

@InputType()
export class ProductCodeNameCompoundUniqueInput {

    @Field(() => Int, {nullable:false})
    code!: number;

    @Field(() => String, {nullable:false})
    name!: string;
}
