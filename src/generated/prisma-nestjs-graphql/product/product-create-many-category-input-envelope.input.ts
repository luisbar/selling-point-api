import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateManyCategoryInput } from './product-create-many-category.input';

@InputType()
export class ProductCreateManyCategoryInputEnvelope {

    @Field(() => [ProductCreateManyCategoryInput], {nullable:false})
    data!: Array<ProductCreateManyCategoryInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
