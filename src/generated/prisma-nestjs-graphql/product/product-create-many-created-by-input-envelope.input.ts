import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateManyCreatedByInput } from './product-create-many-created-by.input';

@InputType()
export class ProductCreateManyCreatedByInputEnvelope {

    @Field(() => [ProductCreateManyCreatedByInput], {nullable:false})
    data!: Array<ProductCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
