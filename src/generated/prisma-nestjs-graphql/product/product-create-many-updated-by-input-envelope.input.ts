import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateManyUpdatedByInput } from './product-create-many-updated-by.input';

@InputType()
export class ProductCreateManyUpdatedByInputEnvelope {

    @Field(() => [ProductCreateManyUpdatedByInput], {nullable:false})
    data!: Array<ProductCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
