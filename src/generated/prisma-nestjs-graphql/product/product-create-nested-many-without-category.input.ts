import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutCategoryInput } from './product-create-without-category.input';
import { ProductCreateOrConnectWithoutCategoryInput } from './product-create-or-connect-without-category.input';
import { ProductCreateManyCategoryInputEnvelope } from './product-create-many-category-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { Type } from 'class-transformer';
import { ArrayMinSize, IsArray, ValidateNested } from 'class-validator';

@InputType()
export class ProductCreateNestedManyWithoutCategoryInput {

    @Field(() => [ProductCreateWithoutCategoryInput], {nullable:true})
    create?: Array<ProductCreateWithoutCategoryInput>;

    @Field(() => [ProductCreateOrConnectWithoutCategoryInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutCategoryInput>;

    @Field(() => ProductCreateManyCategoryInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyCategoryInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    @Type(() => ProductWhereUniqueInput)
    @ArrayMinSize(1, { message: 'errors.category.product.connect.min.size' })
    @IsArray({ message: 'errors.category.product.connect.is.array' })
    @ValidateNested({ each: true, message: 'errors.category.product.connect.nested' })
    connect?: Array<ProductWhereUniqueInput>;
}
