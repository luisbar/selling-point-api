import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutCreatedByInput } from './product-create-without-created-by.input';
import { ProductCreateOrConnectWithoutCreatedByInput } from './product-create-or-connect-without-created-by.input';
import { ProductCreateManyCreatedByInputEnvelope } from './product-create-many-created-by-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';

@InputType()
export class ProductCreateNestedManyWithoutCreatedByInput {

    @Field(() => [ProductCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<ProductCreateWithoutCreatedByInput>;

    @Field(() => [ProductCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutCreatedByInput>;

    @Field(() => ProductCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyCreatedByInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    connect?: Array<ProductWhereUniqueInput>;
}
