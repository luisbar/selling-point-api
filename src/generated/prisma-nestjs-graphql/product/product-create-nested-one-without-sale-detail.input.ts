import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutSaleDetailInput } from './product-create-without-sale-detail.input';
import { ProductCreateOrConnectWithoutSaleDetailInput } from './product-create-or-connect-without-sale-detail.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';

@InputType()
export class ProductCreateNestedOneWithoutSaleDetailInput {

    @Field(() => ProductCreateWithoutSaleDetailInput, {nullable:true})
    create?: ProductCreateWithoutSaleDetailInput;

    @Field(() => ProductCreateOrConnectWithoutSaleDetailInput, {nullable:true})
    connectOrCreate?: ProductCreateOrConnectWithoutSaleDetailInput;

    @Field(() => ProductWhereUniqueInput, {nullable:true})
    connect?: ProductWhereUniqueInput;
}
