import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductCreateWithoutCategoryInput } from './product-create-without-category.input';

@InputType()
export class ProductCreateOrConnectWithoutCategoryInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductCreateWithoutCategoryInput, {nullable:false})
    create!: ProductCreateWithoutCategoryInput;
}
