import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductCreateWithoutCreatedByInput } from './product-create-without-created-by.input';

@InputType()
export class ProductCreateOrConnectWithoutCreatedByInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductCreateWithoutCreatedByInput, {nullable:false})
    create!: ProductCreateWithoutCreatedByInput;
}
