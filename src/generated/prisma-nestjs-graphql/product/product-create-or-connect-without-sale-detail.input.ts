import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductCreateWithoutSaleDetailInput } from './product-create-without-sale-detail.input';

@InputType()
export class ProductCreateOrConnectWithoutSaleDetailInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductCreateWithoutSaleDetailInput, {nullable:false})
    create!: ProductCreateWithoutSaleDetailInput;
}
