import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductCreateWithoutUpdatedByInput } from './product-create-without-updated-by.input';

@InputType()
export class ProductCreateOrConnectWithoutUpdatedByInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductCreateWithoutUpdatedByInput, {nullable:false})
    create!: ProductCreateWithoutUpdatedByInput;
}
