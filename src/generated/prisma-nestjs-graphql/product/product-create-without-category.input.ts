import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByProductInput } from '../user/user-create-nested-one-without-created-by-product.input';
import { UserCreateNestedOneWithoutUpdatedByProductInput } from '../user/user-create-nested-one-without-updated-by-product.input';
import { SaleDetailCreateNestedManyWithoutProductInput } from '../sale-detail/sale-detail-create-nested-many-without-product.input';

@InputType()
export class ProductCreateWithoutCategoryInput {

    @Field(() => Int, {nullable:true})
    code?: number;

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Float, {nullable:false})
    cost!: number;

    @Field(() => Float, {nullable:false})
    price!: number;

    @Field(() => Boolean, {nullable:true})
    withStock?: boolean;

    @Field(() => String, {nullable:true})
    image?: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByProductInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByProductInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedByProductInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByProductInput;

    @Field(() => SaleDetailCreateNestedManyWithoutProductInput, {nullable:true})
    saleDetail?: SaleDetailCreateNestedManyWithoutProductInput;
}
