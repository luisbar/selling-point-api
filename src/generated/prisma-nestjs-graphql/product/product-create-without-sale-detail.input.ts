import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByProductInput } from '../user/user-create-nested-one-without-created-by-product.input';
import { UserCreateNestedOneWithoutUpdatedByProductInput } from '../user/user-create-nested-one-without-updated-by-product.input';
import { CategoryCreateNestedOneWithoutProductInput } from '../category/category-create-nested-one-without-product.input';

@InputType()
export class ProductCreateWithoutSaleDetailInput {

    @Field(() => Int, {nullable:true})
    code?: number;

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Float, {nullable:false})
    cost!: number;

    @Field(() => Float, {nullable:false})
    price!: number;

    @Field(() => Boolean, {nullable:true})
    withStock?: boolean;

    @Field(() => String, {nullable:true})
    image?: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByProductInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByProductInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedByProductInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByProductInput;

    @Field(() => CategoryCreateNestedOneWithoutProductInput, {nullable:true})
    category?: CategoryCreateNestedOneWithoutProductInput;
}
