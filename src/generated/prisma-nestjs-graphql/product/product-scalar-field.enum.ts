import { registerEnumType } from '@nestjs/graphql';

export enum ProductScalarFieldEnum {
    id = "id",
    code = "code",
    name = "name",
    description = "description",
    cost = "cost",
    price = "price",
    withStock = "withStock",
    image = "image",
    enabled = "enabled",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById",
    categoryId = "categoryId"
}


registerEnumType(ProductScalarFieldEnum, { name: 'ProductScalarFieldEnum', description: undefined })
