import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutCategoryInput } from './product-create-without-category.input';
import { ProductCreateOrConnectWithoutCategoryInput } from './product-create-or-connect-without-category.input';
import { ProductCreateManyCategoryInputEnvelope } from './product-create-many-category-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';

@InputType()
export class ProductUncheckedCreateNestedManyWithoutCategoryInput {

    @Field(() => [ProductCreateWithoutCategoryInput], {nullable:true})
    create?: Array<ProductCreateWithoutCategoryInput>;

    @Field(() => [ProductCreateOrConnectWithoutCategoryInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutCategoryInput>;

    @Field(() => ProductCreateManyCategoryInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyCategoryInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    connect?: Array<ProductWhereUniqueInput>;
}
