import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutUpdatedByInput } from './product-create-without-updated-by.input';
import { ProductCreateOrConnectWithoutUpdatedByInput } from './product-create-or-connect-without-updated-by.input';
import { ProductCreateManyUpdatedByInputEnvelope } from './product-create-many-updated-by-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';

@InputType()
export class ProductUncheckedCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [ProductCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<ProductCreateWithoutUpdatedByInput>;

    @Field(() => [ProductCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => ProductCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyUpdatedByInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    connect?: Array<ProductWhereUniqueInput>;
}
