import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { SaleDetailUncheckedCreateNestedManyWithoutProductInput } from '../sale-detail/sale-detail-unchecked-create-nested-many-without-product.input';

@InputType()
export class ProductUncheckedCreateWithoutCategoryInput {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => Int, {nullable:true})
    code?: number;

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Float, {nullable:false})
    cost!: number;

    @Field(() => Float, {nullable:false})
    price!: number;

    @Field(() => Boolean, {nullable:true})
    withStock?: boolean;

    @Field(() => String, {nullable:true})
    image?: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => SaleDetailUncheckedCreateNestedManyWithoutProductInput, {nullable:true})
    saleDetail?: SaleDetailUncheckedCreateNestedManyWithoutProductInput;
}
