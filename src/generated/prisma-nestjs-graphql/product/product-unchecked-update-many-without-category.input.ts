import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutCategoryInput } from './product-create-without-category.input';
import { ProductCreateOrConnectWithoutCategoryInput } from './product-create-or-connect-without-category.input';
import { ProductUpsertWithWhereUniqueWithoutCategoryInput } from './product-upsert-with-where-unique-without-category.input';
import { ProductCreateManyCategoryInputEnvelope } from './product-create-many-category-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithWhereUniqueWithoutCategoryInput } from './product-update-with-where-unique-without-category.input';
import { ProductUpdateManyWithWhereWithoutCategoryInput } from './product-update-many-with-where-without-category.input';
import { ProductScalarWhereInput } from './product-scalar-where.input';

@InputType()
export class ProductUncheckedUpdateManyWithoutCategoryInput {

    @Field(() => [ProductCreateWithoutCategoryInput], {nullable:true})
    create?: Array<ProductCreateWithoutCategoryInput>;

    @Field(() => [ProductCreateOrConnectWithoutCategoryInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutCategoryInput>;

    @Field(() => [ProductUpsertWithWhereUniqueWithoutCategoryInput], {nullable:true})
    upsert?: Array<ProductUpsertWithWhereUniqueWithoutCategoryInput>;

    @Field(() => ProductCreateManyCategoryInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyCategoryInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    set?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    disconnect?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    delete?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    connect?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductUpdateWithWhereUniqueWithoutCategoryInput], {nullable:true})
    update?: Array<ProductUpdateWithWhereUniqueWithoutCategoryInput>;

    @Field(() => [ProductUpdateManyWithWhereWithoutCategoryInput], {nullable:true})
    updateMany?: Array<ProductUpdateManyWithWhereWithoutCategoryInput>;

    @Field(() => [ProductScalarWhereInput], {nullable:true})
    deleteMany?: Array<ProductScalarWhereInput>;
}
