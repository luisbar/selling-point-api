import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutCreatedByInput } from './product-create-without-created-by.input';
import { ProductCreateOrConnectWithoutCreatedByInput } from './product-create-or-connect-without-created-by.input';
import { ProductUpsertWithWhereUniqueWithoutCreatedByInput } from './product-upsert-with-where-unique-without-created-by.input';
import { ProductCreateManyCreatedByInputEnvelope } from './product-create-many-created-by-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithWhereUniqueWithoutCreatedByInput } from './product-update-with-where-unique-without-created-by.input';
import { ProductUpdateManyWithWhereWithoutCreatedByInput } from './product-update-many-with-where-without-created-by.input';
import { ProductScalarWhereInput } from './product-scalar-where.input';

@InputType()
export class ProductUncheckedUpdateManyWithoutCreatedByInput {

    @Field(() => [ProductCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<ProductCreateWithoutCreatedByInput>;

    @Field(() => [ProductCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [ProductUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<ProductUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => ProductCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyCreatedByInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    set?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    disconnect?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    delete?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    connect?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<ProductUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [ProductUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<ProductUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [ProductScalarWhereInput], {nullable:true})
    deleteMany?: Array<ProductScalarWhereInput>;
}
