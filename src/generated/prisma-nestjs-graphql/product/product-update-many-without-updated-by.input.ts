import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutUpdatedByInput } from './product-create-without-updated-by.input';
import { ProductCreateOrConnectWithoutUpdatedByInput } from './product-create-or-connect-without-updated-by.input';
import { ProductUpsertWithWhereUniqueWithoutUpdatedByInput } from './product-upsert-with-where-unique-without-updated-by.input';
import { ProductCreateManyUpdatedByInputEnvelope } from './product-create-many-updated-by-input-envelope.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithWhereUniqueWithoutUpdatedByInput } from './product-update-with-where-unique-without-updated-by.input';
import { ProductUpdateManyWithWhereWithoutUpdatedByInput } from './product-update-many-with-where-without-updated-by.input';
import { ProductScalarWhereInput } from './product-scalar-where.input';

@InputType()
export class ProductUpdateManyWithoutUpdatedByInput {

    @Field(() => [ProductCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<ProductCreateWithoutUpdatedByInput>;

    @Field(() => [ProductCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<ProductCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [ProductUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<ProductUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => ProductCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: ProductCreateManyUpdatedByInputEnvelope;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    set?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    disconnect?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    delete?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductWhereUniqueInput], {nullable:true})
    connect?: Array<ProductWhereUniqueInput>;

    @Field(() => [ProductUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<ProductUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [ProductUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<ProductUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [ProductScalarWhereInput], {nullable:true})
    deleteMany?: Array<ProductScalarWhereInput>;
}
