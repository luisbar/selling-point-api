import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductCreateWithoutSaleDetailInput } from './product-create-without-sale-detail.input';
import { ProductCreateOrConnectWithoutSaleDetailInput } from './product-create-or-connect-without-sale-detail.input';
import { ProductUpsertWithoutSaleDetailInput } from './product-upsert-without-sale-detail.input';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutSaleDetailInput } from './product-update-without-sale-detail.input';

@InputType()
export class ProductUpdateOneRequiredWithoutSaleDetailInput {

    @Field(() => ProductCreateWithoutSaleDetailInput, {nullable:true})
    create?: ProductCreateWithoutSaleDetailInput;

    @Field(() => ProductCreateOrConnectWithoutSaleDetailInput, {nullable:true})
    connectOrCreate?: ProductCreateOrConnectWithoutSaleDetailInput;

    @Field(() => ProductUpsertWithoutSaleDetailInput, {nullable:true})
    upsert?: ProductUpsertWithoutSaleDetailInput;

    @Field(() => ProductWhereUniqueInput, {nullable:true})
    connect?: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutSaleDetailInput, {nullable:true})
    update?: ProductUpdateWithoutSaleDetailInput;
}
