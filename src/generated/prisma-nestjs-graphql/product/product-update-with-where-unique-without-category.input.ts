import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutCategoryInput } from './product-update-without-category.input';

@InputType()
export class ProductUpdateWithWhereUniqueWithoutCategoryInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutCategoryInput, {nullable:false})
    data!: ProductUpdateWithoutCategoryInput;
}
