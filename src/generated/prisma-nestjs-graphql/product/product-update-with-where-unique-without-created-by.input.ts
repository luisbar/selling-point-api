import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutCreatedByInput } from './product-update-without-created-by.input';

@InputType()
export class ProductUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutCreatedByInput, {nullable:false})
    data!: ProductUpdateWithoutCreatedByInput;
}
