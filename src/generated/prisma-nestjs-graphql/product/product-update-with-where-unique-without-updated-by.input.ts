import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutUpdatedByInput } from './product-update-without-updated-by.input';

@InputType()
export class ProductUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutUpdatedByInput, {nullable:false})
    data!: ProductUpdateWithoutUpdatedByInput;
}
