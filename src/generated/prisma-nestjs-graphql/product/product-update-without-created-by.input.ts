import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { NullableIntFieldUpdateOperationsInput } from '../prisma/nullable-int-field-update-operations.input';
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input';
import { FloatFieldUpdateOperationsInput } from '../prisma/float-field-update-operations.input';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { NullableStringFieldUpdateOperationsInput } from '../prisma/nullable-string-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutUpdatedByProductInput } from '../user/user-update-one-without-updated-by-product.input';
import { CategoryUpdateOneWithoutProductInput } from '../category/category-update-one-without-product.input';
import { SaleDetailUpdateManyWithoutProductInput } from '../sale-detail/sale-detail-update-many-without-product.input';

@InputType()
export class ProductUpdateWithoutCreatedByInput {

    @Field(() => NullableIntFieldUpdateOperationsInput, {nullable:true})
    code?: NullableIntFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    name?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    description?: StringFieldUpdateOperationsInput;

    @Field(() => FloatFieldUpdateOperationsInput, {nullable:true})
    cost?: FloatFieldUpdateOperationsInput;

    @Field(() => FloatFieldUpdateOperationsInput, {nullable:true})
    price?: FloatFieldUpdateOperationsInput;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    withStock?: BoolFieldUpdateOperationsInput;

    @Field(() => NullableStringFieldUpdateOperationsInput, {nullable:true})
    image?: NullableStringFieldUpdateOperationsInput;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    enabled?: BoolFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutUpdatedByProductInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedByProductInput;

    @Field(() => CategoryUpdateOneWithoutProductInput, {nullable:true})
    category?: CategoryUpdateOneWithoutProductInput;

    @Field(() => SaleDetailUpdateManyWithoutProductInput, {nullable:true})
    saleDetail?: SaleDetailUpdateManyWithoutProductInput;
}
