import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutCategoryInput } from './product-update-without-category.input';
import { ProductCreateWithoutCategoryInput } from './product-create-without-category.input';

@InputType()
export class ProductUpsertWithWhereUniqueWithoutCategoryInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutCategoryInput, {nullable:false})
    update!: ProductUpdateWithoutCategoryInput;

    @Field(() => ProductCreateWithoutCategoryInput, {nullable:false})
    create!: ProductCreateWithoutCategoryInput;
}
