import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutCreatedByInput } from './product-update-without-created-by.input';
import { ProductCreateWithoutCreatedByInput } from './product-create-without-created-by.input';

@InputType()
export class ProductUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutCreatedByInput, {nullable:false})
    update!: ProductUpdateWithoutCreatedByInput;

    @Field(() => ProductCreateWithoutCreatedByInput, {nullable:false})
    create!: ProductCreateWithoutCreatedByInput;
}
