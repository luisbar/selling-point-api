import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductWhereUniqueInput } from './product-where-unique.input';
import { ProductUpdateWithoutUpdatedByInput } from './product-update-without-updated-by.input';
import { ProductCreateWithoutUpdatedByInput } from './product-create-without-updated-by.input';

@InputType()
export class ProductUpsertWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => ProductWhereUniqueInput, {nullable:false})
    where!: ProductWhereUniqueInput;

    @Field(() => ProductUpdateWithoutUpdatedByInput, {nullable:false})
    update!: ProductUpdateWithoutUpdatedByInput;

    @Field(() => ProductCreateWithoutUpdatedByInput, {nullable:false})
    create!: ProductCreateWithoutUpdatedByInput;
}
