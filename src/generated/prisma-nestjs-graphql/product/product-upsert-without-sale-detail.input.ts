import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ProductUpdateWithoutSaleDetailInput } from './product-update-without-sale-detail.input';
import { ProductCreateWithoutSaleDetailInput } from './product-create-without-sale-detail.input';

@InputType()
export class ProductUpsertWithoutSaleDetailInput {

    @Field(() => ProductUpdateWithoutSaleDetailInput, {nullable:false})
    update!: ProductUpdateWithoutSaleDetailInput;

    @Field(() => ProductCreateWithoutSaleDetailInput, {nullable:false})
    create!: ProductCreateWithoutSaleDetailInput;
}
