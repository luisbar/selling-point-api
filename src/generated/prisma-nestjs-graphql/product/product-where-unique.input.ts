import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { IsInt } from 'class-validator';
import { ProductCodeNameCompoundUniqueInput } from './product-code-name-compound-unique.input';

@InputType()
export class ProductWhereUniqueInput {
    
    @IsInt({
        message: 'errors.category.product.connect.id.is.int',
    })
    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => ProductCodeNameCompoundUniqueInput, {nullable:true})
    code_name?: ProductCodeNameCompoundUniqueInput;
}
