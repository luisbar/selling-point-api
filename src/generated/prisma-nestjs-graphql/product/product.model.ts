import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { User } from '../user/user.model';
import { Category } from '../category/category.model';
import { SaleDetail } from '../sale-detail/sale-detail.model';
import { ProductCount } from './product-count.output';

@ObjectType()
export class Product {

    @Field(() => ID, {nullable:false})
    id!: number;

    @Field(() => Int, {nullable:true})
    code!: number | null;

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => String, {nullable:false})
    description!: string;

    @Field(() => Float, {nullable:false})
    cost!: number;

    @Field(() => Float, {nullable:false})
    price!: number;

    @Field(() => Boolean, {nullable:false,defaultValue:false})
    withStock!: boolean;

    @Field(() => String, {nullable:true})
    image!: string | null;

    @Field(() => Boolean, {nullable:false,defaultValue:true})
    enabled!: boolean;

    @Field(() => Date, {nullable:false})
    createdAt!: Date;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date;

    @Field(() => User, {nullable:true})
    createdBy?: User | null;

    @Field(() => Int, {nullable:true})
    createdById!: number | null;

    @Field(() => User, {nullable:true})
    updatedBy?: User | null;

    @Field(() => Int, {nullable:true})
    updatedById!: number | null;

    @Field(() => Category, {nullable:true})
    category?: Category | null;

    @Field(() => Int, {nullable:true})
    categoryId!: number | null;

    @Field(() => [SaleDetail], {nullable:true})
    saleDetail?: Array<SaleDetail>;

    @Field(() => ProductCount, {nullable:false})
    _count?: ProductCount;
}
