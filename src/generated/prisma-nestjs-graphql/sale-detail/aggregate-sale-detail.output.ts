import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { SaleDetailCountAggregate } from './sale-detail-count-aggregate.output';
import { SaleDetailAvgAggregate } from './sale-detail-avg-aggregate.output';
import { SaleDetailSumAggregate } from './sale-detail-sum-aggregate.output';
import { SaleDetailMinAggregate } from './sale-detail-min-aggregate.output';
import { SaleDetailMaxAggregate } from './sale-detail-max-aggregate.output';

@ObjectType()
export class AggregateSaleDetail {

    @Field(() => SaleDetailCountAggregate, {nullable:true})
    _count?: SaleDetailCountAggregate;

    @Field(() => SaleDetailAvgAggregate, {nullable:true})
    _avg?: SaleDetailAvgAggregate;

    @Field(() => SaleDetailSumAggregate, {nullable:true})
    _sum?: SaleDetailSumAggregate;

    @Field(() => SaleDetailMinAggregate, {nullable:true})
    _min?: SaleDetailMinAggregate;

    @Field(() => SaleDetailMaxAggregate, {nullable:true})
    _max?: SaleDetailMaxAggregate;
}
