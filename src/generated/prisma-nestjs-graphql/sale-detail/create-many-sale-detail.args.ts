import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailCreateManyInput } from './sale-detail-create-many.input';

@ArgsType()
export class CreateManySaleDetailArgs {

    @Field(() => [SaleDetailCreateManyInput], {nullable:false})
    data!: Array<SaleDetailCreateManyInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
