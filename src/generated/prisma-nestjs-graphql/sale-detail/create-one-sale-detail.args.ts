import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailCreateInput } from './sale-detail-create.input';

@ArgsType()
export class CreateOneSaleDetailArgs {

    @Field(() => SaleDetailCreateInput, {nullable:false})
    data!: SaleDetailCreateInput;
}
