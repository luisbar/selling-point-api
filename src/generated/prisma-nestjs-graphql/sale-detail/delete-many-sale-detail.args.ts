import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailWhereInput } from './sale-detail-where.input';

@ArgsType()
export class DeleteManySaleDetailArgs {

    @Field(() => SaleDetailWhereInput, {nullable:true})
    where?: SaleDetailWhereInput;
}
