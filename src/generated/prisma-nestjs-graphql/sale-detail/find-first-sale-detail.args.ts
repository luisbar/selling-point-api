import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailWhereInput } from './sale-detail-where.input';
import { SaleDetailOrderByWithRelationInput } from './sale-detail-order-by-with-relation.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { Int } from '@nestjs/graphql';
import { SaleDetailScalarFieldEnum } from './sale-detail-scalar-field.enum';

@ArgsType()
export class FindFirstSaleDetailArgs {

    @Field(() => SaleDetailWhereInput, {nullable:true})
    where?: SaleDetailWhereInput;

    @Field(() => [SaleDetailOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<SaleDetailOrderByWithRelationInput>;

    @Field(() => SaleDetailWhereUniqueInput, {nullable:true})
    cursor?: SaleDetailWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => [SaleDetailScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof SaleDetailScalarFieldEnum>;
}
