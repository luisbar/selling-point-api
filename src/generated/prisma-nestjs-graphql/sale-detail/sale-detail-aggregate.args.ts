import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailWhereInput } from './sale-detail-where.input';
import { SaleDetailOrderByWithRelationInput } from './sale-detail-order-by-with-relation.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { Int } from '@nestjs/graphql';
import { SaleDetailCountAggregateInput } from './sale-detail-count-aggregate.input';
import { SaleDetailAvgAggregateInput } from './sale-detail-avg-aggregate.input';
import { SaleDetailSumAggregateInput } from './sale-detail-sum-aggregate.input';
import { SaleDetailMinAggregateInput } from './sale-detail-min-aggregate.input';
import { SaleDetailMaxAggregateInput } from './sale-detail-max-aggregate.input';

@ArgsType()
export class SaleDetailAggregateArgs {

    @Field(() => SaleDetailWhereInput, {nullable:true})
    where?: SaleDetailWhereInput;

    @Field(() => [SaleDetailOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<SaleDetailOrderByWithRelationInput>;

    @Field(() => SaleDetailWhereUniqueInput, {nullable:true})
    cursor?: SaleDetailWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => SaleDetailCountAggregateInput, {nullable:true})
    _count?: SaleDetailCountAggregateInput;

    @Field(() => SaleDetailAvgAggregateInput, {nullable:true})
    _avg?: SaleDetailAvgAggregateInput;

    @Field(() => SaleDetailSumAggregateInput, {nullable:true})
    _sum?: SaleDetailSumAggregateInput;

    @Field(() => SaleDetailMinAggregateInput, {nullable:true})
    _min?: SaleDetailMinAggregateInput;

    @Field(() => SaleDetailMaxAggregateInput, {nullable:true})
    _max?: SaleDetailMaxAggregateInput;
}
