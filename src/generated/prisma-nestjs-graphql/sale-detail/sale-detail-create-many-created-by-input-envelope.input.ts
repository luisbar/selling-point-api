import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateManyCreatedByInput } from './sale-detail-create-many-created-by.input';

@InputType()
export class SaleDetailCreateManyCreatedByInputEnvelope {

    @Field(() => [SaleDetailCreateManyCreatedByInput], {nullable:false})
    data!: Array<SaleDetailCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
