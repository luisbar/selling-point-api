import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateManyProductInput } from './sale-detail-create-many-product.input';

@InputType()
export class SaleDetailCreateManyProductInputEnvelope {

    @Field(() => [SaleDetailCreateManyProductInput], {nullable:false})
    data!: Array<SaleDetailCreateManyProductInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
