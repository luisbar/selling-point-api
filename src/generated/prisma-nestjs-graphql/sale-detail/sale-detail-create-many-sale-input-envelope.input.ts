import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateManySaleInput } from './sale-detail-create-many-sale.input';

@InputType()
export class SaleDetailCreateManySaleInputEnvelope {

    @Field(() => [SaleDetailCreateManySaleInput], {nullable:false})
    data!: Array<SaleDetailCreateManySaleInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
