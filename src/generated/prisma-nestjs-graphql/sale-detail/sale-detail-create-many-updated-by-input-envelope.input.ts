import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateManyUpdatedByInput } from './sale-detail-create-many-updated-by.input';

@InputType()
export class SaleDetailCreateManyUpdatedByInputEnvelope {

    @Field(() => [SaleDetailCreateManyUpdatedByInput], {nullable:false})
    data!: Array<SaleDetailCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
