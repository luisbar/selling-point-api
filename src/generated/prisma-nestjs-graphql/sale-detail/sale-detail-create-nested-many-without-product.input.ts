import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutProductInput } from './sale-detail-create-without-product.input';
import { SaleDetailCreateOrConnectWithoutProductInput } from './sale-detail-create-or-connect-without-product.input';
import { SaleDetailCreateManyProductInputEnvelope } from './sale-detail-create-many-product-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';

@InputType()
export class SaleDetailCreateNestedManyWithoutProductInput {

    @Field(() => [SaleDetailCreateWithoutProductInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutProductInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutProductInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutProductInput>;

    @Field(() => SaleDetailCreateManyProductInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManyProductInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;
}
