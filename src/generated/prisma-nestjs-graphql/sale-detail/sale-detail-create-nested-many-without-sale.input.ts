import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutSaleInput } from './sale-detail-create-without-sale.input';
import { SaleDetailCreateOrConnectWithoutSaleInput } from './sale-detail-create-or-connect-without-sale.input';
import { SaleDetailCreateManySaleInputEnvelope } from './sale-detail-create-many-sale-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';

@InputType()
export class SaleDetailCreateNestedManyWithoutSaleInput {

    @Field(() => [SaleDetailCreateWithoutSaleInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutSaleInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutSaleInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutSaleInput>;

    @Field(() => SaleDetailCreateManySaleInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManySaleInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;
}
