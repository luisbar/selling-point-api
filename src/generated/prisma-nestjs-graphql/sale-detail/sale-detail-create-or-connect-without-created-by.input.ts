import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailCreateWithoutCreatedByInput } from './sale-detail-create-without-created-by.input';

@InputType()
export class SaleDetailCreateOrConnectWithoutCreatedByInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailCreateWithoutCreatedByInput, {nullable:false})
    create!: SaleDetailCreateWithoutCreatedByInput;
}
