import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailCreateWithoutProductInput } from './sale-detail-create-without-product.input';

@InputType()
export class SaleDetailCreateOrConnectWithoutProductInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailCreateWithoutProductInput, {nullable:false})
    create!: SaleDetailCreateWithoutProductInput;
}
