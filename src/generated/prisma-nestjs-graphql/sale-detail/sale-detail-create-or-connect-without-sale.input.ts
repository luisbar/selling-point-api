import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailCreateWithoutSaleInput } from './sale-detail-create-without-sale.input';

@InputType()
export class SaleDetailCreateOrConnectWithoutSaleInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailCreateWithoutSaleInput, {nullable:false})
    create!: SaleDetailCreateWithoutSaleInput;
}
