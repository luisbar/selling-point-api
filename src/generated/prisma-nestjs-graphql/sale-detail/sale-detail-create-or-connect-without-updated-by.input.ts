import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailCreateWithoutUpdatedByInput } from './sale-detail-create-without-updated-by.input';

@InputType()
export class SaleDetailCreateOrConnectWithoutUpdatedByInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailCreateWithoutUpdatedByInput, {nullable:false})
    create!: SaleDetailCreateWithoutUpdatedByInput;
}
