import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedBySaleDetailInput } from '../user/user-create-nested-one-without-created-by-sale-detail.input';
import { UserCreateNestedOneWithoutUpdatedBySaleDetailInput } from '../user/user-create-nested-one-without-updated-by-sale-detail.input';
import { ProductCreateNestedOneWithoutSaleDetailInput } from '../product/product-create-nested-one-without-sale-detail.input';

@InputType()
export class SaleDetailCreateWithoutSaleInput {

    @Field(() => Int, {nullable:false})
    quantity!: number;

    @Field(() => Float, {nullable:false})
    subtotal!: number;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedBySaleDetailInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedBySaleDetailInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedBySaleDetailInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedBySaleDetailInput;

    @Field(() => ProductCreateNestedOneWithoutSaleDetailInput, {nullable:false})
    product!: ProductCreateNestedOneWithoutSaleDetailInput;
}
