import { Field, InputType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailWhereInput } from './sale-detail-where.input';
import { SaleDetailOrderByWithAggregationInput } from './sale-detail-order-by-with-aggregation.input';
import { SaleDetailScalarFieldEnum } from './sale-detail-scalar-field.enum';
import { SaleDetailScalarWhereWithAggregatesInput } from './sale-detail-scalar-where-with-aggregates.input';
import { Int } from '@nestjs/graphql';
import { SaleDetailCountAggregateInput } from './sale-detail-count-aggregate.input';
import { SaleDetailAvgAggregateInput } from './sale-detail-avg-aggregate.input';
import { SaleDetailSumAggregateInput } from './sale-detail-sum-aggregate.input';
import { SaleDetailMinAggregateInput } from './sale-detail-min-aggregate.input';
import { SaleDetailMaxAggregateInput } from './sale-detail-max-aggregate.input';

@InputType()
@ArgsType()
export class SaleDetailGroupByArgs {

    @Field(() => SaleDetailWhereInput, {nullable:true})
    where?: SaleDetailWhereInput;

    @Field(() => [SaleDetailOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<SaleDetailOrderByWithAggregationInput>;

    @Field(() => [SaleDetailScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof SaleDetailScalarFieldEnum>;

    @Field(() => SaleDetailScalarWhereWithAggregatesInput, {nullable:true})
    having?: SaleDetailScalarWhereWithAggregatesInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => SaleDetailCountAggregateInput, {nullable:true})
    _count?: SaleDetailCountAggregateInput;

    @Field(() => SaleDetailAvgAggregateInput, {nullable:true})
    _avg?: SaleDetailAvgAggregateInput;

    @Field(() => SaleDetailSumAggregateInput, {nullable:true})
    _sum?: SaleDetailSumAggregateInput;

    @Field(() => SaleDetailMinAggregateInput, {nullable:true})
    _min?: SaleDetailMinAggregateInput;

    @Field(() => SaleDetailMaxAggregateInput, {nullable:true})
    _max?: SaleDetailMaxAggregateInput;
}
