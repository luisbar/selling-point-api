import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { SaleDetailCountAggregate } from './sale-detail-count-aggregate.output';
import { SaleDetailAvgAggregate } from './sale-detail-avg-aggregate.output';
import { SaleDetailSumAggregate } from './sale-detail-sum-aggregate.output';
import { SaleDetailMinAggregate } from './sale-detail-min-aggregate.output';
import { SaleDetailMaxAggregate } from './sale-detail-max-aggregate.output';

@ObjectType()
export class SaleDetailGroupBy {

    @Field(() => Int, {nullable:false})
    id!: number;

    @Field(() => Int, {nullable:false})
    quantity!: number;

    @Field(() => Float, {nullable:false})
    subtotal!: number;

    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => Int, {nullable:false})
    productId!: number;

    @Field(() => Int, {nullable:false})
    saleId!: number;

    @Field(() => SaleDetailCountAggregate, {nullable:true})
    _count?: SaleDetailCountAggregate;

    @Field(() => SaleDetailAvgAggregate, {nullable:true})
    _avg?: SaleDetailAvgAggregate;

    @Field(() => SaleDetailSumAggregate, {nullable:true})
    _sum?: SaleDetailSumAggregate;

    @Field(() => SaleDetailMinAggregate, {nullable:true})
    _min?: SaleDetailMinAggregate;

    @Field(() => SaleDetailMaxAggregate, {nullable:true})
    _max?: SaleDetailMaxAggregate;
}
