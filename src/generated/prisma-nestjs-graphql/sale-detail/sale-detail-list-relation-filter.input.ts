import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereInput } from './sale-detail-where.input';

@InputType()
export class SaleDetailListRelationFilter {

    @Field(() => SaleDetailWhereInput, {nullable:true})
    every?: SaleDetailWhereInput;

    @Field(() => SaleDetailWhereInput, {nullable:true})
    some?: SaleDetailWhereInput;

    @Field(() => SaleDetailWhereInput, {nullable:true})
    none?: SaleDetailWhereInput;
}
