import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { SaleDetailCountOrderByAggregateInput } from './sale-detail-count-order-by-aggregate.input';
import { SaleDetailAvgOrderByAggregateInput } from './sale-detail-avg-order-by-aggregate.input';
import { SaleDetailMaxOrderByAggregateInput } from './sale-detail-max-order-by-aggregate.input';
import { SaleDetailMinOrderByAggregateInput } from './sale-detail-min-order-by-aggregate.input';
import { SaleDetailSumOrderByAggregateInput } from './sale-detail-sum-order-by-aggregate.input';

@InputType()
export class SaleDetailOrderByWithAggregationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    subtotal?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    productId?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    saleId?: keyof typeof SortOrder;

    @Field(() => SaleDetailCountOrderByAggregateInput, {nullable:true})
    _count?: SaleDetailCountOrderByAggregateInput;

    @Field(() => SaleDetailAvgOrderByAggregateInput, {nullable:true})
    _avg?: SaleDetailAvgOrderByAggregateInput;

    @Field(() => SaleDetailMaxOrderByAggregateInput, {nullable:true})
    _max?: SaleDetailMaxOrderByAggregateInput;

    @Field(() => SaleDetailMinOrderByAggregateInput, {nullable:true})
    _min?: SaleDetailMinOrderByAggregateInput;

    @Field(() => SaleDetailSumOrderByAggregateInput, {nullable:true})
    _sum?: SaleDetailSumOrderByAggregateInput;
}
