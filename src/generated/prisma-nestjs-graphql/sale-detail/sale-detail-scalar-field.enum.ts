import { registerEnumType } from '@nestjs/graphql';

export enum SaleDetailScalarFieldEnum {
    id = "id",
    quantity = "quantity",
    subtotal = "subtotal",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById",
    productId = "productId",
    saleId = "saleId"
}


registerEnumType(SaleDetailScalarFieldEnum, { name: 'SaleDetailScalarFieldEnum', description: undefined })
