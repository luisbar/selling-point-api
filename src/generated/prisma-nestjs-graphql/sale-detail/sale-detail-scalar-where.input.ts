import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { FloatFilter } from '../prisma/float-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';

@InputType()
export class SaleDetailScalarWhereInput {

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    AND?: Array<SaleDetailScalarWhereInput>;

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    OR?: Array<SaleDetailScalarWhereInput>;

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    NOT?: Array<SaleDetailScalarWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => IntFilter, {nullable:true})
    quantity?: IntFilter;

    @Field(() => FloatFilter, {nullable:true})
    subtotal?: FloatFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => IntFilter, {nullable:true})
    productId?: IntFilter;

    @Field(() => IntFilter, {nullable:true})
    saleId?: IntFilter;
}
