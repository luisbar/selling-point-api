import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';

@ObjectType()
export class SaleDetailSumAggregate {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => Int, {nullable:true})
    quantity?: number;

    @Field(() => Float, {nullable:true})
    subtotal?: number;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => Int, {nullable:true})
    productId?: number;

    @Field(() => Int, {nullable:true})
    saleId?: number;
}
