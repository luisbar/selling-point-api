import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutCreatedByInput } from './sale-detail-create-without-created-by.input';
import { SaleDetailCreateOrConnectWithoutCreatedByInput } from './sale-detail-create-or-connect-without-created-by.input';
import { SaleDetailCreateManyCreatedByInputEnvelope } from './sale-detail-create-many-created-by-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';

@InputType()
export class SaleDetailUncheckedCreateNestedManyWithoutCreatedByInput {

    @Field(() => [SaleDetailCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutCreatedByInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutCreatedByInput>;

    @Field(() => SaleDetailCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManyCreatedByInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;
}
