import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutUpdatedByInput } from './sale-detail-create-without-updated-by.input';
import { SaleDetailCreateOrConnectWithoutUpdatedByInput } from './sale-detail-create-or-connect-without-updated-by.input';
import { SaleDetailCreateManyUpdatedByInputEnvelope } from './sale-detail-create-many-updated-by-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';

@InputType()
export class SaleDetailUncheckedCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [SaleDetailCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutUpdatedByInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => SaleDetailCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManyUpdatedByInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;
}
