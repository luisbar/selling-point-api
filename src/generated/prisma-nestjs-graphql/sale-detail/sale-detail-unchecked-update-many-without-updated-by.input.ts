import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutUpdatedByInput } from './sale-detail-create-without-updated-by.input';
import { SaleDetailCreateOrConnectWithoutUpdatedByInput } from './sale-detail-create-or-connect-without-updated-by.input';
import { SaleDetailUpsertWithWhereUniqueWithoutUpdatedByInput } from './sale-detail-upsert-with-where-unique-without-updated-by.input';
import { SaleDetailCreateManyUpdatedByInputEnvelope } from './sale-detail-create-many-updated-by-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithWhereUniqueWithoutUpdatedByInput } from './sale-detail-update-with-where-unique-without-updated-by.input';
import { SaleDetailUpdateManyWithWhereWithoutUpdatedByInput } from './sale-detail-update-many-with-where-without-updated-by.input';
import { SaleDetailScalarWhereInput } from './sale-detail-scalar-where.input';

@InputType()
export class SaleDetailUncheckedUpdateManyWithoutUpdatedByInput {

    @Field(() => [SaleDetailCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutUpdatedByInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [SaleDetailUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<SaleDetailUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => SaleDetailCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManyUpdatedByInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    set?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    delete?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<SaleDetailUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [SaleDetailUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<SaleDetailUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleDetailScalarWhereInput>;
}
