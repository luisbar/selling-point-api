import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailScalarWhereInput } from './sale-detail-scalar-where.input';
import { SaleDetailUpdateManyMutationInput } from './sale-detail-update-many-mutation.input';

@InputType()
export class SaleDetailUpdateManyWithWhereWithoutSaleInput {

    @Field(() => SaleDetailScalarWhereInput, {nullable:false})
    where!: SaleDetailScalarWhereInput;

    @Field(() => SaleDetailUpdateManyMutationInput, {nullable:false})
    data!: SaleDetailUpdateManyMutationInput;
}
