import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutCreatedByInput } from './sale-detail-create-without-created-by.input';
import { SaleDetailCreateOrConnectWithoutCreatedByInput } from './sale-detail-create-or-connect-without-created-by.input';
import { SaleDetailUpsertWithWhereUniqueWithoutCreatedByInput } from './sale-detail-upsert-with-where-unique-without-created-by.input';
import { SaleDetailCreateManyCreatedByInputEnvelope } from './sale-detail-create-many-created-by-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithWhereUniqueWithoutCreatedByInput } from './sale-detail-update-with-where-unique-without-created-by.input';
import { SaleDetailUpdateManyWithWhereWithoutCreatedByInput } from './sale-detail-update-many-with-where-without-created-by.input';
import { SaleDetailScalarWhereInput } from './sale-detail-scalar-where.input';

@InputType()
export class SaleDetailUpdateManyWithoutCreatedByInput {

    @Field(() => [SaleDetailCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutCreatedByInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [SaleDetailUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<SaleDetailUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => SaleDetailCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManyCreatedByInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    set?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    delete?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<SaleDetailUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [SaleDetailUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<SaleDetailUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleDetailScalarWhereInput>;
}
