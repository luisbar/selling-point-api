import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutProductInput } from './sale-detail-create-without-product.input';
import { SaleDetailCreateOrConnectWithoutProductInput } from './sale-detail-create-or-connect-without-product.input';
import { SaleDetailUpsertWithWhereUniqueWithoutProductInput } from './sale-detail-upsert-with-where-unique-without-product.input';
import { SaleDetailCreateManyProductInputEnvelope } from './sale-detail-create-many-product-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithWhereUniqueWithoutProductInput } from './sale-detail-update-with-where-unique-without-product.input';
import { SaleDetailUpdateManyWithWhereWithoutProductInput } from './sale-detail-update-many-with-where-without-product.input';
import { SaleDetailScalarWhereInput } from './sale-detail-scalar-where.input';

@InputType()
export class SaleDetailUpdateManyWithoutProductInput {

    @Field(() => [SaleDetailCreateWithoutProductInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutProductInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutProductInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutProductInput>;

    @Field(() => [SaleDetailUpsertWithWhereUniqueWithoutProductInput], {nullable:true})
    upsert?: Array<SaleDetailUpsertWithWhereUniqueWithoutProductInput>;

    @Field(() => SaleDetailCreateManyProductInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManyProductInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    set?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    delete?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailUpdateWithWhereUniqueWithoutProductInput], {nullable:true})
    update?: Array<SaleDetailUpdateWithWhereUniqueWithoutProductInput>;

    @Field(() => [SaleDetailUpdateManyWithWhereWithoutProductInput], {nullable:true})
    updateMany?: Array<SaleDetailUpdateManyWithWhereWithoutProductInput>;

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleDetailScalarWhereInput>;
}
