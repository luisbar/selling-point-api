import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailCreateWithoutSaleInput } from './sale-detail-create-without-sale.input';
import { SaleDetailCreateOrConnectWithoutSaleInput } from './sale-detail-create-or-connect-without-sale.input';
import { SaleDetailUpsertWithWhereUniqueWithoutSaleInput } from './sale-detail-upsert-with-where-unique-without-sale.input';
import { SaleDetailCreateManySaleInputEnvelope } from './sale-detail-create-many-sale-input-envelope.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithWhereUniqueWithoutSaleInput } from './sale-detail-update-with-where-unique-without-sale.input';
import { SaleDetailUpdateManyWithWhereWithoutSaleInput } from './sale-detail-update-many-with-where-without-sale.input';
import { SaleDetailScalarWhereInput } from './sale-detail-scalar-where.input';

@InputType()
export class SaleDetailUpdateManyWithoutSaleInput {

    @Field(() => [SaleDetailCreateWithoutSaleInput], {nullable:true})
    create?: Array<SaleDetailCreateWithoutSaleInput>;

    @Field(() => [SaleDetailCreateOrConnectWithoutSaleInput], {nullable:true})
    connectOrCreate?: Array<SaleDetailCreateOrConnectWithoutSaleInput>;

    @Field(() => [SaleDetailUpsertWithWhereUniqueWithoutSaleInput], {nullable:true})
    upsert?: Array<SaleDetailUpsertWithWhereUniqueWithoutSaleInput>;

    @Field(() => SaleDetailCreateManySaleInputEnvelope, {nullable:true})
    createMany?: SaleDetailCreateManySaleInputEnvelope;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    set?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    delete?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailWhereUniqueInput], {nullable:true})
    connect?: Array<SaleDetailWhereUniqueInput>;

    @Field(() => [SaleDetailUpdateWithWhereUniqueWithoutSaleInput], {nullable:true})
    update?: Array<SaleDetailUpdateWithWhereUniqueWithoutSaleInput>;

    @Field(() => [SaleDetailUpdateManyWithWhereWithoutSaleInput], {nullable:true})
    updateMany?: Array<SaleDetailUpdateManyWithWhereWithoutSaleInput>;

    @Field(() => [SaleDetailScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleDetailScalarWhereInput>;
}
