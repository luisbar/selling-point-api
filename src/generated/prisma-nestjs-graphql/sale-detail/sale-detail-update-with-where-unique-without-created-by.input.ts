import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutCreatedByInput } from './sale-detail-update-without-created-by.input';

@InputType()
export class SaleDetailUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutCreatedByInput, {nullable:false})
    data!: SaleDetailUpdateWithoutCreatedByInput;
}
