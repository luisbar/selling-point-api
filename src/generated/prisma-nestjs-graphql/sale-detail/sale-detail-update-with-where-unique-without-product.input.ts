import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutProductInput } from './sale-detail-update-without-product.input';

@InputType()
export class SaleDetailUpdateWithWhereUniqueWithoutProductInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutProductInput, {nullable:false})
    data!: SaleDetailUpdateWithoutProductInput;
}
