import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutSaleInput } from './sale-detail-update-without-sale.input';

@InputType()
export class SaleDetailUpdateWithWhereUniqueWithoutSaleInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutSaleInput, {nullable:false})
    data!: SaleDetailUpdateWithoutSaleInput;
}
