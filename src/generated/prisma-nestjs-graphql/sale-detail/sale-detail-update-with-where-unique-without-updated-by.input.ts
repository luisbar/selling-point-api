import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutUpdatedByInput } from './sale-detail-update-without-updated-by.input';

@InputType()
export class SaleDetailUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutUpdatedByInput, {nullable:false})
    data!: SaleDetailUpdateWithoutUpdatedByInput;
}
