import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFieldUpdateOperationsInput } from '../prisma/int-field-update-operations.input';
import { FloatFieldUpdateOperationsInput } from '../prisma/float-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutCreatedBySaleDetailInput } from '../user/user-update-one-without-created-by-sale-detail.input';
import { UserUpdateOneWithoutUpdatedBySaleDetailInput } from '../user/user-update-one-without-updated-by-sale-detail.input';
import { SaleUpdateOneRequiredWithoutSaleDetailInput } from '../sale/sale-update-one-required-without-sale-detail.input';

@InputType()
export class SaleDetailUpdateWithoutProductInput {

    @Field(() => IntFieldUpdateOperationsInput, {nullable:true})
    quantity?: IntFieldUpdateOperationsInput;

    @Field(() => FloatFieldUpdateOperationsInput, {nullable:true})
    subtotal?: FloatFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutCreatedBySaleDetailInput, {nullable:true})
    createdBy?: UserUpdateOneWithoutCreatedBySaleDetailInput;

    @Field(() => UserUpdateOneWithoutUpdatedBySaleDetailInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedBySaleDetailInput;

    @Field(() => SaleUpdateOneRequiredWithoutSaleDetailInput, {nullable:true})
    sale?: SaleUpdateOneRequiredWithoutSaleDetailInput;
}
