import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutCreatedByInput } from './sale-detail-update-without-created-by.input';
import { SaleDetailCreateWithoutCreatedByInput } from './sale-detail-create-without-created-by.input';

@InputType()
export class SaleDetailUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutCreatedByInput, {nullable:false})
    update!: SaleDetailUpdateWithoutCreatedByInput;

    @Field(() => SaleDetailCreateWithoutCreatedByInput, {nullable:false})
    create!: SaleDetailCreateWithoutCreatedByInput;
}
