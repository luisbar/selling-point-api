import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutProductInput } from './sale-detail-update-without-product.input';
import { SaleDetailCreateWithoutProductInput } from './sale-detail-create-without-product.input';

@InputType()
export class SaleDetailUpsertWithWhereUniqueWithoutProductInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutProductInput, {nullable:false})
    update!: SaleDetailUpdateWithoutProductInput;

    @Field(() => SaleDetailCreateWithoutProductInput, {nullable:false})
    create!: SaleDetailCreateWithoutProductInput;
}
