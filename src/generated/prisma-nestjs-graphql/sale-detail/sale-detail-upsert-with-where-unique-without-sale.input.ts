import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailUpdateWithoutSaleInput } from './sale-detail-update-without-sale.input';
import { SaleDetailCreateWithoutSaleInput } from './sale-detail-create-without-sale.input';

@InputType()
export class SaleDetailUpsertWithWhereUniqueWithoutSaleInput {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailUpdateWithoutSaleInput, {nullable:false})
    update!: SaleDetailUpdateWithoutSaleInput;

    @Field(() => SaleDetailCreateWithoutSaleInput, {nullable:false})
    create!: SaleDetailCreateWithoutSaleInput;
}
