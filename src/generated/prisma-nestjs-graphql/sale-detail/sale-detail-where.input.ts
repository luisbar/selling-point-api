import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { FloatFilter } from '../prisma/float-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { UserRelationFilter } from '../user/user-relation-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';
import { ProductRelationFilter } from '../product/product-relation-filter.input';
import { SaleRelationFilter } from '../sale/sale-relation-filter.input';

@InputType()
export class SaleDetailWhereInput {

    @Field(() => [SaleDetailWhereInput], {nullable:true})
    AND?: Array<SaleDetailWhereInput>;

    @Field(() => [SaleDetailWhereInput], {nullable:true})
    OR?: Array<SaleDetailWhereInput>;

    @Field(() => [SaleDetailWhereInput], {nullable:true})
    NOT?: Array<SaleDetailWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => IntFilter, {nullable:true})
    quantity?: IntFilter;

    @Field(() => FloatFilter, {nullable:true})
    subtotal?: FloatFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    createdBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    updatedBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => ProductRelationFilter, {nullable:true})
    product?: ProductRelationFilter;

    @Field(() => IntFilter, {nullable:true})
    productId?: IntFilter;

    @Field(() => SaleRelationFilter, {nullable:true})
    sale?: SaleRelationFilter;

    @Field(() => IntFilter, {nullable:true})
    saleId?: IntFilter;
}
