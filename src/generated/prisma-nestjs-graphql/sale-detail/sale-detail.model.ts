import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { User } from '../user/user.model';
import { Product } from '../product/product.model';
import { Sale } from '../sale/sale.model';

@ObjectType()
export class SaleDetail {

    @Field(() => ID, {nullable:false})
    id!: number;

    @Field(() => Int, {nullable:false})
    quantity!: number;

    @Field(() => Float, {nullable:false})
    subtotal!: number;

    @Field(() => Date, {nullable:false})
    createdAt!: Date;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date;

    @Field(() => User, {nullable:true})
    createdBy?: User | null;

    @Field(() => Int, {nullable:true})
    createdById!: number | null;

    @Field(() => User, {nullable:true})
    updatedBy?: User | null;

    @Field(() => Int, {nullable:true})
    updatedById!: number | null;

    @Field(() => Product, {nullable:false})
    product?: Product;

    @Field(() => Int, {nullable:false})
    productId!: number;

    @Field(() => Sale, {nullable:false})
    sale?: Sale;

    @Field(() => Int, {nullable:false})
    saleId!: number;
}
