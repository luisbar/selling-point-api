import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailUpdateManyMutationInput } from './sale-detail-update-many-mutation.input';
import { SaleDetailWhereInput } from './sale-detail-where.input';

@ArgsType()
export class UpdateManySaleDetailArgs {

    @Field(() => SaleDetailUpdateManyMutationInput, {nullable:false})
    data!: SaleDetailUpdateManyMutationInput;

    @Field(() => SaleDetailWhereInput, {nullable:true})
    where?: SaleDetailWhereInput;
}
