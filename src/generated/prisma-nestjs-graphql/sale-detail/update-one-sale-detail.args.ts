import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailUpdateInput } from './sale-detail-update.input';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';

@ArgsType()
export class UpdateOneSaleDetailArgs {

    @Field(() => SaleDetailUpdateInput, {nullable:false})
    data!: SaleDetailUpdateInput;

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;
}
