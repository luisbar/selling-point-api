import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleDetailWhereUniqueInput } from './sale-detail-where-unique.input';
import { SaleDetailCreateInput } from './sale-detail-create.input';
import { SaleDetailUpdateInput } from './sale-detail-update.input';

@ArgsType()
export class UpsertOneSaleDetailArgs {

    @Field(() => SaleDetailWhereUniqueInput, {nullable:false})
    where!: SaleDetailWhereUniqueInput;

    @Field(() => SaleDetailCreateInput, {nullable:false})
    create!: SaleDetailCreateInput;

    @Field(() => SaleDetailUpdateInput, {nullable:false})
    update!: SaleDetailUpdateInput;
}
