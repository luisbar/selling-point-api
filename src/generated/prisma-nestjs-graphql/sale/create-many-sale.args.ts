import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleCreateManyInput } from './sale-create-many.input';

@ArgsType()
export class CreateManySaleArgs {

    @Field(() => [SaleCreateManyInput], {nullable:false})
    data!: Array<SaleCreateManyInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
