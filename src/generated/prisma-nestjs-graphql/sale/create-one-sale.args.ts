import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleCreateInput } from './sale-create.input';

@ArgsType()
export class CreateOneSaleArgs {

    @Field(() => SaleCreateInput, {nullable:false})
    data!: SaleCreateInput;
}
