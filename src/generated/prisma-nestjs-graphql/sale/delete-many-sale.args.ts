import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleWhereInput } from './sale-where.input';

@ArgsType()
export class DeleteManySaleArgs {

    @Field(() => SaleWhereInput, {nullable:true})
    where?: SaleWhereInput;
}
