import { Field, InputType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleWhereInput } from './sale-where.input';
import { SaleOrderByWithRelationInput } from './sale-order-by-with-relation.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { Int } from '@nestjs/graphql';
import { SaleScalarFieldEnum } from './sale-scalar-field.enum';

@InputType()
@ArgsType()
export class FindManySaleArgs {

    @Field(() => SaleWhereInput, {nullable:true})
    where?: SaleWhereInput;

    @Field(() => [SaleOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<SaleOrderByWithRelationInput>;

    @Field(() => SaleWhereUniqueInput, {nullable:true})
    cursor?: SaleWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => [SaleScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof SaleScalarFieldEnum>;
}
