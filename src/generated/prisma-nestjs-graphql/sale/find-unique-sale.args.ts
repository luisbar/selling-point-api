import { Field, InputType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@InputType()
@ArgsType()
export class FindUniqueSaleArgs {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;
}
