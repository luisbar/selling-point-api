import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateManyCreatedByInput } from './sale-create-many-created-by.input';

@InputType()
export class SaleCreateManyCreatedByInputEnvelope {

    @Field(() => [SaleCreateManyCreatedByInput], {nullable:false})
    data!: Array<SaleCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
