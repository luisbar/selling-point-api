import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateManyCustomerInput } from './sale-create-many-customer.input';

@InputType()
export class SaleCreateManyCustomerInputEnvelope {

    @Field(() => [SaleCreateManyCustomerInput], {nullable:false})
    data!: Array<SaleCreateManyCustomerInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
