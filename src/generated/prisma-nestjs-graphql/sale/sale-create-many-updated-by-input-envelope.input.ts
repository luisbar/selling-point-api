import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateManyUpdatedByInput } from './sale-create-many-updated-by.input';

@InputType()
export class SaleCreateManyUpdatedByInputEnvelope {

    @Field(() => [SaleCreateManyUpdatedByInput], {nullable:false})
    data!: Array<SaleCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
