import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutCustomerInput } from './sale-create-without-customer.input';
import { SaleCreateOrConnectWithoutCustomerInput } from './sale-create-or-connect-without-customer.input';
import { SaleCreateManyCustomerInputEnvelope } from './sale-create-many-customer-input-envelope.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@InputType()
export class SaleCreateNestedManyWithoutCustomerInput {

    @Field(() => [SaleCreateWithoutCustomerInput], {nullable:true})
    create?: Array<SaleCreateWithoutCustomerInput>;

    @Field(() => [SaleCreateOrConnectWithoutCustomerInput], {nullable:true})
    connectOrCreate?: Array<SaleCreateOrConnectWithoutCustomerInput>;

    @Field(() => SaleCreateManyCustomerInputEnvelope, {nullable:true})
    createMany?: SaleCreateManyCustomerInputEnvelope;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    connect?: Array<SaleWhereUniqueInput>;
}
