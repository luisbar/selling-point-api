import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutSaleDetailInput } from './sale-create-without-sale-detail.input';
import { SaleCreateOrConnectWithoutSaleDetailInput } from './sale-create-or-connect-without-sale-detail.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@InputType()
export class SaleCreateNestedOneWithoutSaleDetailInput {

    @Field(() => SaleCreateWithoutSaleDetailInput, {nullable:true})
    create?: SaleCreateWithoutSaleDetailInput;

    @Field(() => SaleCreateOrConnectWithoutSaleDetailInput, {nullable:true})
    connectOrCreate?: SaleCreateOrConnectWithoutSaleDetailInput;

    @Field(() => SaleWhereUniqueInput, {nullable:true})
    connect?: SaleWhereUniqueInput;
}
