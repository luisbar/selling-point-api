import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleCreateWithoutCreatedByInput } from './sale-create-without-created-by.input';

@InputType()
export class SaleCreateOrConnectWithoutCreatedByInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleCreateWithoutCreatedByInput, {nullable:false})
    create!: SaleCreateWithoutCreatedByInput;
}
