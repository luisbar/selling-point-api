import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleCreateWithoutCustomerInput } from './sale-create-without-customer.input';

@InputType()
export class SaleCreateOrConnectWithoutCustomerInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleCreateWithoutCustomerInput, {nullable:false})
    create!: SaleCreateWithoutCustomerInput;
}
