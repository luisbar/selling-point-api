import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleCreateWithoutInvoiceInput } from './sale-create-without-invoice.input';

@InputType()
export class SaleCreateOrConnectWithoutInvoiceInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleCreateWithoutInvoiceInput, {nullable:false})
    create!: SaleCreateWithoutInvoiceInput;
}
