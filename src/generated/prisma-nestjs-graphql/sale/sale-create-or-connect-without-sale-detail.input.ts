import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleCreateWithoutSaleDetailInput } from './sale-create-without-sale-detail.input';

@InputType()
export class SaleCreateOrConnectWithoutSaleDetailInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleCreateWithoutSaleDetailInput, {nullable:false})
    create!: SaleCreateWithoutSaleDetailInput;
}
