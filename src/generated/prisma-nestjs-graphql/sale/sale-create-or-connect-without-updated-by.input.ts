import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleCreateWithoutUpdatedByInput } from './sale-create-without-updated-by.input';

@InputType()
export class SaleCreateOrConnectWithoutUpdatedByInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleCreateWithoutUpdatedByInput, {nullable:false})
    create!: SaleCreateWithoutUpdatedByInput;
}
