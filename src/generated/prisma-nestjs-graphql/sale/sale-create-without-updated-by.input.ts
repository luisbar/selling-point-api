import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { Statuses } from '../prisma/statuses.enum';
import { UserCreateNestedOneWithoutCreatedBySaleInput } from '../user/user-create-nested-one-without-created-by-sale.input';
import { CustomerCreateNestedOneWithoutSaleInput } from '../customer/customer-create-nested-one-without-sale.input';
import { InvoiceCreateNestedOneWithoutSaleInput } from '../invoice/invoice-create-nested-one-without-sale.input';
import { SaleDetailCreateNestedManyWithoutSaleInput } from '../sale-detail/sale-detail-create-nested-many-without-sale.input';

@InputType()
export class SaleCreateWithoutUpdatedByInput {

    @Field(() => Boolean, {nullable:true})
    withInvoice?: boolean;

    @Field(() => Float, {nullable:false})
    total!: number;

    @Field(() => Statuses, {nullable:true})
    status?: keyof typeof Statuses;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedBySaleInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedBySaleInput;

    @Field(() => CustomerCreateNestedOneWithoutSaleInput, {nullable:true})
    customer?: CustomerCreateNestedOneWithoutSaleInput;

    @Field(() => InvoiceCreateNestedOneWithoutSaleInput, {nullable:true})
    invoice?: InvoiceCreateNestedOneWithoutSaleInput;

    @Field(() => SaleDetailCreateNestedManyWithoutSaleInput, {nullable:true})
    saleDetail?: SaleDetailCreateNestedManyWithoutSaleInput;
}
