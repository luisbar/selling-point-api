import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { Statuses } from '../prisma/statuses.enum';
import { SaleCountAggregate } from './sale-count-aggregate.output';
import { SaleAvgAggregate } from './sale-avg-aggregate.output';
import { SaleSumAggregate } from './sale-sum-aggregate.output';
import { SaleMinAggregate } from './sale-min-aggregate.output';
import { SaleMaxAggregate } from './sale-max-aggregate.output';

@ObjectType()
export class SaleGroupBy {

    @Field(() => Int, {nullable:false})
    id!: number;

    @Field(() => Boolean, {nullable:false})
    withInvoice!: boolean;

    @Field(() => Float, {nullable:false})
    total!: number;

    @Field(() => Statuses, {nullable:false})
    status!: keyof typeof Statuses;

    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => Int, {nullable:true})
    customerId?: number;

    @Field(() => Int, {nullable:true})
    invoiceId?: number;

    @Field(() => SaleCountAggregate, {nullable:true})
    _count?: SaleCountAggregate;

    @Field(() => SaleAvgAggregate, {nullable:true})
    _avg?: SaleAvgAggregate;

    @Field(() => SaleSumAggregate, {nullable:true})
    _sum?: SaleSumAggregate;

    @Field(() => SaleMinAggregate, {nullable:true})
    _min?: SaleMinAggregate;

    @Field(() => SaleMaxAggregate, {nullable:true})
    _max?: SaleMaxAggregate;
}
