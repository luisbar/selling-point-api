import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { Statuses } from '../prisma/statuses.enum';

@ObjectType()
export class SaleMinAggregate {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => Boolean, {nullable:true})
    withInvoice?: boolean;

    @Field(() => Float, {nullable:true})
    total?: number;

    @Field(() => Statuses, {nullable:true})
    status?: keyof typeof Statuses;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => Int, {nullable:true})
    customerId?: number;

    @Field(() => Int, {nullable:true})
    invoiceId?: number;
}
