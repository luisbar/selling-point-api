import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { SaleCountOrderByAggregateInput } from './sale-count-order-by-aggregate.input';
import { SaleAvgOrderByAggregateInput } from './sale-avg-order-by-aggregate.input';
import { SaleMaxOrderByAggregateInput } from './sale-max-order-by-aggregate.input';
import { SaleMinOrderByAggregateInput } from './sale-min-order-by-aggregate.input';
import { SaleSumOrderByAggregateInput } from './sale-sum-order-by-aggregate.input';

@InputType()
export class SaleOrderByWithAggregationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    withInvoice?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    total?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    customerId?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    invoiceId?: keyof typeof SortOrder;

    @Field(() => SaleCountOrderByAggregateInput, {nullable:true})
    _count?: SaleCountOrderByAggregateInput;

    @Field(() => SaleAvgOrderByAggregateInput, {nullable:true})
    _avg?: SaleAvgOrderByAggregateInput;

    @Field(() => SaleMaxOrderByAggregateInput, {nullable:true})
    _max?: SaleMaxOrderByAggregateInput;

    @Field(() => SaleMinOrderByAggregateInput, {nullable:true})
    _min?: SaleMinOrderByAggregateInput;

    @Field(() => SaleSumOrderByAggregateInput, {nullable:true})
    _sum?: SaleSumOrderByAggregateInput;
}
