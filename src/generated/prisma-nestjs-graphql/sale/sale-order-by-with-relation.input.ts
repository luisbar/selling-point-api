import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { UserOrderByWithRelationInput } from '../user/user-order-by-with-relation.input';
import { CustomerOrderByWithRelationInput } from '../customer/customer-order-by-with-relation.input';
import { InvoiceOrderByWithRelationInput } from '../invoice/invoice-order-by-with-relation.input';
import { SaleDetailOrderByRelationAggregateInput } from '../sale-detail/sale-detail-order-by-relation-aggregate.input';

@InputType()
export class SaleOrderByWithRelationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    withInvoice?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    total?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    createdBy?: UserOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    updatedBy?: UserOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => CustomerOrderByWithRelationInput, {nullable:true})
    customer?: CustomerOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    customerId?: keyof typeof SortOrder;

    @Field(() => InvoiceOrderByWithRelationInput, {nullable:true})
    invoice?: InvoiceOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    invoiceId?: keyof typeof SortOrder;

    @Field(() => SaleDetailOrderByRelationAggregateInput, {nullable:true})
    saleDetail?: SaleDetailOrderByRelationAggregateInput;
}
