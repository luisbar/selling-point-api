import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereInput } from './sale-where.input';

@InputType()
export class SaleRelationFilter {

    @Field(() => SaleWhereInput, {nullable:true})
    is?: SaleWhereInput;

    @Field(() => SaleWhereInput, {nullable:true})
    isNot?: SaleWhereInput;
}
