import { registerEnumType } from '@nestjs/graphql';

export enum SaleScalarFieldEnum {
    id = "id",
    withInvoice = "withInvoice",
    total = "total",
    status = "status",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById",
    customerId = "customerId",
    invoiceId = "invoiceId"
}


registerEnumType(SaleScalarFieldEnum, { name: 'SaleScalarFieldEnum', description: undefined })
