import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { FloatFilter } from '../prisma/float-filter.input';
import { EnumStatusesFilter } from '../prisma/enum-statuses-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';

@InputType()
export class SaleScalarWhereInput {

    @Field(() => [SaleScalarWhereInput], {nullable:true})
    AND?: Array<SaleScalarWhereInput>;

    @Field(() => [SaleScalarWhereInput], {nullable:true})
    OR?: Array<SaleScalarWhereInput>;

    @Field(() => [SaleScalarWhereInput], {nullable:true})
    NOT?: Array<SaleScalarWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => BoolFilter, {nullable:true})
    withInvoice?: BoolFilter;

    @Field(() => FloatFilter, {nullable:true})
    total?: FloatFilter;

    @Field(() => EnumStatusesFilter, {nullable:true})
    status?: EnumStatusesFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    customerId?: IntNullableFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    invoiceId?: IntNullableFilter;
}
