import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutCreatedByInput } from './sale-create-without-created-by.input';
import { SaleCreateOrConnectWithoutCreatedByInput } from './sale-create-or-connect-without-created-by.input';
import { SaleCreateManyCreatedByInputEnvelope } from './sale-create-many-created-by-input-envelope.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@InputType()
export class SaleUncheckedCreateNestedManyWithoutCreatedByInput {

    @Field(() => [SaleCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<SaleCreateWithoutCreatedByInput>;

    @Field(() => [SaleCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleCreateOrConnectWithoutCreatedByInput>;

    @Field(() => SaleCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: SaleCreateManyCreatedByInputEnvelope;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    connect?: Array<SaleWhereUniqueInput>;
}
