import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutUpdatedByInput } from './sale-create-without-updated-by.input';
import { SaleCreateOrConnectWithoutUpdatedByInput } from './sale-create-or-connect-without-updated-by.input';
import { SaleCreateManyUpdatedByInputEnvelope } from './sale-create-many-updated-by-input-envelope.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@InputType()
export class SaleUncheckedCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [SaleCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<SaleCreateWithoutUpdatedByInput>;

    @Field(() => [SaleCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => SaleCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: SaleCreateManyUpdatedByInputEnvelope;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    connect?: Array<SaleWhereUniqueInput>;
}
