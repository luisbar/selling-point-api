import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutInvoiceInput } from './sale-create-without-invoice.input';
import { SaleCreateOrConnectWithoutInvoiceInput } from './sale-create-or-connect-without-invoice.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@InputType()
export class SaleUncheckedCreateNestedOneWithoutInvoiceInput {

    @Field(() => SaleCreateWithoutInvoiceInput, {nullable:true})
    create?: SaleCreateWithoutInvoiceInput;

    @Field(() => SaleCreateOrConnectWithoutInvoiceInput, {nullable:true})
    connectOrCreate?: SaleCreateOrConnectWithoutInvoiceInput;

    @Field(() => SaleWhereUniqueInput, {nullable:true})
    connect?: SaleWhereUniqueInput;
}
