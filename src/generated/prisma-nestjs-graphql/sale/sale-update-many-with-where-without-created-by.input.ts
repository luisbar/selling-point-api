import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleScalarWhereInput } from './sale-scalar-where.input';
import { SaleUpdateManyMutationInput } from './sale-update-many-mutation.input';

@InputType()
export class SaleUpdateManyWithWhereWithoutCreatedByInput {

    @Field(() => SaleScalarWhereInput, {nullable:false})
    where!: SaleScalarWhereInput;

    @Field(() => SaleUpdateManyMutationInput, {nullable:false})
    data!: SaleUpdateManyMutationInput;
}
