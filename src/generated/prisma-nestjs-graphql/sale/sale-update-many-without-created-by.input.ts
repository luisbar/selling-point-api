import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutCreatedByInput } from './sale-create-without-created-by.input';
import { SaleCreateOrConnectWithoutCreatedByInput } from './sale-create-or-connect-without-created-by.input';
import { SaleUpsertWithWhereUniqueWithoutCreatedByInput } from './sale-upsert-with-where-unique-without-created-by.input';
import { SaleCreateManyCreatedByInputEnvelope } from './sale-create-many-created-by-input-envelope.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithWhereUniqueWithoutCreatedByInput } from './sale-update-with-where-unique-without-created-by.input';
import { SaleUpdateManyWithWhereWithoutCreatedByInput } from './sale-update-many-with-where-without-created-by.input';
import { SaleScalarWhereInput } from './sale-scalar-where.input';

@InputType()
export class SaleUpdateManyWithoutCreatedByInput {

    @Field(() => [SaleCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<SaleCreateWithoutCreatedByInput>;

    @Field(() => [SaleCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [SaleUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<SaleUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => SaleCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: SaleCreateManyCreatedByInputEnvelope;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    set?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    delete?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    connect?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<SaleUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [SaleUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<SaleUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [SaleScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleScalarWhereInput>;
}
