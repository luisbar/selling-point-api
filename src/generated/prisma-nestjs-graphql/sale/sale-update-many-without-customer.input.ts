import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutCustomerInput } from './sale-create-without-customer.input';
import { SaleCreateOrConnectWithoutCustomerInput } from './sale-create-or-connect-without-customer.input';
import { SaleUpsertWithWhereUniqueWithoutCustomerInput } from './sale-upsert-with-where-unique-without-customer.input';
import { SaleCreateManyCustomerInputEnvelope } from './sale-create-many-customer-input-envelope.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithWhereUniqueWithoutCustomerInput } from './sale-update-with-where-unique-without-customer.input';
import { SaleUpdateManyWithWhereWithoutCustomerInput } from './sale-update-many-with-where-without-customer.input';
import { SaleScalarWhereInput } from './sale-scalar-where.input';

@InputType()
export class SaleUpdateManyWithoutCustomerInput {

    @Field(() => [SaleCreateWithoutCustomerInput], {nullable:true})
    create?: Array<SaleCreateWithoutCustomerInput>;

    @Field(() => [SaleCreateOrConnectWithoutCustomerInput], {nullable:true})
    connectOrCreate?: Array<SaleCreateOrConnectWithoutCustomerInput>;

    @Field(() => [SaleUpsertWithWhereUniqueWithoutCustomerInput], {nullable:true})
    upsert?: Array<SaleUpsertWithWhereUniqueWithoutCustomerInput>;

    @Field(() => SaleCreateManyCustomerInputEnvelope, {nullable:true})
    createMany?: SaleCreateManyCustomerInputEnvelope;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    set?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    delete?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    connect?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleUpdateWithWhereUniqueWithoutCustomerInput], {nullable:true})
    update?: Array<SaleUpdateWithWhereUniqueWithoutCustomerInput>;

    @Field(() => [SaleUpdateManyWithWhereWithoutCustomerInput], {nullable:true})
    updateMany?: Array<SaleUpdateManyWithWhereWithoutCustomerInput>;

    @Field(() => [SaleScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleScalarWhereInput>;
}
