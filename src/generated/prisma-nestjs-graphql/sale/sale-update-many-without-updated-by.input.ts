import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutUpdatedByInput } from './sale-create-without-updated-by.input';
import { SaleCreateOrConnectWithoutUpdatedByInput } from './sale-create-or-connect-without-updated-by.input';
import { SaleUpsertWithWhereUniqueWithoutUpdatedByInput } from './sale-upsert-with-where-unique-without-updated-by.input';
import { SaleCreateManyUpdatedByInputEnvelope } from './sale-create-many-updated-by-input-envelope.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithWhereUniqueWithoutUpdatedByInput } from './sale-update-with-where-unique-without-updated-by.input';
import { SaleUpdateManyWithWhereWithoutUpdatedByInput } from './sale-update-many-with-where-without-updated-by.input';
import { SaleScalarWhereInput } from './sale-scalar-where.input';

@InputType()
export class SaleUpdateManyWithoutUpdatedByInput {

    @Field(() => [SaleCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<SaleCreateWithoutUpdatedByInput>;

    @Field(() => [SaleCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<SaleCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [SaleUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<SaleUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => SaleCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: SaleCreateManyUpdatedByInputEnvelope;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    set?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    disconnect?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    delete?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleWhereUniqueInput], {nullable:true})
    connect?: Array<SaleWhereUniqueInput>;

    @Field(() => [SaleUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<SaleUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [SaleUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<SaleUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [SaleScalarWhereInput], {nullable:true})
    deleteMany?: Array<SaleScalarWhereInput>;
}
