import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutSaleDetailInput } from './sale-create-without-sale-detail.input';
import { SaleCreateOrConnectWithoutSaleDetailInput } from './sale-create-or-connect-without-sale-detail.input';
import { SaleUpsertWithoutSaleDetailInput } from './sale-upsert-without-sale-detail.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutSaleDetailInput } from './sale-update-without-sale-detail.input';

@InputType()
export class SaleUpdateOneRequiredWithoutSaleDetailInput {

    @Field(() => SaleCreateWithoutSaleDetailInput, {nullable:true})
    create?: SaleCreateWithoutSaleDetailInput;

    @Field(() => SaleCreateOrConnectWithoutSaleDetailInput, {nullable:true})
    connectOrCreate?: SaleCreateOrConnectWithoutSaleDetailInput;

    @Field(() => SaleUpsertWithoutSaleDetailInput, {nullable:true})
    upsert?: SaleUpsertWithoutSaleDetailInput;

    @Field(() => SaleWhereUniqueInput, {nullable:true})
    connect?: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutSaleDetailInput, {nullable:true})
    update?: SaleUpdateWithoutSaleDetailInput;
}
