import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleCreateWithoutInvoiceInput } from './sale-create-without-invoice.input';
import { SaleCreateOrConnectWithoutInvoiceInput } from './sale-create-or-connect-without-invoice.input';
import { SaleUpsertWithoutInvoiceInput } from './sale-upsert-without-invoice.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutInvoiceInput } from './sale-update-without-invoice.input';

@InputType()
export class SaleUpdateOneWithoutInvoiceInput {

    @Field(() => SaleCreateWithoutInvoiceInput, {nullable:true})
    create?: SaleCreateWithoutInvoiceInput;

    @Field(() => SaleCreateOrConnectWithoutInvoiceInput, {nullable:true})
    connectOrCreate?: SaleCreateOrConnectWithoutInvoiceInput;

    @Field(() => SaleUpsertWithoutInvoiceInput, {nullable:true})
    upsert?: SaleUpsertWithoutInvoiceInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => SaleWhereUniqueInput, {nullable:true})
    connect?: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutInvoiceInput, {nullable:true})
    update?: SaleUpdateWithoutInvoiceInput;
}
