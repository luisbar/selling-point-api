import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutCreatedByInput } from './sale-update-without-created-by.input';

@InputType()
export class SaleUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutCreatedByInput, {nullable:false})
    data!: SaleUpdateWithoutCreatedByInput;
}
