import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutCustomerInput } from './sale-update-without-customer.input';

@InputType()
export class SaleUpdateWithWhereUniqueWithoutCustomerInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutCustomerInput, {nullable:false})
    data!: SaleUpdateWithoutCustomerInput;
}
