import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutUpdatedByInput } from './sale-update-without-updated-by.input';

@InputType()
export class SaleUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutUpdatedByInput, {nullable:false})
    data!: SaleUpdateWithoutUpdatedByInput;
}
