import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { FloatFieldUpdateOperationsInput } from '../prisma/float-field-update-operations.input';
import { EnumStatusesFieldUpdateOperationsInput } from '../prisma/enum-statuses-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutUpdatedBySaleInput } from '../user/user-update-one-without-updated-by-sale.input';
import { CustomerUpdateOneWithoutSaleInput } from '../customer/customer-update-one-without-sale.input';
import { InvoiceUpdateOneWithoutSaleInput } from '../invoice/invoice-update-one-without-sale.input';
import { SaleDetailUpdateManyWithoutSaleInput } from '../sale-detail/sale-detail-update-many-without-sale.input';

@InputType()
export class SaleUpdateWithoutCreatedByInput {

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    withInvoice?: BoolFieldUpdateOperationsInput;

    @Field(() => FloatFieldUpdateOperationsInput, {nullable:true})
    total?: FloatFieldUpdateOperationsInput;

    @Field(() => EnumStatusesFieldUpdateOperationsInput, {nullable:true})
    status?: EnumStatusesFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutUpdatedBySaleInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedBySaleInput;

    @Field(() => CustomerUpdateOneWithoutSaleInput, {nullable:true})
    customer?: CustomerUpdateOneWithoutSaleInput;

    @Field(() => InvoiceUpdateOneWithoutSaleInput, {nullable:true})
    invoice?: InvoiceUpdateOneWithoutSaleInput;

    @Field(() => SaleDetailUpdateManyWithoutSaleInput, {nullable:true})
    saleDetail?: SaleDetailUpdateManyWithoutSaleInput;
}
