import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutCreatedByInput } from './sale-update-without-created-by.input';
import { SaleCreateWithoutCreatedByInput } from './sale-create-without-created-by.input';

@InputType()
export class SaleUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutCreatedByInput, {nullable:false})
    update!: SaleUpdateWithoutCreatedByInput;

    @Field(() => SaleCreateWithoutCreatedByInput, {nullable:false})
    create!: SaleCreateWithoutCreatedByInput;
}
