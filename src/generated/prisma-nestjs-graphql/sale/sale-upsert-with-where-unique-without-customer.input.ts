import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutCustomerInput } from './sale-update-without-customer.input';
import { SaleCreateWithoutCustomerInput } from './sale-create-without-customer.input';

@InputType()
export class SaleUpsertWithWhereUniqueWithoutCustomerInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutCustomerInput, {nullable:false})
    update!: SaleUpdateWithoutCustomerInput;

    @Field(() => SaleCreateWithoutCustomerInput, {nullable:false})
    create!: SaleCreateWithoutCustomerInput;
}
