import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleUpdateWithoutUpdatedByInput } from './sale-update-without-updated-by.input';
import { SaleCreateWithoutUpdatedByInput } from './sale-create-without-updated-by.input';

@InputType()
export class SaleUpsertWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleUpdateWithoutUpdatedByInput, {nullable:false})
    update!: SaleUpdateWithoutUpdatedByInput;

    @Field(() => SaleCreateWithoutUpdatedByInput, {nullable:false})
    create!: SaleCreateWithoutUpdatedByInput;
}
