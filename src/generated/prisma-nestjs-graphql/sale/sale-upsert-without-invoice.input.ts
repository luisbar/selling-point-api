import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleUpdateWithoutInvoiceInput } from './sale-update-without-invoice.input';
import { SaleCreateWithoutInvoiceInput } from './sale-create-without-invoice.input';

@InputType()
export class SaleUpsertWithoutInvoiceInput {

    @Field(() => SaleUpdateWithoutInvoiceInput, {nullable:false})
    update!: SaleUpdateWithoutInvoiceInput;

    @Field(() => SaleCreateWithoutInvoiceInput, {nullable:false})
    create!: SaleCreateWithoutInvoiceInput;
}
