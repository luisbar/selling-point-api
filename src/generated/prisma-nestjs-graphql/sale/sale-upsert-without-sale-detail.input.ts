import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SaleUpdateWithoutSaleDetailInput } from './sale-update-without-sale-detail.input';
import { SaleCreateWithoutSaleDetailInput } from './sale-create-without-sale-detail.input';

@InputType()
export class SaleUpsertWithoutSaleDetailInput {

    @Field(() => SaleUpdateWithoutSaleDetailInput, {nullable:false})
    update!: SaleUpdateWithoutSaleDetailInput;

    @Field(() => SaleCreateWithoutSaleDetailInput, {nullable:false})
    create!: SaleCreateWithoutSaleDetailInput;
}
