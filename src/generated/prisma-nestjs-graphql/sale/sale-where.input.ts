import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { FloatFilter } from '../prisma/float-filter.input';
import { EnumStatusesFilter } from '../prisma/enum-statuses-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { UserRelationFilter } from '../user/user-relation-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';
import { CustomerRelationFilter } from '../customer/customer-relation-filter.input';
import { InvoiceRelationFilter } from '../invoice/invoice-relation-filter.input';
import { SaleDetailListRelationFilter } from '../sale-detail/sale-detail-list-relation-filter.input';

@InputType()
export class SaleWhereInput {

    @Field(() => [SaleWhereInput], {nullable:true})
    AND?: Array<SaleWhereInput>;

    @Field(() => [SaleWhereInput], {nullable:true})
    OR?: Array<SaleWhereInput>;

    @Field(() => [SaleWhereInput], {nullable:true})
    NOT?: Array<SaleWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => BoolFilter, {nullable:true})
    withInvoice?: BoolFilter;

    @Field(() => FloatFilter, {nullable:true})
    total?: FloatFilter;

    @Field(() => EnumStatusesFilter, {nullable:true})
    status?: EnumStatusesFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    createdBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    updatedBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => CustomerRelationFilter, {nullable:true})
    customer?: CustomerRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    customerId?: IntNullableFilter;

    @Field(() => InvoiceRelationFilter, {nullable:true})
    invoice?: InvoiceRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    invoiceId?: IntNullableFilter;

    @Field(() => SaleDetailListRelationFilter, {nullable:true})
    saleDetail?: SaleDetailListRelationFilter;
}
