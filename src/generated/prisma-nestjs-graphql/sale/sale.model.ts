import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import { Statuses } from '../prisma/statuses.enum';
import { User } from '../user/user.model';
import { Int } from '@nestjs/graphql';
import { Customer } from '../customer/customer.model';
import { Invoice } from '../invoice/invoice.model';
import { SaleDetail } from '../sale-detail/sale-detail.model';
import { SaleCount } from './sale-count.output';

@ObjectType()
export class Sale {

    @Field(() => ID, {nullable:false})
    id!: number;

    @Field(() => Boolean, {nullable:false,defaultValue:false})
    withInvoice!: boolean;

    @Field(() => Float, {nullable:false})
    total!: number;

    @Field(() => Statuses, {nullable:false,defaultValue:'pending'})
    status!: keyof typeof Statuses;

    @Field(() => Date, {nullable:false})
    createdAt!: Date;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date;

    @Field(() => User, {nullable:true})
    createdBy?: User | null;

    @Field(() => Int, {nullable:true})
    createdById!: number | null;

    @Field(() => User, {nullable:true})
    updatedBy?: User | null;

    @Field(() => Int, {nullable:true})
    updatedById!: number | null;

    @Field(() => Customer, {nullable:true})
    customer?: Customer | null;

    @Field(() => Int, {nullable:true})
    customerId!: number | null;

    @Field(() => Invoice, {nullable:true})
    invoice?: Invoice | null;

    @Field(() => Int, {nullable:true})
    invoiceId!: number | null;

    @Field(() => [SaleDetail], {nullable:true})
    saleDetail?: Array<SaleDetail>;

    @Field(() => SaleCount, {nullable:false})
    _count?: SaleCount;
}
