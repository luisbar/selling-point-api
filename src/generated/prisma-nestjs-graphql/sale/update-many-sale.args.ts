import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleUpdateManyMutationInput } from './sale-update-many-mutation.input';
import { SaleWhereInput } from './sale-where.input';

@ArgsType()
export class UpdateManySaleArgs {

    @Field(() => SaleUpdateManyMutationInput, {nullable:false})
    data!: SaleUpdateManyMutationInput;

    @Field(() => SaleWhereInput, {nullable:true})
    where?: SaleWhereInput;
}
