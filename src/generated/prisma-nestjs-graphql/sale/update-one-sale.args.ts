import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleUpdateInput } from './sale-update.input';
import { SaleWhereUniqueInput } from './sale-where-unique.input';

@ArgsType()
export class UpdateOneSaleArgs {

    @Field(() => SaleUpdateInput, {nullable:false})
    data!: SaleUpdateInput;

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;
}
