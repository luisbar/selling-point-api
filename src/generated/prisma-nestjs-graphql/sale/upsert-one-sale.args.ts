import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { SaleWhereUniqueInput } from './sale-where-unique.input';
import { SaleCreateInput } from './sale-create.input';
import { SaleUpdateInput } from './sale-update.input';

@ArgsType()
export class UpsertOneSaleArgs {

    @Field(() => SaleWhereUniqueInput, {nullable:false})
    where!: SaleWhereUniqueInput;

    @Field(() => SaleCreateInput, {nullable:false})
    create!: SaleCreateInput;

    @Field(() => SaleUpdateInput, {nullable:false})
    update!: SaleUpdateInput;
}
