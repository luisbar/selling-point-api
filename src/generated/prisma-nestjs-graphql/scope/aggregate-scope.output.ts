import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ScopeCountAggregate } from './scope-count-aggregate.output';
import { ScopeAvgAggregate } from './scope-avg-aggregate.output';
import { ScopeSumAggregate } from './scope-sum-aggregate.output';
import { ScopeMinAggregate } from './scope-min-aggregate.output';
import { ScopeMaxAggregate } from './scope-max-aggregate.output';

@ObjectType()
export class AggregateScope {

    @Field(() => ScopeCountAggregate, {nullable:true})
    _count?: ScopeCountAggregate;

    @Field(() => ScopeAvgAggregate, {nullable:true})
    _avg?: ScopeAvgAggregate;

    @Field(() => ScopeSumAggregate, {nullable:true})
    _sum?: ScopeSumAggregate;

    @Field(() => ScopeMinAggregate, {nullable:true})
    _min?: ScopeMinAggregate;

    @Field(() => ScopeMaxAggregate, {nullable:true})
    _max?: ScopeMaxAggregate;
}
