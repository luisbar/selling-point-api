import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { ScopeWhereInput } from './scope-where.input';
import { ScopeOrderByWithRelationInput } from './scope-order-by-with-relation.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { Int } from '@nestjs/graphql';
import { ScopeCountAggregateInput } from './scope-count-aggregate.input';
import { ScopeAvgAggregateInput } from './scope-avg-aggregate.input';
import { ScopeSumAggregateInput } from './scope-sum-aggregate.input';
import { ScopeMinAggregateInput } from './scope-min-aggregate.input';
import { ScopeMaxAggregateInput } from './scope-max-aggregate.input';

@ArgsType()
export class ScopeAggregateArgs {

    @Field(() => ScopeWhereInput, {nullable:true})
    where?: ScopeWhereInput;

    @Field(() => [ScopeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<ScopeOrderByWithRelationInput>;

    @Field(() => ScopeWhereUniqueInput, {nullable:true})
    cursor?: ScopeWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => ScopeCountAggregateInput, {nullable:true})
    _count?: ScopeCountAggregateInput;

    @Field(() => ScopeAvgAggregateInput, {nullable:true})
    _avg?: ScopeAvgAggregateInput;

    @Field(() => ScopeSumAggregateInput, {nullable:true})
    _sum?: ScopeSumAggregateInput;

    @Field(() => ScopeMinAggregateInput, {nullable:true})
    _min?: ScopeMinAggregateInput;

    @Field(() => ScopeMaxAggregateInput, {nullable:true})
    _max?: ScopeMaxAggregateInput;
}
