import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateManyCreatedByInput } from './scope-create-many-created-by.input';

@InputType()
export class ScopeCreateManyCreatedByInputEnvelope {

    @Field(() => [ScopeCreateManyCreatedByInput], {nullable:false})
    data!: Array<ScopeCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
