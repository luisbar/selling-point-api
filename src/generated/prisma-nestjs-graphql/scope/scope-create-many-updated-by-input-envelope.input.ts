import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateManyUpdatedByInput } from './scope-create-many-updated-by.input';

@InputType()
export class ScopeCreateManyUpdatedByInputEnvelope {

    @Field(() => [ScopeCreateManyUpdatedByInput], {nullable:false})
    data!: Array<ScopeCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
