import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateWithoutUpdatedByInput } from './scope-create-without-updated-by.input';
import { ScopeCreateOrConnectWithoutUpdatedByInput } from './scope-create-or-connect-without-updated-by.input';
import { ScopeCreateManyUpdatedByInputEnvelope } from './scope-create-many-updated-by-input-envelope.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';

@InputType()
export class ScopeCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [ScopeCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<ScopeCreateWithoutUpdatedByInput>;

    @Field(() => [ScopeCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<ScopeCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => ScopeCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: ScopeCreateManyUpdatedByInputEnvelope;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    connect?: Array<ScopeWhereUniqueInput>;
}
