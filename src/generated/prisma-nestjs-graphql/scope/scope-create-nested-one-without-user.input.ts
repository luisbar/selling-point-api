import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateWithoutUserInput } from './scope-create-without-user.input';
import { ScopeCreateOrConnectWithoutUserInput } from './scope-create-or-connect-without-user.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';

@InputType()
export class ScopeCreateNestedOneWithoutUserInput {

    @Field(() => ScopeCreateWithoutUserInput, {nullable:true})
    create?: ScopeCreateWithoutUserInput;

    @Field(() => ScopeCreateOrConnectWithoutUserInput, {nullable:true})
    connectOrCreate?: ScopeCreateOrConnectWithoutUserInput;

    @Field(() => ScopeWhereUniqueInput, {nullable:true})
    connect?: ScopeWhereUniqueInput;
}
