import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeCreateWithoutCreatedByInput } from './scope-create-without-created-by.input';

@InputType()
export class ScopeCreateOrConnectWithoutCreatedByInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeCreateWithoutCreatedByInput, {nullable:false})
    create!: ScopeCreateWithoutCreatedByInput;
}
