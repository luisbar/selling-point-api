import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeCreateWithoutUpdatedByInput } from './scope-create-without-updated-by.input';

@InputType()
export class ScopeCreateOrConnectWithoutUpdatedByInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeCreateWithoutUpdatedByInput, {nullable:false})
    create!: ScopeCreateWithoutUpdatedByInput;
}
