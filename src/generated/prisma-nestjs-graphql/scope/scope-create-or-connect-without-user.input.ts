import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeCreateWithoutUserInput } from './scope-create-without-user.input';

@InputType()
export class ScopeCreateOrConnectWithoutUserInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeCreateWithoutUserInput, {nullable:false})
    create!: ScopeCreateWithoutUserInput;
}
