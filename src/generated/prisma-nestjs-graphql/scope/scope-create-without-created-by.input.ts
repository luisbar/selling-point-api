import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { UserCreateNestedOneWithoutUpdatedByScopeInput } from '../user/user-create-nested-one-without-updated-by-scope.input';
import { UserCreateNestedManyWithoutScopeInput } from '../user/user-create-nested-many-without-scope.input';

@InputType()
export class ScopeCreateWithoutCreatedByInput {

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => GraphQLJSON, {nullable:false})
    rules!: any;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutUpdatedByScopeInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByScopeInput;

    @Field(() => UserCreateNestedManyWithoutScopeInput, {nullable:true})
    user?: UserCreateNestedManyWithoutScopeInput;
}
