import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { UserCreateNestedOneWithoutCreatedByScopeInput } from '../user/user-create-nested-one-without-created-by-scope.input';
import { UserCreateNestedManyWithoutScopeInput } from '../user/user-create-nested-many-without-scope.input';

@InputType()
export class ScopeCreateWithoutUpdatedByInput {

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => GraphQLJSON, {nullable:false})
    rules!: any;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByScopeInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByScopeInput;

    @Field(() => UserCreateNestedManyWithoutScopeInput, {nullable:true})
    user?: UserCreateNestedManyWithoutScopeInput;
}
