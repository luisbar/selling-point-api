import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { ScopeWhereInput } from './scope-where.input';
import { ScopeOrderByWithAggregationInput } from './scope-order-by-with-aggregation.input';
import { ScopeScalarFieldEnum } from './scope-scalar-field.enum';
import { ScopeScalarWhereWithAggregatesInput } from './scope-scalar-where-with-aggregates.input';
import { Int } from '@nestjs/graphql';
import { ScopeCountAggregateInput } from './scope-count-aggregate.input';
import { ScopeAvgAggregateInput } from './scope-avg-aggregate.input';
import { ScopeSumAggregateInput } from './scope-sum-aggregate.input';
import { ScopeMinAggregateInput } from './scope-min-aggregate.input';
import { ScopeMaxAggregateInput } from './scope-max-aggregate.input';

@ArgsType()
export class ScopeGroupByArgs {

    @Field(() => ScopeWhereInput, {nullable:true})
    where?: ScopeWhereInput;

    @Field(() => [ScopeOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<ScopeOrderByWithAggregationInput>;

    @Field(() => [ScopeScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof ScopeScalarFieldEnum>;

    @Field(() => ScopeScalarWhereWithAggregatesInput, {nullable:true})
    having?: ScopeScalarWhereWithAggregatesInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => ScopeCountAggregateInput, {nullable:true})
    _count?: ScopeCountAggregateInput;

    @Field(() => ScopeAvgAggregateInput, {nullable:true})
    _avg?: ScopeAvgAggregateInput;

    @Field(() => ScopeSumAggregateInput, {nullable:true})
    _sum?: ScopeSumAggregateInput;

    @Field(() => ScopeMinAggregateInput, {nullable:true})
    _min?: ScopeMinAggregateInput;

    @Field(() => ScopeMaxAggregateInput, {nullable:true})
    _max?: ScopeMaxAggregateInput;
}
