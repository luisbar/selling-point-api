import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { ScopeCountAggregate } from './scope-count-aggregate.output';
import { ScopeAvgAggregate } from './scope-avg-aggregate.output';
import { ScopeSumAggregate } from './scope-sum-aggregate.output';
import { ScopeMinAggregate } from './scope-min-aggregate.output';
import { ScopeMaxAggregate } from './scope-max-aggregate.output';

@ObjectType()
export class ScopeGroupBy {

    @Field(() => Int, {nullable:false})
    id!: number;

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => GraphQLJSON, {nullable:false})
    rules!: any;

    @Field(() => Boolean, {nullable:false})
    enabled!: boolean;

    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => ScopeCountAggregate, {nullable:true})
    _count?: ScopeCountAggregate;

    @Field(() => ScopeAvgAggregate, {nullable:true})
    _avg?: ScopeAvgAggregate;

    @Field(() => ScopeSumAggregate, {nullable:true})
    _sum?: ScopeSumAggregate;

    @Field(() => ScopeMinAggregate, {nullable:true})
    _min?: ScopeMinAggregate;

    @Field(() => ScopeMaxAggregate, {nullable:true})
    _max?: ScopeMaxAggregate;
}
