import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { ScopeCountOrderByAggregateInput } from './scope-count-order-by-aggregate.input';
import { ScopeAvgOrderByAggregateInput } from './scope-avg-order-by-aggregate.input';
import { ScopeMaxOrderByAggregateInput } from './scope-max-order-by-aggregate.input';
import { ScopeMinOrderByAggregateInput } from './scope-min-order-by-aggregate.input';
import { ScopeSumOrderByAggregateInput } from './scope-sum-order-by-aggregate.input';

@InputType()
export class ScopeOrderByWithAggregationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    rules?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    enabled?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => ScopeCountOrderByAggregateInput, {nullable:true})
    _count?: ScopeCountOrderByAggregateInput;

    @Field(() => ScopeAvgOrderByAggregateInput, {nullable:true})
    _avg?: ScopeAvgOrderByAggregateInput;

    @Field(() => ScopeMaxOrderByAggregateInput, {nullable:true})
    _max?: ScopeMaxOrderByAggregateInput;

    @Field(() => ScopeMinOrderByAggregateInput, {nullable:true})
    _min?: ScopeMinOrderByAggregateInput;

    @Field(() => ScopeSumOrderByAggregateInput, {nullable:true})
    _sum?: ScopeSumOrderByAggregateInput;
}
