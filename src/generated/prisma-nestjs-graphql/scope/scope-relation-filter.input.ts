import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereInput } from './scope-where.input';

@InputType()
export class ScopeRelationFilter {

    @Field(() => ScopeWhereInput, {nullable:true})
    is?: ScopeWhereInput;

    @Field(() => ScopeWhereInput, {nullable:true})
    isNot?: ScopeWhereInput;
}
