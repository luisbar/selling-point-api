import { registerEnumType } from '@nestjs/graphql';

export enum ScopeScalarFieldEnum {
    id = "id",
    name = "name",
    rules = "rules",
    enabled = "enabled",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById"
}


registerEnumType(ScopeScalarFieldEnum, { name: 'ScopeScalarFieldEnum', description: undefined })
