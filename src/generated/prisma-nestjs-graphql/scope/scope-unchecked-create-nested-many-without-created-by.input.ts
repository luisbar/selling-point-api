import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateWithoutCreatedByInput } from './scope-create-without-created-by.input';
import { ScopeCreateOrConnectWithoutCreatedByInput } from './scope-create-or-connect-without-created-by.input';
import { ScopeCreateManyCreatedByInputEnvelope } from './scope-create-many-created-by-input-envelope.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';

@InputType()
export class ScopeUncheckedCreateNestedManyWithoutCreatedByInput {

    @Field(() => [ScopeCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<ScopeCreateWithoutCreatedByInput>;

    @Field(() => [ScopeCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<ScopeCreateOrConnectWithoutCreatedByInput>;

    @Field(() => ScopeCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: ScopeCreateManyCreatedByInputEnvelope;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    connect?: Array<ScopeWhereUniqueInput>;
}
