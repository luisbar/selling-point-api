import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateWithoutCreatedByInput } from './scope-create-without-created-by.input';
import { ScopeCreateOrConnectWithoutCreatedByInput } from './scope-create-or-connect-without-created-by.input';
import { ScopeUpsertWithWhereUniqueWithoutCreatedByInput } from './scope-upsert-with-where-unique-without-created-by.input';
import { ScopeCreateManyCreatedByInputEnvelope } from './scope-create-many-created-by-input-envelope.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithWhereUniqueWithoutCreatedByInput } from './scope-update-with-where-unique-without-created-by.input';
import { ScopeUpdateManyWithWhereWithoutCreatedByInput } from './scope-update-many-with-where-without-created-by.input';
import { ScopeScalarWhereInput } from './scope-scalar-where.input';

@InputType()
export class ScopeUncheckedUpdateManyWithoutCreatedByInput {

    @Field(() => [ScopeCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<ScopeCreateWithoutCreatedByInput>;

    @Field(() => [ScopeCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<ScopeCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [ScopeUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<ScopeUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => ScopeCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: ScopeCreateManyCreatedByInputEnvelope;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    set?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    disconnect?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    delete?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    connect?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<ScopeUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [ScopeUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<ScopeUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [ScopeScalarWhereInput], {nullable:true})
    deleteMany?: Array<ScopeScalarWhereInput>;
}
