import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateWithoutUpdatedByInput } from './scope-create-without-updated-by.input';
import { ScopeCreateOrConnectWithoutUpdatedByInput } from './scope-create-or-connect-without-updated-by.input';
import { ScopeUpsertWithWhereUniqueWithoutUpdatedByInput } from './scope-upsert-with-where-unique-without-updated-by.input';
import { ScopeCreateManyUpdatedByInputEnvelope } from './scope-create-many-updated-by-input-envelope.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithWhereUniqueWithoutUpdatedByInput } from './scope-update-with-where-unique-without-updated-by.input';
import { ScopeUpdateManyWithWhereWithoutUpdatedByInput } from './scope-update-many-with-where-without-updated-by.input';
import { ScopeScalarWhereInput } from './scope-scalar-where.input';

@InputType()
export class ScopeUncheckedUpdateManyWithoutUpdatedByInput {

    @Field(() => [ScopeCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<ScopeCreateWithoutUpdatedByInput>;

    @Field(() => [ScopeCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<ScopeCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [ScopeUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<ScopeUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => ScopeCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: ScopeCreateManyUpdatedByInputEnvelope;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    set?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    disconnect?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    delete?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeWhereUniqueInput], {nullable:true})
    connect?: Array<ScopeWhereUniqueInput>;

    @Field(() => [ScopeUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<ScopeUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [ScopeUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<ScopeUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [ScopeScalarWhereInput], {nullable:true})
    deleteMany?: Array<ScopeScalarWhereInput>;
}
