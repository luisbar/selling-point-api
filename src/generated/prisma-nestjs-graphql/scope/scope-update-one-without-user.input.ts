import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeCreateWithoutUserInput } from './scope-create-without-user.input';
import { ScopeCreateOrConnectWithoutUserInput } from './scope-create-or-connect-without-user.input';
import { ScopeUpsertWithoutUserInput } from './scope-upsert-without-user.input';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithoutUserInput } from './scope-update-without-user.input';

@InputType()
export class ScopeUpdateOneWithoutUserInput {

    @Field(() => ScopeCreateWithoutUserInput, {nullable:true})
    create?: ScopeCreateWithoutUserInput;

    @Field(() => ScopeCreateOrConnectWithoutUserInput, {nullable:true})
    connectOrCreate?: ScopeCreateOrConnectWithoutUserInput;

    @Field(() => ScopeUpsertWithoutUserInput, {nullable:true})
    upsert?: ScopeUpsertWithoutUserInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => ScopeWhereUniqueInput, {nullable:true})
    connect?: ScopeWhereUniqueInput;

    @Field(() => ScopeUpdateWithoutUserInput, {nullable:true})
    update?: ScopeUpdateWithoutUserInput;
}
