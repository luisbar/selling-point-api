import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithoutCreatedByInput } from './scope-update-without-created-by.input';

@InputType()
export class ScopeUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeUpdateWithoutCreatedByInput, {nullable:false})
    data!: ScopeUpdateWithoutCreatedByInput;
}
