import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithoutUpdatedByInput } from './scope-update-without-updated-by.input';

@InputType()
export class ScopeUpdateWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeUpdateWithoutUpdatedByInput, {nullable:false})
    data!: ScopeUpdateWithoutUpdatedByInput;
}
