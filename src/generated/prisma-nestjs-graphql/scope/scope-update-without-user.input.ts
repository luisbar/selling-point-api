import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input';
import { GraphQLJSON } from 'graphql-type-json';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutCreatedByScopeInput } from '../user/user-update-one-without-created-by-scope.input';
import { UserUpdateOneWithoutUpdatedByScopeInput } from '../user/user-update-one-without-updated-by-scope.input';

@InputType()
export class ScopeUpdateWithoutUserInput {

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    name?: StringFieldUpdateOperationsInput;

    @Field(() => GraphQLJSON, {nullable:true})
    rules?: any;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    enabled?: BoolFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutCreatedByScopeInput, {nullable:true})
    createdBy?: UserUpdateOneWithoutCreatedByScopeInput;

    @Field(() => UserUpdateOneWithoutUpdatedByScopeInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedByScopeInput;
}
