import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithoutCreatedByInput } from './scope-update-without-created-by.input';
import { ScopeCreateWithoutCreatedByInput } from './scope-create-without-created-by.input';

@InputType()
export class ScopeUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeUpdateWithoutCreatedByInput, {nullable:false})
    update!: ScopeUpdateWithoutCreatedByInput;

    @Field(() => ScopeCreateWithoutCreatedByInput, {nullable:false})
    create!: ScopeCreateWithoutCreatedByInput;
}
