import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeWhereUniqueInput } from './scope-where-unique.input';
import { ScopeUpdateWithoutUpdatedByInput } from './scope-update-without-updated-by.input';
import { ScopeCreateWithoutUpdatedByInput } from './scope-create-without-updated-by.input';

@InputType()
export class ScopeUpsertWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => ScopeWhereUniqueInput, {nullable:false})
    where!: ScopeWhereUniqueInput;

    @Field(() => ScopeUpdateWithoutUpdatedByInput, {nullable:false})
    update!: ScopeUpdateWithoutUpdatedByInput;

    @Field(() => ScopeCreateWithoutUpdatedByInput, {nullable:false})
    create!: ScopeCreateWithoutUpdatedByInput;
}
