import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { ScopeUpdateWithoutUserInput } from './scope-update-without-user.input';
import { ScopeCreateWithoutUserInput } from './scope-create-without-user.input';

@InputType()
export class ScopeUpsertWithoutUserInput {

    @Field(() => ScopeUpdateWithoutUserInput, {nullable:false})
    update!: ScopeUpdateWithoutUserInput;

    @Field(() => ScopeCreateWithoutUserInput, {nullable:false})
    create!: ScopeCreateWithoutUserInput;
}
