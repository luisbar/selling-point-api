import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { StringFilter } from '../prisma/string-filter.input';
import { JsonFilter } from '../prisma/json-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { UserRelationFilter } from '../user/user-relation-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';
import { UserListRelationFilter } from '../user/user-list-relation-filter.input';

@InputType()
export class ScopeWhereInput {

    @Field(() => [ScopeWhereInput], {nullable:true})
    AND?: Array<ScopeWhereInput>;

    @Field(() => [ScopeWhereInput], {nullable:true})
    OR?: Array<ScopeWhereInput>;

    @Field(() => [ScopeWhereInput], {nullable:true})
    NOT?: Array<ScopeWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => StringFilter, {nullable:true})
    name?: StringFilter;

    @Field(() => JsonFilter, {nullable:true})
    rules?: JsonFilter;

    @Field(() => BoolFilter, {nullable:true})
    enabled?: BoolFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    createdBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    updatedBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => UserListRelationFilter, {nullable:true})
    user?: UserListRelationFilter;
}
