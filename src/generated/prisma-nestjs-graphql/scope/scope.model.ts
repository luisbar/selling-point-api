import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { User } from '../user/user.model';
import { Int } from '@nestjs/graphql';
import { ScopeCount } from './scope-count.output';

@ObjectType()
export class Scope {

    @Field(() => ID, {nullable:false})
    id!: number;

    @Field(() => String, {nullable:false})
    name!: string;

    @Field(() => GraphQLJSON, {nullable:false})
    rules!: any;

    @Field(() => Boolean, {nullable:false,defaultValue:true})
    enabled!: boolean;

    @Field(() => Date, {nullable:false})
    createdAt!: Date;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date;

    @Field(() => User, {nullable:true})
    createdBy?: User | null;

    @Field(() => Int, {nullable:true})
    createdById!: number | null;

    @Field(() => User, {nullable:true})
    updatedBy?: User | null;

    @Field(() => Int, {nullable:true})
    updatedById!: number | null;

    @Field(() => [User], {nullable:true})
    user?: Array<User>;

    @Field(() => ScopeCount, {nullable:false})
    _count?: ScopeCount;
}
