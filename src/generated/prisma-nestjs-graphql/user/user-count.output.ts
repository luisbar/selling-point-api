import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';

@ObjectType()
export class UserCount {

    @Field(() => Int, {nullable:false})
    createdByUser!: number;

    @Field(() => Int, {nullable:false})
    updatedByUser!: number;

    @Field(() => Int, {nullable:false})
    createdByScope!: number;

    @Field(() => Int, {nullable:false})
    updatedByScope!: number;

    @Field(() => Int, {nullable:false})
    createdByCustomer!: number;

    @Field(() => Int, {nullable:false})
    updatedByCustomer!: number;

    @Field(() => Int, {nullable:false})
    createdBySale!: number;

    @Field(() => Int, {nullable:false})
    updatedBySale!: number;

    @Field(() => Int, {nullable:false})
    createdByInvoice!: number;

    @Field(() => Int, {nullable:false})
    updatedByInvoice!: number;

    @Field(() => Int, {nullable:false})
    createdByProduct!: number;

    @Field(() => Int, {nullable:false})
    updatedByProduct!: number;

    @Field(() => Int, {nullable:false})
    createdBySaleDetail!: number;

    @Field(() => Int, {nullable:false})
    updatedBySaleDetail!: number;

    @Field(() => Int, {nullable:false})
    createdByCategory!: number;

    @Field(() => Int, {nullable:false})
    updatedByCategory!: number;
}
