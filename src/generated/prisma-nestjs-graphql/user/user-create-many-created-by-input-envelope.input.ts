import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateManyCreatedByInput } from './user-create-many-created-by.input';

@InputType()
export class UserCreateManyCreatedByInputEnvelope {

    @Field(() => [UserCreateManyCreatedByInput], {nullable:false})
    data!: Array<UserCreateManyCreatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
