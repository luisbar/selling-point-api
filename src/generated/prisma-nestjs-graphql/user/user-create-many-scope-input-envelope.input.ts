import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateManyScopeInput } from './user-create-many-scope.input';

@InputType()
export class UserCreateManyScopeInputEnvelope {

    @Field(() => [UserCreateManyScopeInput], {nullable:false})
    data!: Array<UserCreateManyScopeInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
