import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateManyUpdatedByInput } from './user-create-many-updated-by.input';

@InputType()
export class UserCreateManyUpdatedByInputEnvelope {

    @Field(() => [UserCreateManyUpdatedByInput], {nullable:false})
    data!: Array<UserCreateManyUpdatedByInput>;

    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}
