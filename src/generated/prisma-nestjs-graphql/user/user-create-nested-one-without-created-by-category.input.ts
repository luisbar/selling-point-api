import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByCategoryInput } from './user-create-without-created-by-category.input';
import { UserCreateOrConnectWithoutCreatedByCategoryInput } from './user-create-or-connect-without-created-by-category.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedByCategoryInput {

    @Field(() => UserCreateWithoutCreatedByCategoryInput, {nullable:true})
    create?: UserCreateWithoutCreatedByCategoryInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByCategoryInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByCategoryInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
