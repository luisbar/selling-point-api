import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByCustomerInput } from './user-create-without-created-by-customer.input';
import { UserCreateOrConnectWithoutCreatedByCustomerInput } from './user-create-or-connect-without-created-by-customer.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedByCustomerInput {

    @Field(() => UserCreateWithoutCreatedByCustomerInput, {nullable:true})
    create?: UserCreateWithoutCreatedByCustomerInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByCustomerInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByCustomerInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
