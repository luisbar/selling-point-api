import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByInvoiceInput } from './user-create-without-created-by-invoice.input';
import { UserCreateOrConnectWithoutCreatedByInvoiceInput } from './user-create-or-connect-without-created-by-invoice.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedByInvoiceInput {

    @Field(() => UserCreateWithoutCreatedByInvoiceInput, {nullable:true})
    create?: UserCreateWithoutCreatedByInvoiceInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByInvoiceInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByInvoiceInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
