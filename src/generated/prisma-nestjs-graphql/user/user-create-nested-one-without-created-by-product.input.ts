import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByProductInput } from './user-create-without-created-by-product.input';
import { UserCreateOrConnectWithoutCreatedByProductInput } from './user-create-or-connect-without-created-by-product.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedByProductInput {

    @Field(() => UserCreateWithoutCreatedByProductInput, {nullable:true})
    create?: UserCreateWithoutCreatedByProductInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByProductInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByProductInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
