import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedBySaleDetailInput } from './user-create-without-created-by-sale-detail.input';
import { UserCreateOrConnectWithoutCreatedBySaleDetailInput } from './user-create-or-connect-without-created-by-sale-detail.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedBySaleDetailInput {

    @Field(() => UserCreateWithoutCreatedBySaleDetailInput, {nullable:true})
    create?: UserCreateWithoutCreatedBySaleDetailInput;

    @Field(() => UserCreateOrConnectWithoutCreatedBySaleDetailInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedBySaleDetailInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
