import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedBySaleInput } from './user-create-without-created-by-sale.input';
import { UserCreateOrConnectWithoutCreatedBySaleInput } from './user-create-or-connect-without-created-by-sale.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedBySaleInput {

    @Field(() => UserCreateWithoutCreatedBySaleInput, {nullable:true})
    create?: UserCreateWithoutCreatedBySaleInput;

    @Field(() => UserCreateOrConnectWithoutCreatedBySaleInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedBySaleInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
