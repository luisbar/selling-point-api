import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByScopeInput } from './user-create-without-created-by-scope.input';
import { UserCreateOrConnectWithoutCreatedByScopeInput } from './user-create-or-connect-without-created-by-scope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedByScopeInput {

    @Field(() => UserCreateWithoutCreatedByScopeInput, {nullable:true})
    create?: UserCreateWithoutCreatedByScopeInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByScopeInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByScopeInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
