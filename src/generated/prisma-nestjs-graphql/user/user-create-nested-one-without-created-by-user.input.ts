import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByUserInput } from './user-create-without-created-by-user.input';
import { UserCreateOrConnectWithoutCreatedByUserInput } from './user-create-or-connect-without-created-by-user.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutCreatedByUserInput {

    @Field(() => UserCreateWithoutCreatedByUserInput, {nullable:true})
    create?: UserCreateWithoutCreatedByUserInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByUserInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByUserInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
