import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByCategoryInput } from './user-create-without-updated-by-category.input';
import { UserCreateOrConnectWithoutUpdatedByCategoryInput } from './user-create-or-connect-without-updated-by-category.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedByCategoryInput {

    @Field(() => UserCreateWithoutUpdatedByCategoryInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByCategoryInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByCategoryInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByCategoryInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
