import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByCustomerInput } from './user-create-without-updated-by-customer.input';
import { UserCreateOrConnectWithoutUpdatedByCustomerInput } from './user-create-or-connect-without-updated-by-customer.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedByCustomerInput {

    @Field(() => UserCreateWithoutUpdatedByCustomerInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByCustomerInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByCustomerInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByCustomerInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
