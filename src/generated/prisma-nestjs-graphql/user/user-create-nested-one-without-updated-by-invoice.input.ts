import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByInvoiceInput } from './user-create-without-updated-by-invoice.input';
import { UserCreateOrConnectWithoutUpdatedByInvoiceInput } from './user-create-or-connect-without-updated-by-invoice.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedByInvoiceInput {

    @Field(() => UserCreateWithoutUpdatedByInvoiceInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByInvoiceInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByInvoiceInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByInvoiceInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
