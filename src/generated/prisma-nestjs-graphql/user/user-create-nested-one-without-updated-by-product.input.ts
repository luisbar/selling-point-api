import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByProductInput } from './user-create-without-updated-by-product.input';
import { UserCreateOrConnectWithoutUpdatedByProductInput } from './user-create-or-connect-without-updated-by-product.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedByProductInput {

    @Field(() => UserCreateWithoutUpdatedByProductInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByProductInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByProductInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByProductInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
