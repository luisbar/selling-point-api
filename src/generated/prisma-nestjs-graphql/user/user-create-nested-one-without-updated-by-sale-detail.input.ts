import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedBySaleDetailInput } from './user-create-without-updated-by-sale-detail.input';
import { UserCreateOrConnectWithoutUpdatedBySaleDetailInput } from './user-create-or-connect-without-updated-by-sale-detail.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedBySaleDetailInput {

    @Field(() => UserCreateWithoutUpdatedBySaleDetailInput, {nullable:true})
    create?: UserCreateWithoutUpdatedBySaleDetailInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedBySaleDetailInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedBySaleDetailInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
