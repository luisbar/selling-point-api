import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedBySaleInput } from './user-create-without-updated-by-sale.input';
import { UserCreateOrConnectWithoutUpdatedBySaleInput } from './user-create-or-connect-without-updated-by-sale.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedBySaleInput {

    @Field(() => UserCreateWithoutUpdatedBySaleInput, {nullable:true})
    create?: UserCreateWithoutUpdatedBySaleInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedBySaleInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedBySaleInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
