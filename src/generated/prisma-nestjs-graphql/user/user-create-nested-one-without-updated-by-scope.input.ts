import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByScopeInput } from './user-create-without-updated-by-scope.input';
import { UserCreateOrConnectWithoutUpdatedByScopeInput } from './user-create-or-connect-without-updated-by-scope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedByScopeInput {

    @Field(() => UserCreateWithoutUpdatedByScopeInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByScopeInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByScopeInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByScopeInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
