import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByUserInput } from './user-create-without-updated-by-user.input';
import { UserCreateOrConnectWithoutUpdatedByUserInput } from './user-create-or-connect-without-updated-by-user.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateNestedOneWithoutUpdatedByUserInput {

    @Field(() => UserCreateWithoutUpdatedByUserInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByUserInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByUserInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByUserInput;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;
}
