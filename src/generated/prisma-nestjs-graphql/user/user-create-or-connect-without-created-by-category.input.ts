import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByCategoryInput } from './user-create-without-created-by-category.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByCategoryInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByCategoryInput, {nullable:false})
    create!: UserCreateWithoutCreatedByCategoryInput;
}
