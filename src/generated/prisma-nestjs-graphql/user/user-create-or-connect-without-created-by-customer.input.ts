import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByCustomerInput } from './user-create-without-created-by-customer.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByCustomerInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByCustomerInput, {nullable:false})
    create!: UserCreateWithoutCreatedByCustomerInput;
}
