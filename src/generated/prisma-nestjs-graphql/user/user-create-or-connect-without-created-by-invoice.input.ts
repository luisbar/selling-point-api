import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByInvoiceInput } from './user-create-without-created-by-invoice.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByInvoiceInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByInvoiceInput, {nullable:false})
    create!: UserCreateWithoutCreatedByInvoiceInput;
}
