import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByProductInput } from './user-create-without-created-by-product.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByProductInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByProductInput, {nullable:false})
    create!: UserCreateWithoutCreatedByProductInput;
}
