import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedBySaleDetailInput } from './user-create-without-created-by-sale-detail.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedBySaleDetailInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedBySaleDetailInput, {nullable:false})
    create!: UserCreateWithoutCreatedBySaleDetailInput;
}
