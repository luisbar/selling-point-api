import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedBySaleInput } from './user-create-without-created-by-sale.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedBySaleInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedBySaleInput, {nullable:false})
    create!: UserCreateWithoutCreatedBySaleInput;
}
