import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByScopeInput } from './user-create-without-created-by-scope.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByScopeInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByScopeInput, {nullable:false})
    create!: UserCreateWithoutCreatedByScopeInput;
}
