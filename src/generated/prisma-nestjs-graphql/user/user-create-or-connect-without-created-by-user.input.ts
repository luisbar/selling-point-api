import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByUserInput } from './user-create-without-created-by-user.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByUserInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByUserInput, {nullable:false})
    create!: UserCreateWithoutCreatedByUserInput;
}
