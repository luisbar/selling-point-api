import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutCreatedByInput } from './user-create-without-created-by.input';

@InputType()
export class UserCreateOrConnectWithoutCreatedByInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutCreatedByInput, {nullable:false})
    create!: UserCreateWithoutCreatedByInput;
}
