import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutScopeInput } from './user-create-without-scope.input';

@InputType()
export class UserCreateOrConnectWithoutScopeInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutScopeInput, {nullable:false})
    create!: UserCreateWithoutScopeInput;
}
