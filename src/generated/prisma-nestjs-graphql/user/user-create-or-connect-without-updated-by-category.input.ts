import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByCategoryInput } from './user-create-without-updated-by-category.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByCategoryInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByCategoryInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByCategoryInput;
}
