import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByCustomerInput } from './user-create-without-updated-by-customer.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByCustomerInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByCustomerInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByCustomerInput;
}
