import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByInvoiceInput } from './user-create-without-updated-by-invoice.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByInvoiceInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByInvoiceInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByInvoiceInput;
}
