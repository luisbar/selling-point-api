import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByProductInput } from './user-create-without-updated-by-product.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByProductInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByProductInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByProductInput;
}
