import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedBySaleDetailInput } from './user-create-without-updated-by-sale-detail.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedBySaleDetailInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedBySaleDetailInput, {nullable:false})
    create!: UserCreateWithoutUpdatedBySaleDetailInput;
}
