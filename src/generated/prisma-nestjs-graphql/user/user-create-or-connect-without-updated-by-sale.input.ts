import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedBySaleInput } from './user-create-without-updated-by-sale.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedBySaleInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedBySaleInput, {nullable:false})
    create!: UserCreateWithoutUpdatedBySaleInput;
}
