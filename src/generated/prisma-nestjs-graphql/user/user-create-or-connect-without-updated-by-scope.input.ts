import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByScopeInput } from './user-create-without-updated-by-scope.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByScopeInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByScopeInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByScopeInput;
}
