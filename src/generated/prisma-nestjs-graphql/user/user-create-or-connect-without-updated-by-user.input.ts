import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByUserInput } from './user-create-without-updated-by-user.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByUserInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByUserInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByUserInput;
}
