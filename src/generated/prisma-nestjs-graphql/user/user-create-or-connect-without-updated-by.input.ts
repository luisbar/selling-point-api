import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserCreateWithoutUpdatedByInput } from './user-create-without-updated-by.input';

@InputType()
export class UserCreateOrConnectWithoutUpdatedByInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserCreateWithoutUpdatedByInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByInput;
}
