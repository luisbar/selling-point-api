import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateNestedOneWithoutCreatedByUserInput } from './user-create-nested-one-without-created-by-user.input';
import { UserCreateNestedOneWithoutUpdatedByUserInput } from './user-create-nested-one-without-updated-by-user.input';
import { ScopeCreateNestedOneWithoutUserInput } from '../scope/scope-create-nested-one-without-user.input';
import { UserCreateNestedManyWithoutCreatedByInput } from './user-create-nested-many-without-created-by.input';
import { UserCreateNestedManyWithoutUpdatedByInput } from './user-create-nested-many-without-updated-by.input';
import { ScopeCreateNestedManyWithoutCreatedByInput } from '../scope/scope-create-nested-many-without-created-by.input';
import { ScopeCreateNestedManyWithoutUpdatedByInput } from '../scope/scope-create-nested-many-without-updated-by.input';
import { CustomerCreateNestedManyWithoutCreatedByInput } from '../customer/customer-create-nested-many-without-created-by.input';
import { CustomerCreateNestedManyWithoutUpdatedByInput } from '../customer/customer-create-nested-many-without-updated-by.input';
import { SaleCreateNestedManyWithoutCreatedByInput } from '../sale/sale-create-nested-many-without-created-by.input';
import { SaleCreateNestedManyWithoutUpdatedByInput } from '../sale/sale-create-nested-many-without-updated-by.input';
import { InvoiceCreateNestedManyWithoutUpdatedByInput } from '../invoice/invoice-create-nested-many-without-updated-by.input';
import { ProductCreateNestedManyWithoutCreatedByInput } from '../product/product-create-nested-many-without-created-by.input';
import { ProductCreateNestedManyWithoutUpdatedByInput } from '../product/product-create-nested-many-without-updated-by.input';
import { SaleDetailCreateNestedManyWithoutCreatedByInput } from '../sale-detail/sale-detail-create-nested-many-without-created-by.input';
import { SaleDetailCreateNestedManyWithoutUpdatedByInput } from '../sale-detail/sale-detail-create-nested-many-without-updated-by.input';
import { CategoryCreateNestedManyWithoutCreatedByInput } from '../category/category-create-nested-many-without-created-by.input';
import { CategoryCreateNestedManyWithoutUpdatedByInput } from '../category/category-create-nested-many-without-updated-by.input';

@InputType()
export class UserCreateWithoutCreatedByInvoiceInput {

    @Field(() => String, {nullable:false})
    firstName!: string;

    @Field(() => String, {nullable:false})
    lastName!: string;

    @Field(() => String, {nullable:false})
    username!: string;

    @Field(() => String, {nullable:false})
    password!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => UserCreateNestedOneWithoutCreatedByUserInput, {nullable:true})
    createdBy?: UserCreateNestedOneWithoutCreatedByUserInput;

    @Field(() => UserCreateNestedOneWithoutUpdatedByUserInput, {nullable:true})
    updatedBy?: UserCreateNestedOneWithoutUpdatedByUserInput;

    @Field(() => ScopeCreateNestedOneWithoutUserInput, {nullable:true})
    scope?: ScopeCreateNestedOneWithoutUserInput;

    @Field(() => UserCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByUser?: UserCreateNestedManyWithoutCreatedByInput;

    @Field(() => UserCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByUser?: UserCreateNestedManyWithoutUpdatedByInput;

    @Field(() => ScopeCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByScope?: ScopeCreateNestedManyWithoutCreatedByInput;

    @Field(() => ScopeCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByScope?: ScopeCreateNestedManyWithoutUpdatedByInput;

    @Field(() => CustomerCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByCustomer?: CustomerCreateNestedManyWithoutCreatedByInput;

    @Field(() => CustomerCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByCustomer?: CustomerCreateNestedManyWithoutUpdatedByInput;

    @Field(() => SaleCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdBySale?: SaleCreateNestedManyWithoutCreatedByInput;

    @Field(() => SaleCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedBySale?: SaleCreateNestedManyWithoutUpdatedByInput;

    @Field(() => InvoiceCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByInvoice?: InvoiceCreateNestedManyWithoutUpdatedByInput;

    @Field(() => ProductCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByProduct?: ProductCreateNestedManyWithoutCreatedByInput;

    @Field(() => ProductCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByProduct?: ProductCreateNestedManyWithoutUpdatedByInput;

    @Field(() => SaleDetailCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdBySaleDetail?: SaleDetailCreateNestedManyWithoutCreatedByInput;

    @Field(() => SaleDetailCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedBySaleDetail?: SaleDetailCreateNestedManyWithoutUpdatedByInput;

    @Field(() => CategoryCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByCategory?: CategoryCreateNestedManyWithoutCreatedByInput;

    @Field(() => CategoryCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByCategory?: CategoryCreateNestedManyWithoutUpdatedByInput;
}
