import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { ScopeOrderByWithRelationInput } from '../scope/scope-order-by-with-relation.input';
import { UserOrderByRelationAggregateInput } from './user-order-by-relation-aggregate.input';
import { ScopeOrderByRelationAggregateInput } from '../scope/scope-order-by-relation-aggregate.input';
import { CustomerOrderByRelationAggregateInput } from '../customer/customer-order-by-relation-aggregate.input';
import { SaleOrderByRelationAggregateInput } from '../sale/sale-order-by-relation-aggregate.input';
import { InvoiceOrderByRelationAggregateInput } from '../invoice/invoice-order-by-relation-aggregate.input';
import { ProductOrderByRelationAggregateInput } from '../product/product-order-by-relation-aggregate.input';
import { SaleDetailOrderByRelationAggregateInput } from '../sale-detail/sale-detail-order-by-relation-aggregate.input';
import { CategoryOrderByRelationAggregateInput } from '../category/category-order-by-relation-aggregate.input';

@InputType()
export class UserOrderByWithRelationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    firstName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    lastName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    username?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    password?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    enabled?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;

    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    createdBy?: UserOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    createdById?: keyof typeof SortOrder;

    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    updatedBy?: UserOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    updatedById?: keyof typeof SortOrder;

    @Field(() => ScopeOrderByWithRelationInput, {nullable:true})
    scope?: ScopeOrderByWithRelationInput;

    @Field(() => SortOrder, {nullable:true})
    scopeId?: keyof typeof SortOrder;

    @Field(() => UserOrderByRelationAggregateInput, {nullable:true})
    createdByUser?: UserOrderByRelationAggregateInput;

    @Field(() => UserOrderByRelationAggregateInput, {nullable:true})
    updatedByUser?: UserOrderByRelationAggregateInput;

    @Field(() => ScopeOrderByRelationAggregateInput, {nullable:true})
    createdByScope?: ScopeOrderByRelationAggregateInput;

    @Field(() => ScopeOrderByRelationAggregateInput, {nullable:true})
    updatedByScope?: ScopeOrderByRelationAggregateInput;

    @Field(() => CustomerOrderByRelationAggregateInput, {nullable:true})
    createdByCustomer?: CustomerOrderByRelationAggregateInput;

    @Field(() => CustomerOrderByRelationAggregateInput, {nullable:true})
    updatedByCustomer?: CustomerOrderByRelationAggregateInput;

    @Field(() => SaleOrderByRelationAggregateInput, {nullable:true})
    createdBySale?: SaleOrderByRelationAggregateInput;

    @Field(() => SaleOrderByRelationAggregateInput, {nullable:true})
    updatedBySale?: SaleOrderByRelationAggregateInput;

    @Field(() => InvoiceOrderByRelationAggregateInput, {nullable:true})
    createdByInvoice?: InvoiceOrderByRelationAggregateInput;

    @Field(() => InvoiceOrderByRelationAggregateInput, {nullable:true})
    updatedByInvoice?: InvoiceOrderByRelationAggregateInput;

    @Field(() => ProductOrderByRelationAggregateInput, {nullable:true})
    createdByProduct?: ProductOrderByRelationAggregateInput;

    @Field(() => ProductOrderByRelationAggregateInput, {nullable:true})
    updatedByProduct?: ProductOrderByRelationAggregateInput;

    @Field(() => SaleDetailOrderByRelationAggregateInput, {nullable:true})
    createdBySaleDetail?: SaleDetailOrderByRelationAggregateInput;

    @Field(() => SaleDetailOrderByRelationAggregateInput, {nullable:true})
    updatedBySaleDetail?: SaleDetailOrderByRelationAggregateInput;

    @Field(() => CategoryOrderByRelationAggregateInput, {nullable:true})
    createdByCategory?: CategoryOrderByRelationAggregateInput;

    @Field(() => CategoryOrderByRelationAggregateInput, {nullable:true})
    updatedByCategory?: CategoryOrderByRelationAggregateInput;
}
