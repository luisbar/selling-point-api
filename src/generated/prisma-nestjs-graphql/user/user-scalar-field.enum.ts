import { registerEnumType } from '@nestjs/graphql';

export enum UserScalarFieldEnum {
    id = "id",
    firstName = "firstName",
    lastName = "lastName",
    username = "username",
    password = "password",
    enabled = "enabled",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    createdById = "createdById",
    updatedById = "updatedById",
    scopeId = "scopeId"
}


registerEnumType(UserScalarFieldEnum, { name: 'UserScalarFieldEnum', description: undefined })
