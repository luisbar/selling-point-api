import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByInput } from './user-create-without-created-by.input';
import { UserCreateOrConnectWithoutCreatedByInput } from './user-create-or-connect-without-created-by.input';
import { UserCreateManyCreatedByInputEnvelope } from './user-create-many-created-by-input-envelope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserUncheckedCreateNestedManyWithoutCreatedByInput {

    @Field(() => [UserCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<UserCreateWithoutCreatedByInput>;

    @Field(() => [UserCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<UserCreateOrConnectWithoutCreatedByInput>;

    @Field(() => UserCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: UserCreateManyCreatedByInputEnvelope;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    connect?: Array<UserWhereUniqueInput>;
}
