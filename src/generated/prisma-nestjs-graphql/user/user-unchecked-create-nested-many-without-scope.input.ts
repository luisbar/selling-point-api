import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutScopeInput } from './user-create-without-scope.input';
import { UserCreateOrConnectWithoutScopeInput } from './user-create-or-connect-without-scope.input';
import { UserCreateManyScopeInputEnvelope } from './user-create-many-scope-input-envelope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserUncheckedCreateNestedManyWithoutScopeInput {

    @Field(() => [UserCreateWithoutScopeInput], {nullable:true})
    create?: Array<UserCreateWithoutScopeInput>;

    @Field(() => [UserCreateOrConnectWithoutScopeInput], {nullable:true})
    connectOrCreate?: Array<UserCreateOrConnectWithoutScopeInput>;

    @Field(() => UserCreateManyScopeInputEnvelope, {nullable:true})
    createMany?: UserCreateManyScopeInputEnvelope;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    connect?: Array<UserWhereUniqueInput>;
}
