import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByInput } from './user-create-without-updated-by.input';
import { UserCreateOrConnectWithoutUpdatedByInput } from './user-create-or-connect-without-updated-by.input';
import { UserCreateManyUpdatedByInputEnvelope } from './user-create-many-updated-by-input-envelope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserUncheckedCreateNestedManyWithoutUpdatedByInput {

    @Field(() => [UserCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<UserCreateWithoutUpdatedByInput>;

    @Field(() => [UserCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<UserCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => UserCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: UserCreateManyUpdatedByInputEnvelope;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    connect?: Array<UserWhereUniqueInput>;
}
