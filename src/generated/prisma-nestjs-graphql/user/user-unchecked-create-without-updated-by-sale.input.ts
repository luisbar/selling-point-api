import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { UserUncheckedCreateNestedManyWithoutCreatedByInput } from './user-unchecked-create-nested-many-without-created-by.input';
import { UserUncheckedCreateNestedManyWithoutUpdatedByInput } from './user-unchecked-create-nested-many-without-updated-by.input';
import { ScopeUncheckedCreateNestedManyWithoutCreatedByInput } from '../scope/scope-unchecked-create-nested-many-without-created-by.input';
import { ScopeUncheckedCreateNestedManyWithoutUpdatedByInput } from '../scope/scope-unchecked-create-nested-many-without-updated-by.input';
import { CustomerUncheckedCreateNestedManyWithoutCreatedByInput } from '../customer/customer-unchecked-create-nested-many-without-created-by.input';
import { CustomerUncheckedCreateNestedManyWithoutUpdatedByInput } from '../customer/customer-unchecked-create-nested-many-without-updated-by.input';
import { SaleUncheckedCreateNestedManyWithoutCreatedByInput } from '../sale/sale-unchecked-create-nested-many-without-created-by.input';
import { InvoiceUncheckedCreateNestedManyWithoutCreatedByInput } from '../invoice/invoice-unchecked-create-nested-many-without-created-by.input';
import { InvoiceUncheckedCreateNestedManyWithoutUpdatedByInput } from '../invoice/invoice-unchecked-create-nested-many-without-updated-by.input';
import { ProductUncheckedCreateNestedManyWithoutCreatedByInput } from '../product/product-unchecked-create-nested-many-without-created-by.input';
import { ProductUncheckedCreateNestedManyWithoutUpdatedByInput } from '../product/product-unchecked-create-nested-many-without-updated-by.input';
import { SaleDetailUncheckedCreateNestedManyWithoutCreatedByInput } from '../sale-detail/sale-detail-unchecked-create-nested-many-without-created-by.input';
import { SaleDetailUncheckedCreateNestedManyWithoutUpdatedByInput } from '../sale-detail/sale-detail-unchecked-create-nested-many-without-updated-by.input';
import { CategoryUncheckedCreateNestedManyWithoutCreatedByInput } from '../category/category-unchecked-create-nested-many-without-created-by.input';
import { CategoryUncheckedCreateNestedManyWithoutUpdatedByInput } from '../category/category-unchecked-create-nested-many-without-updated-by.input';

@InputType()
export class UserUncheckedCreateWithoutUpdatedBySaleInput {

    @Field(() => Int, {nullable:true})
    id?: number;

    @Field(() => String, {nullable:false})
    firstName!: string;

    @Field(() => String, {nullable:false})
    lastName!: string;

    @Field(() => String, {nullable:false})
    username!: string;

    @Field(() => String, {nullable:false})
    password!: string;

    @Field(() => Boolean, {nullable:true})
    enabled?: boolean;

    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;

    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;

    @Field(() => Int, {nullable:true})
    createdById?: number;

    @Field(() => Int, {nullable:true})
    updatedById?: number;

    @Field(() => Int, {nullable:true})
    scopeId?: number;

    @Field(() => UserUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByUser?: UserUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => UserUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByUser?: UserUncheckedCreateNestedManyWithoutUpdatedByInput;

    @Field(() => ScopeUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByScope?: ScopeUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => ScopeUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByScope?: ScopeUncheckedCreateNestedManyWithoutUpdatedByInput;

    @Field(() => CustomerUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByCustomer?: CustomerUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => CustomerUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByCustomer?: CustomerUncheckedCreateNestedManyWithoutUpdatedByInput;

    @Field(() => SaleUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdBySale?: SaleUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => InvoiceUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByInvoice?: InvoiceUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => InvoiceUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByInvoice?: InvoiceUncheckedCreateNestedManyWithoutUpdatedByInput;

    @Field(() => ProductUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByProduct?: ProductUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => ProductUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByProduct?: ProductUncheckedCreateNestedManyWithoutUpdatedByInput;

    @Field(() => SaleDetailUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdBySaleDetail?: SaleDetailUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => SaleDetailUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedBySaleDetail?: SaleDetailUncheckedCreateNestedManyWithoutUpdatedByInput;

    @Field(() => CategoryUncheckedCreateNestedManyWithoutCreatedByInput, {nullable:true})
    createdByCategory?: CategoryUncheckedCreateNestedManyWithoutCreatedByInput;

    @Field(() => CategoryUncheckedCreateNestedManyWithoutUpdatedByInput, {nullable:true})
    updatedByCategory?: CategoryUncheckedCreateNestedManyWithoutUpdatedByInput;
}
