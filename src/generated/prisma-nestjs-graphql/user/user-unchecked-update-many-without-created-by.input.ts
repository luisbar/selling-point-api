import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByInput } from './user-create-without-created-by.input';
import { UserCreateOrConnectWithoutCreatedByInput } from './user-create-or-connect-without-created-by.input';
import { UserUpsertWithWhereUniqueWithoutCreatedByInput } from './user-upsert-with-where-unique-without-created-by.input';
import { UserCreateManyCreatedByInputEnvelope } from './user-create-many-created-by-input-envelope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithWhereUniqueWithoutCreatedByInput } from './user-update-with-where-unique-without-created-by.input';
import { UserUpdateManyWithWhereWithoutCreatedByInput } from './user-update-many-with-where-without-created-by.input';
import { UserScalarWhereInput } from './user-scalar-where.input';

@InputType()
export class UserUncheckedUpdateManyWithoutCreatedByInput {

    @Field(() => [UserCreateWithoutCreatedByInput], {nullable:true})
    create?: Array<UserCreateWithoutCreatedByInput>;

    @Field(() => [UserCreateOrConnectWithoutCreatedByInput], {nullable:true})
    connectOrCreate?: Array<UserCreateOrConnectWithoutCreatedByInput>;

    @Field(() => [UserUpsertWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    upsert?: Array<UserUpsertWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => UserCreateManyCreatedByInputEnvelope, {nullable:true})
    createMany?: UserCreateManyCreatedByInputEnvelope;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    set?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    disconnect?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    delete?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    connect?: Array<UserWhereUniqueInput>;

    @Field(() => [UserUpdateWithWhereUniqueWithoutCreatedByInput], {nullable:true})
    update?: Array<UserUpdateWithWhereUniqueWithoutCreatedByInput>;

    @Field(() => [UserUpdateManyWithWhereWithoutCreatedByInput], {nullable:true})
    updateMany?: Array<UserUpdateManyWithWhereWithoutCreatedByInput>;

    @Field(() => [UserScalarWhereInput], {nullable:true})
    deleteMany?: Array<UserScalarWhereInput>;
}
