import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFieldUpdateOperationsInput } from '../prisma/int-field-update-operations.input';
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { NullableIntFieldUpdateOperationsInput } from '../prisma/nullable-int-field-update-operations.input';
import { UserUncheckedUpdateManyWithoutCreatedByInput } from './user-unchecked-update-many-without-created-by.input';
import { UserUncheckedUpdateManyWithoutUpdatedByInput } from './user-unchecked-update-many-without-updated-by.input';
import { ScopeUncheckedUpdateManyWithoutCreatedByInput } from '../scope/scope-unchecked-update-many-without-created-by.input';
import { ScopeUncheckedUpdateManyWithoutUpdatedByInput } from '../scope/scope-unchecked-update-many-without-updated-by.input';
import { CustomerUncheckedUpdateManyWithoutCreatedByInput } from '../customer/customer-unchecked-update-many-without-created-by.input';
import { CustomerUncheckedUpdateManyWithoutUpdatedByInput } from '../customer/customer-unchecked-update-many-without-updated-by.input';
import { SaleUncheckedUpdateManyWithoutCreatedByInput } from '../sale/sale-unchecked-update-many-without-created-by.input';
import { SaleUncheckedUpdateManyWithoutUpdatedByInput } from '../sale/sale-unchecked-update-many-without-updated-by.input';
import { InvoiceUncheckedUpdateManyWithoutCreatedByInput } from '../invoice/invoice-unchecked-update-many-without-created-by.input';
import { InvoiceUncheckedUpdateManyWithoutUpdatedByInput } from '../invoice/invoice-unchecked-update-many-without-updated-by.input';
import { ProductUncheckedUpdateManyWithoutCreatedByInput } from '../product/product-unchecked-update-many-without-created-by.input';
import { ProductUncheckedUpdateManyWithoutUpdatedByInput } from '../product/product-unchecked-update-many-without-updated-by.input';
import { SaleDetailUncheckedUpdateManyWithoutCreatedByInput } from '../sale-detail/sale-detail-unchecked-update-many-without-created-by.input';
import { SaleDetailUncheckedUpdateManyWithoutUpdatedByInput } from '../sale-detail/sale-detail-unchecked-update-many-without-updated-by.input';
import { CategoryUncheckedUpdateManyWithoutCreatedByInput } from '../category/category-unchecked-update-many-without-created-by.input';
import { CategoryUncheckedUpdateManyWithoutUpdatedByInput } from '../category/category-unchecked-update-many-without-updated-by.input';

@InputType()
export class UserUncheckedUpdateWithoutScopeInput {

    @Field(() => IntFieldUpdateOperationsInput, {nullable:true})
    id?: IntFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    firstName?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    lastName?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    username?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    password?: StringFieldUpdateOperationsInput;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    enabled?: BoolFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => NullableIntFieldUpdateOperationsInput, {nullable:true})
    createdById?: NullableIntFieldUpdateOperationsInput;

    @Field(() => NullableIntFieldUpdateOperationsInput, {nullable:true})
    updatedById?: NullableIntFieldUpdateOperationsInput;

    @Field(() => UserUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByUser?: UserUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => UserUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByUser?: UserUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => ScopeUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByScope?: ScopeUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => ScopeUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByScope?: ScopeUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => CustomerUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByCustomer?: CustomerUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => CustomerUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByCustomer?: CustomerUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => SaleUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdBySale?: SaleUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => SaleUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedBySale?: SaleUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => InvoiceUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByInvoice?: InvoiceUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => InvoiceUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByInvoice?: InvoiceUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => ProductUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByProduct?: ProductUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => ProductUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByProduct?: ProductUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => SaleDetailUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdBySaleDetail?: SaleDetailUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => SaleDetailUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedBySaleDetail?: SaleDetailUncheckedUpdateManyWithoutUpdatedByInput;

    @Field(() => CategoryUncheckedUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByCategory?: CategoryUncheckedUpdateManyWithoutCreatedByInput;

    @Field(() => CategoryUncheckedUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByCategory?: CategoryUncheckedUpdateManyWithoutUpdatedByInput;
}
