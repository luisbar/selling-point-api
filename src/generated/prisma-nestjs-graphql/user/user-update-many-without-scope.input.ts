import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutScopeInput } from './user-create-without-scope.input';
import { UserCreateOrConnectWithoutScopeInput } from './user-create-or-connect-without-scope.input';
import { UserUpsertWithWhereUniqueWithoutScopeInput } from './user-upsert-with-where-unique-without-scope.input';
import { UserCreateManyScopeInputEnvelope } from './user-create-many-scope-input-envelope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithWhereUniqueWithoutScopeInput } from './user-update-with-where-unique-without-scope.input';
import { UserUpdateManyWithWhereWithoutScopeInput } from './user-update-many-with-where-without-scope.input';
import { UserScalarWhereInput } from './user-scalar-where.input';

@InputType()
export class UserUpdateManyWithoutScopeInput {

    @Field(() => [UserCreateWithoutScopeInput], {nullable:true})
    create?: Array<UserCreateWithoutScopeInput>;

    @Field(() => [UserCreateOrConnectWithoutScopeInput], {nullable:true})
    connectOrCreate?: Array<UserCreateOrConnectWithoutScopeInput>;

    @Field(() => [UserUpsertWithWhereUniqueWithoutScopeInput], {nullable:true})
    upsert?: Array<UserUpsertWithWhereUniqueWithoutScopeInput>;

    @Field(() => UserCreateManyScopeInputEnvelope, {nullable:true})
    createMany?: UserCreateManyScopeInputEnvelope;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    set?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    disconnect?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    delete?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    connect?: Array<UserWhereUniqueInput>;

    @Field(() => [UserUpdateWithWhereUniqueWithoutScopeInput], {nullable:true})
    update?: Array<UserUpdateWithWhereUniqueWithoutScopeInput>;

    @Field(() => [UserUpdateManyWithWhereWithoutScopeInput], {nullable:true})
    updateMany?: Array<UserUpdateManyWithWhereWithoutScopeInput>;

    @Field(() => [UserScalarWhereInput], {nullable:true})
    deleteMany?: Array<UserScalarWhereInput>;
}
