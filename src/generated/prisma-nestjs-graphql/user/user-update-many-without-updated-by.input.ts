import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByInput } from './user-create-without-updated-by.input';
import { UserCreateOrConnectWithoutUpdatedByInput } from './user-create-or-connect-without-updated-by.input';
import { UserUpsertWithWhereUniqueWithoutUpdatedByInput } from './user-upsert-with-where-unique-without-updated-by.input';
import { UserCreateManyUpdatedByInputEnvelope } from './user-create-many-updated-by-input-envelope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithWhereUniqueWithoutUpdatedByInput } from './user-update-with-where-unique-without-updated-by.input';
import { UserUpdateManyWithWhereWithoutUpdatedByInput } from './user-update-many-with-where-without-updated-by.input';
import { UserScalarWhereInput } from './user-scalar-where.input';

@InputType()
export class UserUpdateManyWithoutUpdatedByInput {

    @Field(() => [UserCreateWithoutUpdatedByInput], {nullable:true})
    create?: Array<UserCreateWithoutUpdatedByInput>;

    @Field(() => [UserCreateOrConnectWithoutUpdatedByInput], {nullable:true})
    connectOrCreate?: Array<UserCreateOrConnectWithoutUpdatedByInput>;

    @Field(() => [UserUpsertWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    upsert?: Array<UserUpsertWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => UserCreateManyUpdatedByInputEnvelope, {nullable:true})
    createMany?: UserCreateManyUpdatedByInputEnvelope;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    set?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    disconnect?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    delete?: Array<UserWhereUniqueInput>;

    @Field(() => [UserWhereUniqueInput], {nullable:true})
    connect?: Array<UserWhereUniqueInput>;

    @Field(() => [UserUpdateWithWhereUniqueWithoutUpdatedByInput], {nullable:true})
    update?: Array<UserUpdateWithWhereUniqueWithoutUpdatedByInput>;

    @Field(() => [UserUpdateManyWithWhereWithoutUpdatedByInput], {nullable:true})
    updateMany?: Array<UserUpdateManyWithWhereWithoutUpdatedByInput>;

    @Field(() => [UserScalarWhereInput], {nullable:true})
    deleteMany?: Array<UserScalarWhereInput>;
}
