import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByCategoryInput } from './user-create-without-created-by-category.input';
import { UserCreateOrConnectWithoutCreatedByCategoryInput } from './user-create-or-connect-without-created-by-category.input';
import { UserUpsertWithoutCreatedByCategoryInput } from './user-upsert-without-created-by-category.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByCategoryInput } from './user-update-without-created-by-category.input';

@InputType()
export class UserUpdateOneWithoutCreatedByCategoryInput {

    @Field(() => UserCreateWithoutCreatedByCategoryInput, {nullable:true})
    create?: UserCreateWithoutCreatedByCategoryInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByCategoryInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByCategoryInput;

    @Field(() => UserUpsertWithoutCreatedByCategoryInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedByCategoryInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByCategoryInput, {nullable:true})
    update?: UserUpdateWithoutCreatedByCategoryInput;
}
