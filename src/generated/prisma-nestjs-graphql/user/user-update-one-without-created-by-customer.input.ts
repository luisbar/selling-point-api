import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByCustomerInput } from './user-create-without-created-by-customer.input';
import { UserCreateOrConnectWithoutCreatedByCustomerInput } from './user-create-or-connect-without-created-by-customer.input';
import { UserUpsertWithoutCreatedByCustomerInput } from './user-upsert-without-created-by-customer.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByCustomerInput } from './user-update-without-created-by-customer.input';

@InputType()
export class UserUpdateOneWithoutCreatedByCustomerInput {

    @Field(() => UserCreateWithoutCreatedByCustomerInput, {nullable:true})
    create?: UserCreateWithoutCreatedByCustomerInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByCustomerInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByCustomerInput;

    @Field(() => UserUpsertWithoutCreatedByCustomerInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedByCustomerInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByCustomerInput, {nullable:true})
    update?: UserUpdateWithoutCreatedByCustomerInput;
}
