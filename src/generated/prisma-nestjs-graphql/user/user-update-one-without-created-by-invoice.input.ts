import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByInvoiceInput } from './user-create-without-created-by-invoice.input';
import { UserCreateOrConnectWithoutCreatedByInvoiceInput } from './user-create-or-connect-without-created-by-invoice.input';
import { UserUpsertWithoutCreatedByInvoiceInput } from './user-upsert-without-created-by-invoice.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByInvoiceInput } from './user-update-without-created-by-invoice.input';

@InputType()
export class UserUpdateOneWithoutCreatedByInvoiceInput {

    @Field(() => UserCreateWithoutCreatedByInvoiceInput, {nullable:true})
    create?: UserCreateWithoutCreatedByInvoiceInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByInvoiceInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByInvoiceInput;

    @Field(() => UserUpsertWithoutCreatedByInvoiceInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedByInvoiceInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByInvoiceInput, {nullable:true})
    update?: UserUpdateWithoutCreatedByInvoiceInput;
}
