import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByProductInput } from './user-create-without-created-by-product.input';
import { UserCreateOrConnectWithoutCreatedByProductInput } from './user-create-or-connect-without-created-by-product.input';
import { UserUpsertWithoutCreatedByProductInput } from './user-upsert-without-created-by-product.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByProductInput } from './user-update-without-created-by-product.input';

@InputType()
export class UserUpdateOneWithoutCreatedByProductInput {

    @Field(() => UserCreateWithoutCreatedByProductInput, {nullable:true})
    create?: UserCreateWithoutCreatedByProductInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByProductInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByProductInput;

    @Field(() => UserUpsertWithoutCreatedByProductInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedByProductInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByProductInput, {nullable:true})
    update?: UserUpdateWithoutCreatedByProductInput;
}
