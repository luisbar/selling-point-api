import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedBySaleDetailInput } from './user-create-without-created-by-sale-detail.input';
import { UserCreateOrConnectWithoutCreatedBySaleDetailInput } from './user-create-or-connect-without-created-by-sale-detail.input';
import { UserUpsertWithoutCreatedBySaleDetailInput } from './user-upsert-without-created-by-sale-detail.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedBySaleDetailInput } from './user-update-without-created-by-sale-detail.input';

@InputType()
export class UserUpdateOneWithoutCreatedBySaleDetailInput {

    @Field(() => UserCreateWithoutCreatedBySaleDetailInput, {nullable:true})
    create?: UserCreateWithoutCreatedBySaleDetailInput;

    @Field(() => UserCreateOrConnectWithoutCreatedBySaleDetailInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedBySaleDetailInput;

    @Field(() => UserUpsertWithoutCreatedBySaleDetailInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedBySaleDetailInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedBySaleDetailInput, {nullable:true})
    update?: UserUpdateWithoutCreatedBySaleDetailInput;
}
