import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedBySaleInput } from './user-create-without-created-by-sale.input';
import { UserCreateOrConnectWithoutCreatedBySaleInput } from './user-create-or-connect-without-created-by-sale.input';
import { UserUpsertWithoutCreatedBySaleInput } from './user-upsert-without-created-by-sale.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedBySaleInput } from './user-update-without-created-by-sale.input';

@InputType()
export class UserUpdateOneWithoutCreatedBySaleInput {

    @Field(() => UserCreateWithoutCreatedBySaleInput, {nullable:true})
    create?: UserCreateWithoutCreatedBySaleInput;

    @Field(() => UserCreateOrConnectWithoutCreatedBySaleInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedBySaleInput;

    @Field(() => UserUpsertWithoutCreatedBySaleInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedBySaleInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedBySaleInput, {nullable:true})
    update?: UserUpdateWithoutCreatedBySaleInput;
}
