import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByScopeInput } from './user-create-without-created-by-scope.input';
import { UserCreateOrConnectWithoutCreatedByScopeInput } from './user-create-or-connect-without-created-by-scope.input';
import { UserUpsertWithoutCreatedByScopeInput } from './user-upsert-without-created-by-scope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByScopeInput } from './user-update-without-created-by-scope.input';

@InputType()
export class UserUpdateOneWithoutCreatedByScopeInput {

    @Field(() => UserCreateWithoutCreatedByScopeInput, {nullable:true})
    create?: UserCreateWithoutCreatedByScopeInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByScopeInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByScopeInput;

    @Field(() => UserUpsertWithoutCreatedByScopeInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedByScopeInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByScopeInput, {nullable:true})
    update?: UserUpdateWithoutCreatedByScopeInput;
}
