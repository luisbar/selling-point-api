import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutCreatedByUserInput } from './user-create-without-created-by-user.input';
import { UserCreateOrConnectWithoutCreatedByUserInput } from './user-create-or-connect-without-created-by-user.input';
import { UserUpsertWithoutCreatedByUserInput } from './user-upsert-without-created-by-user.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByUserInput } from './user-update-without-created-by-user.input';

@InputType()
export class UserUpdateOneWithoutCreatedByUserInput {

    @Field(() => UserCreateWithoutCreatedByUserInput, {nullable:true})
    create?: UserCreateWithoutCreatedByUserInput;

    @Field(() => UserCreateOrConnectWithoutCreatedByUserInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutCreatedByUserInput;

    @Field(() => UserUpsertWithoutCreatedByUserInput, {nullable:true})
    upsert?: UserUpsertWithoutCreatedByUserInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByUserInput, {nullable:true})
    update?: UserUpdateWithoutCreatedByUserInput;
}
