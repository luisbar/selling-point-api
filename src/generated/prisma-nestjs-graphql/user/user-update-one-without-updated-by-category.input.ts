import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByCategoryInput } from './user-create-without-updated-by-category.input';
import { UserCreateOrConnectWithoutUpdatedByCategoryInput } from './user-create-or-connect-without-updated-by-category.input';
import { UserUpsertWithoutUpdatedByCategoryInput } from './user-upsert-without-updated-by-category.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByCategoryInput } from './user-update-without-updated-by-category.input';

@InputType()
export class UserUpdateOneWithoutUpdatedByCategoryInput {

    @Field(() => UserCreateWithoutUpdatedByCategoryInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByCategoryInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByCategoryInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByCategoryInput;

    @Field(() => UserUpsertWithoutUpdatedByCategoryInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedByCategoryInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByCategoryInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedByCategoryInput;
}
