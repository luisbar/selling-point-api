import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByCustomerInput } from './user-create-without-updated-by-customer.input';
import { UserCreateOrConnectWithoutUpdatedByCustomerInput } from './user-create-or-connect-without-updated-by-customer.input';
import { UserUpsertWithoutUpdatedByCustomerInput } from './user-upsert-without-updated-by-customer.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByCustomerInput } from './user-update-without-updated-by-customer.input';

@InputType()
export class UserUpdateOneWithoutUpdatedByCustomerInput {

    @Field(() => UserCreateWithoutUpdatedByCustomerInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByCustomerInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByCustomerInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByCustomerInput;

    @Field(() => UserUpsertWithoutUpdatedByCustomerInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedByCustomerInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByCustomerInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedByCustomerInput;
}
