import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByInvoiceInput } from './user-create-without-updated-by-invoice.input';
import { UserCreateOrConnectWithoutUpdatedByInvoiceInput } from './user-create-or-connect-without-updated-by-invoice.input';
import { UserUpsertWithoutUpdatedByInvoiceInput } from './user-upsert-without-updated-by-invoice.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByInvoiceInput } from './user-update-without-updated-by-invoice.input';

@InputType()
export class UserUpdateOneWithoutUpdatedByInvoiceInput {

    @Field(() => UserCreateWithoutUpdatedByInvoiceInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByInvoiceInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByInvoiceInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByInvoiceInput;

    @Field(() => UserUpsertWithoutUpdatedByInvoiceInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedByInvoiceInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByInvoiceInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedByInvoiceInput;
}
