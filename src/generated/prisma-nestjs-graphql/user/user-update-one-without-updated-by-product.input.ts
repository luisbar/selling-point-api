import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByProductInput } from './user-create-without-updated-by-product.input';
import { UserCreateOrConnectWithoutUpdatedByProductInput } from './user-create-or-connect-without-updated-by-product.input';
import { UserUpsertWithoutUpdatedByProductInput } from './user-upsert-without-updated-by-product.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByProductInput } from './user-update-without-updated-by-product.input';

@InputType()
export class UserUpdateOneWithoutUpdatedByProductInput {

    @Field(() => UserCreateWithoutUpdatedByProductInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByProductInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByProductInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByProductInput;

    @Field(() => UserUpsertWithoutUpdatedByProductInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedByProductInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByProductInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedByProductInput;
}
