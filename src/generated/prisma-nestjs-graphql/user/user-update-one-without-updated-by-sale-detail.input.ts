import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedBySaleDetailInput } from './user-create-without-updated-by-sale-detail.input';
import { UserCreateOrConnectWithoutUpdatedBySaleDetailInput } from './user-create-or-connect-without-updated-by-sale-detail.input';
import { UserUpsertWithoutUpdatedBySaleDetailInput } from './user-upsert-without-updated-by-sale-detail.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedBySaleDetailInput } from './user-update-without-updated-by-sale-detail.input';

@InputType()
export class UserUpdateOneWithoutUpdatedBySaleDetailInput {

    @Field(() => UserCreateWithoutUpdatedBySaleDetailInput, {nullable:true})
    create?: UserCreateWithoutUpdatedBySaleDetailInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedBySaleDetailInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedBySaleDetailInput;

    @Field(() => UserUpsertWithoutUpdatedBySaleDetailInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedBySaleDetailInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedBySaleDetailInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedBySaleDetailInput;
}
