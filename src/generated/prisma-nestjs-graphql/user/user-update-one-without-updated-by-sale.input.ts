import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedBySaleInput } from './user-create-without-updated-by-sale.input';
import { UserCreateOrConnectWithoutUpdatedBySaleInput } from './user-create-or-connect-without-updated-by-sale.input';
import { UserUpsertWithoutUpdatedBySaleInput } from './user-upsert-without-updated-by-sale.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedBySaleInput } from './user-update-without-updated-by-sale.input';

@InputType()
export class UserUpdateOneWithoutUpdatedBySaleInput {

    @Field(() => UserCreateWithoutUpdatedBySaleInput, {nullable:true})
    create?: UserCreateWithoutUpdatedBySaleInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedBySaleInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedBySaleInput;

    @Field(() => UserUpsertWithoutUpdatedBySaleInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedBySaleInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedBySaleInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedBySaleInput;
}
