import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByScopeInput } from './user-create-without-updated-by-scope.input';
import { UserCreateOrConnectWithoutUpdatedByScopeInput } from './user-create-or-connect-without-updated-by-scope.input';
import { UserUpsertWithoutUpdatedByScopeInput } from './user-upsert-without-updated-by-scope.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByScopeInput } from './user-update-without-updated-by-scope.input';

@InputType()
export class UserUpdateOneWithoutUpdatedByScopeInput {

    @Field(() => UserCreateWithoutUpdatedByScopeInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByScopeInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByScopeInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByScopeInput;

    @Field(() => UserUpsertWithoutUpdatedByScopeInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedByScopeInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByScopeInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedByScopeInput;
}
