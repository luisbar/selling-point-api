import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserCreateWithoutUpdatedByUserInput } from './user-create-without-updated-by-user.input';
import { UserCreateOrConnectWithoutUpdatedByUserInput } from './user-create-or-connect-without-updated-by-user.input';
import { UserUpsertWithoutUpdatedByUserInput } from './user-upsert-without-updated-by-user.input';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByUserInput } from './user-update-without-updated-by-user.input';

@InputType()
export class UserUpdateOneWithoutUpdatedByUserInput {

    @Field(() => UserCreateWithoutUpdatedByUserInput, {nullable:true})
    create?: UserCreateWithoutUpdatedByUserInput;

    @Field(() => UserCreateOrConnectWithoutUpdatedByUserInput, {nullable:true})
    connectOrCreate?: UserCreateOrConnectWithoutUpdatedByUserInput;

    @Field(() => UserUpsertWithoutUpdatedByUserInput, {nullable:true})
    upsert?: UserUpsertWithoutUpdatedByUserInput;

    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;

    @Field(() => Boolean, {nullable:true})
    delete?: boolean;

    @Field(() => UserWhereUniqueInput, {nullable:true})
    connect?: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByUserInput, {nullable:true})
    update?: UserUpdateWithoutUpdatedByUserInput;
}
