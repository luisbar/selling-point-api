import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByInput } from './user-update-without-created-by.input';

@InputType()
export class UserUpdateWithWhereUniqueWithoutCreatedByInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByInput, {nullable:false})
    data!: UserUpdateWithoutCreatedByInput;
}
