import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutScopeInput } from './user-update-without-scope.input';

@InputType()
export class UserUpdateWithWhereUniqueWithoutScopeInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutScopeInput, {nullable:false})
    data!: UserUpdateWithoutScopeInput;
}
