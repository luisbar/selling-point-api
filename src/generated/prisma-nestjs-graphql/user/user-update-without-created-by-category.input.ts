import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input';
import { BoolFieldUpdateOperationsInput } from '../prisma/bool-field-update-operations.input';
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input';
import { UserUpdateOneWithoutCreatedByUserInput } from './user-update-one-without-created-by-user.input';
import { UserUpdateOneWithoutUpdatedByUserInput } from './user-update-one-without-updated-by-user.input';
import { ScopeUpdateOneWithoutUserInput } from '../scope/scope-update-one-without-user.input';
import { UserUpdateManyWithoutCreatedByInput } from './user-update-many-without-created-by.input';
import { UserUpdateManyWithoutUpdatedByInput } from './user-update-many-without-updated-by.input';
import { ScopeUpdateManyWithoutCreatedByInput } from '../scope/scope-update-many-without-created-by.input';
import { ScopeUpdateManyWithoutUpdatedByInput } from '../scope/scope-update-many-without-updated-by.input';
import { CustomerUpdateManyWithoutCreatedByInput } from '../customer/customer-update-many-without-created-by.input';
import { CustomerUpdateManyWithoutUpdatedByInput } from '../customer/customer-update-many-without-updated-by.input';
import { SaleUpdateManyWithoutCreatedByInput } from '../sale/sale-update-many-without-created-by.input';
import { SaleUpdateManyWithoutUpdatedByInput } from '../sale/sale-update-many-without-updated-by.input';
import { InvoiceUpdateManyWithoutCreatedByInput } from '../invoice/invoice-update-many-without-created-by.input';
import { InvoiceUpdateManyWithoutUpdatedByInput } from '../invoice/invoice-update-many-without-updated-by.input';
import { ProductUpdateManyWithoutCreatedByInput } from '../product/product-update-many-without-created-by.input';
import { ProductUpdateManyWithoutUpdatedByInput } from '../product/product-update-many-without-updated-by.input';
import { SaleDetailUpdateManyWithoutCreatedByInput } from '../sale-detail/sale-detail-update-many-without-created-by.input';
import { SaleDetailUpdateManyWithoutUpdatedByInput } from '../sale-detail/sale-detail-update-many-without-updated-by.input';
import { CategoryUpdateManyWithoutUpdatedByInput } from '../category/category-update-many-without-updated-by.input';

@InputType()
export class UserUpdateWithoutCreatedByCategoryInput {

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    firstName?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    lastName?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    username?: StringFieldUpdateOperationsInput;

    @Field(() => StringFieldUpdateOperationsInput, {nullable:true})
    password?: StringFieldUpdateOperationsInput;

    @Field(() => BoolFieldUpdateOperationsInput, {nullable:true})
    enabled?: BoolFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    createdAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => DateTimeFieldUpdateOperationsInput, {nullable:true})
    updatedAt?: DateTimeFieldUpdateOperationsInput;

    @Field(() => UserUpdateOneWithoutCreatedByUserInput, {nullable:true})
    createdBy?: UserUpdateOneWithoutCreatedByUserInput;

    @Field(() => UserUpdateOneWithoutUpdatedByUserInput, {nullable:true})
    updatedBy?: UserUpdateOneWithoutUpdatedByUserInput;

    @Field(() => ScopeUpdateOneWithoutUserInput, {nullable:true})
    scope?: ScopeUpdateOneWithoutUserInput;

    @Field(() => UserUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByUser?: UserUpdateManyWithoutCreatedByInput;

    @Field(() => UserUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByUser?: UserUpdateManyWithoutUpdatedByInput;

    @Field(() => ScopeUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByScope?: ScopeUpdateManyWithoutCreatedByInput;

    @Field(() => ScopeUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByScope?: ScopeUpdateManyWithoutUpdatedByInput;

    @Field(() => CustomerUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByCustomer?: CustomerUpdateManyWithoutCreatedByInput;

    @Field(() => CustomerUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByCustomer?: CustomerUpdateManyWithoutUpdatedByInput;

    @Field(() => SaleUpdateManyWithoutCreatedByInput, {nullable:true})
    createdBySale?: SaleUpdateManyWithoutCreatedByInput;

    @Field(() => SaleUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedBySale?: SaleUpdateManyWithoutUpdatedByInput;

    @Field(() => InvoiceUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByInvoice?: InvoiceUpdateManyWithoutCreatedByInput;

    @Field(() => InvoiceUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByInvoice?: InvoiceUpdateManyWithoutUpdatedByInput;

    @Field(() => ProductUpdateManyWithoutCreatedByInput, {nullable:true})
    createdByProduct?: ProductUpdateManyWithoutCreatedByInput;

    @Field(() => ProductUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByProduct?: ProductUpdateManyWithoutUpdatedByInput;

    @Field(() => SaleDetailUpdateManyWithoutCreatedByInput, {nullable:true})
    createdBySaleDetail?: SaleDetailUpdateManyWithoutCreatedByInput;

    @Field(() => SaleDetailUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedBySaleDetail?: SaleDetailUpdateManyWithoutUpdatedByInput;

    @Field(() => CategoryUpdateManyWithoutUpdatedByInput, {nullable:true})
    updatedByCategory?: CategoryUpdateManyWithoutUpdatedByInput;
}
