import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutCreatedByInput } from './user-update-without-created-by.input';
import { UserCreateWithoutCreatedByInput } from './user-create-without-created-by.input';

@InputType()
export class UserUpsertWithWhereUniqueWithoutCreatedByInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutCreatedByInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByInput;

    @Field(() => UserCreateWithoutCreatedByInput, {nullable:false})
    create!: UserCreateWithoutCreatedByInput;
}
