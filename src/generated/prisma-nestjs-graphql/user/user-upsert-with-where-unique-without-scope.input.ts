import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutScopeInput } from './user-update-without-scope.input';
import { UserCreateWithoutScopeInput } from './user-create-without-scope.input';

@InputType()
export class UserUpsertWithWhereUniqueWithoutScopeInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutScopeInput, {nullable:false})
    update!: UserUpdateWithoutScopeInput;

    @Field(() => UserCreateWithoutScopeInput, {nullable:false})
    create!: UserCreateWithoutScopeInput;
}
