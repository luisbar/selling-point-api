import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserWhereUniqueInput } from './user-where-unique.input';
import { UserUpdateWithoutUpdatedByInput } from './user-update-without-updated-by.input';
import { UserCreateWithoutUpdatedByInput } from './user-create-without-updated-by.input';

@InputType()
export class UserUpsertWithWhereUniqueWithoutUpdatedByInput {

    @Field(() => UserWhereUniqueInput, {nullable:false})
    where!: UserWhereUniqueInput;

    @Field(() => UserUpdateWithoutUpdatedByInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByInput;

    @Field(() => UserCreateWithoutUpdatedByInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByInput;
}
