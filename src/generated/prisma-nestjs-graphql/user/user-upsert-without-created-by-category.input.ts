import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedByCategoryInput } from './user-update-without-created-by-category.input';
import { UserCreateWithoutCreatedByCategoryInput } from './user-create-without-created-by-category.input';

@InputType()
export class UserUpsertWithoutCreatedByCategoryInput {

    @Field(() => UserUpdateWithoutCreatedByCategoryInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByCategoryInput;

    @Field(() => UserCreateWithoutCreatedByCategoryInput, {nullable:false})
    create!: UserCreateWithoutCreatedByCategoryInput;
}
