import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedByCustomerInput } from './user-update-without-created-by-customer.input';
import { UserCreateWithoutCreatedByCustomerInput } from './user-create-without-created-by-customer.input';

@InputType()
export class UserUpsertWithoutCreatedByCustomerInput {

    @Field(() => UserUpdateWithoutCreatedByCustomerInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByCustomerInput;

    @Field(() => UserCreateWithoutCreatedByCustomerInput, {nullable:false})
    create!: UserCreateWithoutCreatedByCustomerInput;
}
