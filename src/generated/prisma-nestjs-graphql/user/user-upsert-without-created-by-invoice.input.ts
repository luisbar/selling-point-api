import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedByInvoiceInput } from './user-update-without-created-by-invoice.input';
import { UserCreateWithoutCreatedByInvoiceInput } from './user-create-without-created-by-invoice.input';

@InputType()
export class UserUpsertWithoutCreatedByInvoiceInput {

    @Field(() => UserUpdateWithoutCreatedByInvoiceInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByInvoiceInput;

    @Field(() => UserCreateWithoutCreatedByInvoiceInput, {nullable:false})
    create!: UserCreateWithoutCreatedByInvoiceInput;
}
