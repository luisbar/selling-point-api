import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedByProductInput } from './user-update-without-created-by-product.input';
import { UserCreateWithoutCreatedByProductInput } from './user-create-without-created-by-product.input';

@InputType()
export class UserUpsertWithoutCreatedByProductInput {

    @Field(() => UserUpdateWithoutCreatedByProductInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByProductInput;

    @Field(() => UserCreateWithoutCreatedByProductInput, {nullable:false})
    create!: UserCreateWithoutCreatedByProductInput;
}
