import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedBySaleDetailInput } from './user-update-without-created-by-sale-detail.input';
import { UserCreateWithoutCreatedBySaleDetailInput } from './user-create-without-created-by-sale-detail.input';

@InputType()
export class UserUpsertWithoutCreatedBySaleDetailInput {

    @Field(() => UserUpdateWithoutCreatedBySaleDetailInput, {nullable:false})
    update!: UserUpdateWithoutCreatedBySaleDetailInput;

    @Field(() => UserCreateWithoutCreatedBySaleDetailInput, {nullable:false})
    create!: UserCreateWithoutCreatedBySaleDetailInput;
}
