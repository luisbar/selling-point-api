import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedBySaleInput } from './user-update-without-created-by-sale.input';
import { UserCreateWithoutCreatedBySaleInput } from './user-create-without-created-by-sale.input';

@InputType()
export class UserUpsertWithoutCreatedBySaleInput {

    @Field(() => UserUpdateWithoutCreatedBySaleInput, {nullable:false})
    update!: UserUpdateWithoutCreatedBySaleInput;

    @Field(() => UserCreateWithoutCreatedBySaleInput, {nullable:false})
    create!: UserCreateWithoutCreatedBySaleInput;
}
