import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedByScopeInput } from './user-update-without-created-by-scope.input';
import { UserCreateWithoutCreatedByScopeInput } from './user-create-without-created-by-scope.input';

@InputType()
export class UserUpsertWithoutCreatedByScopeInput {

    @Field(() => UserUpdateWithoutCreatedByScopeInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByScopeInput;

    @Field(() => UserCreateWithoutCreatedByScopeInput, {nullable:false})
    create!: UserCreateWithoutCreatedByScopeInput;
}
