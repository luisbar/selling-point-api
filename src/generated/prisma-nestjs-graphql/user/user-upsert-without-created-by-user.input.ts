import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutCreatedByUserInput } from './user-update-without-created-by-user.input';
import { UserCreateWithoutCreatedByUserInput } from './user-create-without-created-by-user.input';

@InputType()
export class UserUpsertWithoutCreatedByUserInput {

    @Field(() => UserUpdateWithoutCreatedByUserInput, {nullable:false})
    update!: UserUpdateWithoutCreatedByUserInput;

    @Field(() => UserCreateWithoutCreatedByUserInput, {nullable:false})
    create!: UserCreateWithoutCreatedByUserInput;
}
