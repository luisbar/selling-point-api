import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedByCategoryInput } from './user-update-without-updated-by-category.input';
import { UserCreateWithoutUpdatedByCategoryInput } from './user-create-without-updated-by-category.input';

@InputType()
export class UserUpsertWithoutUpdatedByCategoryInput {

    @Field(() => UserUpdateWithoutUpdatedByCategoryInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByCategoryInput;

    @Field(() => UserCreateWithoutUpdatedByCategoryInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByCategoryInput;
}
