import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedByCustomerInput } from './user-update-without-updated-by-customer.input';
import { UserCreateWithoutUpdatedByCustomerInput } from './user-create-without-updated-by-customer.input';

@InputType()
export class UserUpsertWithoutUpdatedByCustomerInput {

    @Field(() => UserUpdateWithoutUpdatedByCustomerInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByCustomerInput;

    @Field(() => UserCreateWithoutUpdatedByCustomerInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByCustomerInput;
}
