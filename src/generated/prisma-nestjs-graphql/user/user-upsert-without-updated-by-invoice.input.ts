import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedByInvoiceInput } from './user-update-without-updated-by-invoice.input';
import { UserCreateWithoutUpdatedByInvoiceInput } from './user-create-without-updated-by-invoice.input';

@InputType()
export class UserUpsertWithoutUpdatedByInvoiceInput {

    @Field(() => UserUpdateWithoutUpdatedByInvoiceInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByInvoiceInput;

    @Field(() => UserCreateWithoutUpdatedByInvoiceInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByInvoiceInput;
}
