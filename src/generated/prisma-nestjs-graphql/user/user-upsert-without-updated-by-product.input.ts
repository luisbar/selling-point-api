import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedByProductInput } from './user-update-without-updated-by-product.input';
import { UserCreateWithoutUpdatedByProductInput } from './user-create-without-updated-by-product.input';

@InputType()
export class UserUpsertWithoutUpdatedByProductInput {

    @Field(() => UserUpdateWithoutUpdatedByProductInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByProductInput;

    @Field(() => UserCreateWithoutUpdatedByProductInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByProductInput;
}
