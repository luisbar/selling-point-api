import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedBySaleDetailInput } from './user-update-without-updated-by-sale-detail.input';
import { UserCreateWithoutUpdatedBySaleDetailInput } from './user-create-without-updated-by-sale-detail.input';

@InputType()
export class UserUpsertWithoutUpdatedBySaleDetailInput {

    @Field(() => UserUpdateWithoutUpdatedBySaleDetailInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedBySaleDetailInput;

    @Field(() => UserCreateWithoutUpdatedBySaleDetailInput, {nullable:false})
    create!: UserCreateWithoutUpdatedBySaleDetailInput;
}
