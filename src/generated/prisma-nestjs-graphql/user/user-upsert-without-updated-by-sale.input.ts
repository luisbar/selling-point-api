import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedBySaleInput } from './user-update-without-updated-by-sale.input';
import { UserCreateWithoutUpdatedBySaleInput } from './user-create-without-updated-by-sale.input';

@InputType()
export class UserUpsertWithoutUpdatedBySaleInput {

    @Field(() => UserUpdateWithoutUpdatedBySaleInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedBySaleInput;

    @Field(() => UserCreateWithoutUpdatedBySaleInput, {nullable:false})
    create!: UserCreateWithoutUpdatedBySaleInput;
}
