import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedByScopeInput } from './user-update-without-updated-by-scope.input';
import { UserCreateWithoutUpdatedByScopeInput } from './user-create-without-updated-by-scope.input';

@InputType()
export class UserUpsertWithoutUpdatedByScopeInput {

    @Field(() => UserUpdateWithoutUpdatedByScopeInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByScopeInput;

    @Field(() => UserCreateWithoutUpdatedByScopeInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByScopeInput;
}
