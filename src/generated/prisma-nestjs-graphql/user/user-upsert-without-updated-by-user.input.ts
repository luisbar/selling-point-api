import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { UserUpdateWithoutUpdatedByUserInput } from './user-update-without-updated-by-user.input';
import { UserCreateWithoutUpdatedByUserInput } from './user-create-without-updated-by-user.input';

@InputType()
export class UserUpsertWithoutUpdatedByUserInput {

    @Field(() => UserUpdateWithoutUpdatedByUserInput, {nullable:false})
    update!: UserUpdateWithoutUpdatedByUserInput;

    @Field(() => UserCreateWithoutUpdatedByUserInput, {nullable:false})
    create!: UserCreateWithoutUpdatedByUserInput;
}
