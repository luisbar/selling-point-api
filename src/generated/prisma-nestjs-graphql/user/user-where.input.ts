import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { IntFilter } from '../prisma/int-filter.input';
import { StringFilter } from '../prisma/string-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { UserRelationFilter } from './user-relation-filter.input';
import { IntNullableFilter } from '../prisma/int-nullable-filter.input';
import { ScopeRelationFilter } from '../scope/scope-relation-filter.input';
import { UserListRelationFilter } from './user-list-relation-filter.input';
import { ScopeListRelationFilter } from '../scope/scope-list-relation-filter.input';
import { CustomerListRelationFilter } from '../customer/customer-list-relation-filter.input';
import { SaleListRelationFilter } from '../sale/sale-list-relation-filter.input';
import { InvoiceListRelationFilter } from '../invoice/invoice-list-relation-filter.input';
import { ProductListRelationFilter } from '../product/product-list-relation-filter.input';
import { SaleDetailListRelationFilter } from '../sale-detail/sale-detail-list-relation-filter.input';
import { CategoryListRelationFilter } from '../category/category-list-relation-filter.input';

@InputType()
export class UserWhereInput {

    @Field(() => [UserWhereInput], {nullable:true})
    AND?: Array<UserWhereInput>;

    @Field(() => [UserWhereInput], {nullable:true})
    OR?: Array<UserWhereInput>;

    @Field(() => [UserWhereInput], {nullable:true})
    NOT?: Array<UserWhereInput>;

    @Field(() => IntFilter, {nullable:true})
    id?: IntFilter;

    @Field(() => StringFilter, {nullable:true})
    firstName?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    lastName?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    username?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    password?: StringFilter;

    @Field(() => BoolFilter, {nullable:true})
    enabled?: BoolFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: DateTimeFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    createdBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    createdById?: IntNullableFilter;

    @Field(() => UserRelationFilter, {nullable:true})
    updatedBy?: UserRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    updatedById?: IntNullableFilter;

    @Field(() => ScopeRelationFilter, {nullable:true})
    scope?: ScopeRelationFilter;

    @Field(() => IntNullableFilter, {nullable:true})
    scopeId?: IntNullableFilter;

    @Field(() => UserListRelationFilter, {nullable:true})
    createdByUser?: UserListRelationFilter;

    @Field(() => UserListRelationFilter, {nullable:true})
    updatedByUser?: UserListRelationFilter;

    @Field(() => ScopeListRelationFilter, {nullable:true})
    createdByScope?: ScopeListRelationFilter;

    @Field(() => ScopeListRelationFilter, {nullable:true})
    updatedByScope?: ScopeListRelationFilter;

    @Field(() => CustomerListRelationFilter, {nullable:true})
    createdByCustomer?: CustomerListRelationFilter;

    @Field(() => CustomerListRelationFilter, {nullable:true})
    updatedByCustomer?: CustomerListRelationFilter;

    @Field(() => SaleListRelationFilter, {nullable:true})
    createdBySale?: SaleListRelationFilter;

    @Field(() => SaleListRelationFilter, {nullable:true})
    updatedBySale?: SaleListRelationFilter;

    @Field(() => InvoiceListRelationFilter, {nullable:true})
    createdByInvoice?: InvoiceListRelationFilter;

    @Field(() => InvoiceListRelationFilter, {nullable:true})
    updatedByInvoice?: InvoiceListRelationFilter;

    @Field(() => ProductListRelationFilter, {nullable:true})
    createdByProduct?: ProductListRelationFilter;

    @Field(() => ProductListRelationFilter, {nullable:true})
    updatedByProduct?: ProductListRelationFilter;

    @Field(() => SaleDetailListRelationFilter, {nullable:true})
    createdBySaleDetail?: SaleDetailListRelationFilter;

    @Field(() => SaleDetailListRelationFilter, {nullable:true})
    updatedBySaleDetail?: SaleDetailListRelationFilter;

    @Field(() => CategoryListRelationFilter, {nullable:true})
    createdByCategory?: CategoryListRelationFilter;

    @Field(() => CategoryListRelationFilter, {nullable:true})
    updatedByCategory?: CategoryListRelationFilter;
}
