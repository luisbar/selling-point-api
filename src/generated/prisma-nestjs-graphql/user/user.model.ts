import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';
import { Scope } from '../scope/scope.model';
import { Customer } from '../customer/customer.model';
import { Sale } from '../sale/sale.model';
import { Invoice } from '../invoice/invoice.model';
import { Product } from '../product/product.model';
import { SaleDetail } from '../sale-detail/sale-detail.model';
import { Category } from '../category/category.model';
import { UserCount } from './user-count.output';

@ObjectType()
export class User {

    @Field(() => ID, {nullable:false})
    id!: number;

    @Field(() => String, {nullable:false})
    firstName!: string;

    @Field(() => String, {nullable:false})
    lastName!: string;

    @Field(() => String, {nullable:false})
    username!: string;

    @Field(() => String, {nullable:false})
    password!: string;

    @Field(() => Boolean, {nullable:false,defaultValue:true})
    enabled!: boolean;

    @Field(() => Date, {nullable:false})
    createdAt!: Date;

    @Field(() => Date, {nullable:false})
    updatedAt!: Date;

    @Field(() => User, {nullable:true})
    createdBy?: User | null;

    @Field(() => Int, {nullable:true})
    createdById!: number | null;

    @Field(() => User, {nullable:true})
    updatedBy?: User | null;

    @Field(() => Int, {nullable:true})
    updatedById!: number | null;

    @Field(() => Scope, {nullable:true})
    scope?: Scope | null;

    @Field(() => Int, {nullable:true})
    scopeId!: number | null;

    @Field(() => [User], {nullable:true})
    createdByUser?: Array<User>;

    @Field(() => [User], {nullable:true})
    updatedByUser?: Array<User>;

    @Field(() => [Scope], {nullable:true})
    createdByScope?: Array<Scope>;

    @Field(() => [Scope], {nullable:true})
    updatedByScope?: Array<Scope>;

    @Field(() => [Customer], {nullable:true})
    createdByCustomer?: Array<Customer>;

    @Field(() => [Customer], {nullable:true})
    updatedByCustomer?: Array<Customer>;

    @Field(() => [Sale], {nullable:true})
    createdBySale?: Array<Sale>;

    @Field(() => [Sale], {nullable:true})
    updatedBySale?: Array<Sale>;

    @Field(() => [Invoice], {nullable:true})
    createdByInvoice?: Array<Invoice>;

    @Field(() => [Invoice], {nullable:true})
    updatedByInvoice?: Array<Invoice>;

    @Field(() => [Product], {nullable:true})
    createdByProduct?: Array<Product>;

    @Field(() => [Product], {nullable:true})
    updatedByProduct?: Array<Product>;

    @Field(() => [SaleDetail], {nullable:true})
    createdBySaleDetail?: Array<SaleDetail>;

    @Field(() => [SaleDetail], {nullable:true})
    updatedBySaleDetail?: Array<SaleDetail>;

    @Field(() => [Category], {nullable:true})
    createdByCategory?: Array<Category>;

    @Field(() => [Category], {nullable:true})
    updatedByCategory?: Array<Category>;

    @Field(() => UserCount, {nullable:false})
    _count?: UserCount;
}
