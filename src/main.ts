import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './common/logger/logger.service';
import { PrismaService } from './common/prisma/prisma.service';

const setupLogger = (app) => {
  app.useLogger(new LoggerService().setContext('Default'));
}

const setupPrisma = (app) => {
  const prismaService: PrismaService = app.get(PrismaService);
  prismaService.enableShutdownHooks(app);
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });
  setupLogger(app);
  setupPrisma(app);
  await app.listen(3000);
}

bootstrap();
