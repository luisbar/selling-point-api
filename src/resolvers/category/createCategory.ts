import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { CategoryCreateInput } from '../../generated/prisma-nestjs-graphql/category/category-create.input';
import { Category } from '../../generated/prisma-nestjs-graphql/category/category.model';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Category)
export class CreateCategoryResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(CreateCategoryResolver.name);
  }

  @Mutation(returns => Category)
  async createCategory(
    @Context() context,
    @Args('categoryCreateInput', { type: () => CategoryCreateInput }) categoryCreateInput: CategoryCreateInput,
  ) {
    try {
      if (!context.ability.can('create', 'Category')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        data: {
          ...categoryCreateInput,
          createdBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          createdBy: true,
        },
      };

      return await this.prisma.category.create(query);
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );

      throw this.logger.error(
        await this.i18n.translate('errors.category.cannot.create', { lang: context.language }),
        error,
      );
    }
  }
}