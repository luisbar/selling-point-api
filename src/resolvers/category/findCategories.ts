import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { Category } from '../../generated/prisma-nestjs-graphql/category/category.model';
import { FindManyCategoryArgs } from '../../generated/prisma-nestjs-graphql/category/find-many-category.args';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Category)
export class FindCategoriesResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindCategoriesResolver.name);
  }

  @Query(returns => [Category])
  async findCategories(
    @Context() context,
    @Args('args', { type: () => FindManyCategoryArgs }) args: FindManyCategoryArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Category')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          product: true,
        },
      };

      return await this.prisma.category.findMany(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.category.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}