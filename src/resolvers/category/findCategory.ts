import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { FindUniqueCategoryArgs } from '../../generated/prisma-nestjs-graphql/category/find-unique-category.args';
import { Category } from '../../generated/prisma-nestjs-graphql/category/category.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Category)
export class FindCategoryResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindCategoryResolver.name);
  }

  @Query(returns => Category)
  async findCategory(
    @Context() context,
    @Args('args', { type: () => FindUniqueCategoryArgs }) args: FindUniqueCategoryArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Category')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          product: true,
        },
      };
      const result = await this.prisma.category.findUnique(query);
      if (!result) throw new ApiException('Not found', HttpStatus.NOT_FOUND, 'errors.not.found', {});
      
      return result;
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.category.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}