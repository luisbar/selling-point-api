import { Args, Context, Int, Mutation, Resolver } from '@nestjs/graphql';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { CategoryUpdateInput } from '../../generated/prisma-nestjs-graphql/category/category-update.input';
import { Category } from '../../generated/prisma-nestjs-graphql/category/category.model';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Category)
export class UpdateCategoryResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(UpdateCategoryResolver.name);
  }

  @Mutation(returns => Category)
  async updateCategory(
    @Context() context,
    @Args('id', { type: () => Int }) id: number,
    @Args('categoryUpdateInput', { type: () => CategoryUpdateInput }) categoryUpdateInput: CategoryUpdateInput,
  ) {
    try {
      if (!context.ability.can('update', 'Category')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );
      
      const query = {
        where: {
          id,
        },
        data: {
          ...categoryUpdateInput,
          updatedBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          updatedBy: true,
        },
      };

      return await this.prisma.category.update(query)
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.category.cannot.update', { lang: context.language }),
        error,
      );
    }
  }
}