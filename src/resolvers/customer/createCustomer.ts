import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { CustomerCreateInput } from '../../generated/prisma-nestjs-graphql/customer/customer-create.input';
import { Customer } from '../../generated/prisma-nestjs-graphql/customer/customer.model';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Customer)
export class CreateCustomerResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(CreateCustomerResolver.name);
  }

  @Mutation(returns => Customer)
  async createCustomer(
    @Context() context,
    @Args('customerCreateInput', { type: () => CustomerCreateInput }) customerCreateInput: CustomerCreateInput,
  ) {
    try {
      if (!context.ability.can('create', 'Customer')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        data: {
          ...customerCreateInput,
          createdBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          createdBy: true,
        },
      };

      return await this.prisma.customer.create(query);
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.customer.cannot.create', { lang: context.language }),
        error,
      );
    }
  }
}