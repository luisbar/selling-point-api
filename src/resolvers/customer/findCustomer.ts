import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { FindUniqueCustomerArgs } from '../../generated/prisma-nestjs-graphql/customer/find-unique-customer.args';
import { Customer } from '../../generated/prisma-nestjs-graphql/customer/customer.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Customer)
export class FindCustomerResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindCustomerResolver.name);
  }

  @Query(returns => Customer)
  async findCustomer(
    @Context() context,
    @Args('args', { type: () => FindUniqueCustomerArgs }) args: FindUniqueCustomerArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Customer')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          sale: true,
        },
      };
      const result = await this.prisma.customer.findUnique(query);
      if (!result) throw new ApiException('Not found', HttpStatus.NOT_FOUND, 'errors.not.found', {});
      
      return result;
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.customer.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}