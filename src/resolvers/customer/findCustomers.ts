import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { Customer } from '../../generated/prisma-nestjs-graphql/customer/customer.model';
import { FindManyCustomerArgs } from '../../generated/prisma-nestjs-graphql/customer/find-many-customer.args';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Customer)
export class FindCustomersResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindCustomersResolver.name);
  }

  @Query(returns => [Customer])
  async findCustomers(
    @Context() context,
    @Args('args', { type: () => FindManyCustomerArgs }) args: FindManyCustomerArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Customer')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          sale: true,
        },
      };

      return await this.prisma.customer.findMany(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.customer.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}