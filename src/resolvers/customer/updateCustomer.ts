import { Args, Context, Int, Mutation, Resolver } from '@nestjs/graphql';
import { CustomerUpdateInput } from '../../generated/prisma-nestjs-graphql/customer/customer-update.input';
import { Customer } from '../../generated/prisma-nestjs-graphql/customer/customer.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Customer)
export class UpdateCustomerResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(UpdateCustomerResolver.name);
  }

  @Mutation(returns => Customer)
  async updateCustomer(
    @Context() context,
    @Args('id', { type: () => Int }) id: number,
    @Args('customerUpdateInput', { type: () => CustomerUpdateInput }) customerUpdateInput: CustomerUpdateInput,
  ) {
    try {
      if (!context.ability.can('update', 'Customer')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );
      
      const query = {
        where: {
          id,
        },
        data: {
          ...customerUpdateInput,
          updatedBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          updatedBy: true,
        },
      };

      return await this.prisma.customer.update(query)
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.customer.cannot.update', { lang: context.language }),
        error,
      );
    }
  }
}