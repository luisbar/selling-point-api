import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { InvoiceCreateInput } from '../../generated/prisma-nestjs-graphql/invoice/invoice-create.input';
import { Invoice } from '../../generated/prisma-nestjs-graphql/invoice/invoice.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Invoice)
export class CreateInvoiceResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(CreateInvoiceResolver.name);
  }

  @Mutation(returns => Invoice)
  async createInvoice(
    @Context() context,
    @Args('invoiceCreateInput', { type: () => InvoiceCreateInput }) invoiceCreateInput: InvoiceCreateInput,
  ) {
    try {
      if (!context.ability.can('create', 'Invoice')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        data: {
          ...invoiceCreateInput,
          createdBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          createdBy: true,
        },
      };

      return await this.prisma.invoice.create(query);
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.invoice.cannot.create', { lang: context.language }),
        error,
      );
    }
  }
}