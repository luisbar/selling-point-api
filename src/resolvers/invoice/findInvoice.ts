import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { FindUniqueInvoiceArgs } from '../../generated/prisma-nestjs-graphql/invoice/find-unique-invoice.args';
import { Invoice } from '../../generated/prisma-nestjs-graphql/invoice/invoice.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Invoice)
export class FindInvoiceResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindInvoiceResolver.name);
  }

  @Query(returns => Invoice)
  async findInvoice(
    @Context() context,
    @Args('args', { type: () => FindUniqueInvoiceArgs }) args: FindUniqueInvoiceArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Invoice')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          sale: true,
        },
      };
      const result = await this.prisma.invoice.findUnique(query);
      if (!result) throw new ApiException('Not found', HttpStatus.NOT_FOUND, 'errors.not.found', {});
      
      return result;
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.invoice.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}