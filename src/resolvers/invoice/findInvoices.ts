import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { Invoice } from '../../generated/prisma-nestjs-graphql/invoice/invoice.model';
import { FindManyInvoiceArgs } from '../../generated/prisma-nestjs-graphql/invoice/find-many-invoice.args';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Invoice)
export class FindInvoicesResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindInvoicesResolver.name);
  }

  @Query(returns => [Invoice])
  async findInvoices(
    @Context() context,
    @Args('args', { type: () => FindManyInvoiceArgs }) args: FindManyInvoiceArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Invoice')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          sale: true,
        },
      };

      return await this.prisma.invoice.findMany(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.invoice.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}