import { Args, Context, Int, Mutation, Resolver } from '@nestjs/graphql';
import { InvoiceUpdateInput } from '../../generated/prisma-nestjs-graphql/invoice/invoice-update.input';
import { Invoice } from '../../generated/prisma-nestjs-graphql/invoice/invoice.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Invoice)
export class UpdateInvoiceResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(UpdateInvoiceResolver.name);
  }

  @Mutation(returns => Invoice)
  async updateInvoice(
    @Context() context,
    @Args('id', { type: () => Int }) id: number,
    @Args('invoiceUpdateInput', { type: () => InvoiceUpdateInput }) invoiceUpdateInput: InvoiceUpdateInput,
  ) {
    try {
      if (!context.ability.can('update', 'Invoice')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );
      
      const query = {
        where: {
          id,
        },
        data: {
          ...invoiceUpdateInput,
          updatedBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          updatedBy: true,
        },
      };

      return await this.prisma.invoice.update(query)
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.invoice.cannot.update', { lang: context.language }),
        error,
      );
    }
  }
}