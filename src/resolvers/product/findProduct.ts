import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { FindUniqueProductArgs } from '../../generated/prisma-nestjs-graphql/product/find-unique-product.args';
import { Product } from '../../generated/prisma-nestjs-graphql/product/product.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Product)
export class FindProductResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindProductResolver.name);
  }

  @Query(returns => Product)
  async findProduct(
    @Context() context,
    @Args('args', { type: () => FindUniqueProductArgs }) args: FindUniqueProductArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Product')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          saleDetail: true,
          category: true,
        },
      };
      const result = await this.prisma.product.findUnique(query);
      if (!result) throw new ApiException('Not found', HttpStatus.NOT_FOUND, 'errors.not.found', {});
      
      return result;
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.product.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}