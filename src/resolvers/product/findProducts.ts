import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { Product } from '../../generated/prisma-nestjs-graphql/product/product.model';
import { FindManyProductArgs } from '../../generated/prisma-nestjs-graphql/product/find-many-product.args';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Product)
export class FindProductsResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindProductsResolver.name);
  }

  @Query(returns => [Product])
  async findProducts(
    @Context() context,
    @Args('args', { type: () => FindManyProductArgs }) args: FindManyProductArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Product')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          saleDetail: true,
          category: true,
        },
      };

      return await this.prisma.product.findMany(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.product.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}