import { Args, Context, Int, Mutation, Resolver } from '@nestjs/graphql';
import { ProductUpdateInput } from '../../generated/prisma-nestjs-graphql/product/product-update.input';
import { Product } from '../../generated/prisma-nestjs-graphql/product/product.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Product)
export class UpdateProductResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(UpdateProductResolver.name);
  }

  @Mutation(returns => Product)
  async updateProduct(
    @Context() context,
    @Args('id', { type: () => Int }) id: number,
    @Args('productUpdateInput', { type: () => ProductUpdateInput }) productUpdateInput: ProductUpdateInput,
  ) {
    try {
      if (!context.ability.can('update', 'Product')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );
      
      const query = {
        where: {
          id,
        },
        data: {
          ...productUpdateInput,
          updatedBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          updatedBy: true,
        },
      };

      return await this.prisma.product.update(query)
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.product.cannot.update', { lang: context.language }),
        error,
      );
    }
  }
}