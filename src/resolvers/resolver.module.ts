import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import * as resolvers from './index';

@Module({
  providers: [
    ...Object.values(resolvers).map(resolver => resolver),
  ],
  imports: [
    CommonModule,
  ],
})
export class ResolversModule {}
