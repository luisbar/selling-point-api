import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { SaleAggregateArgs } from '../../generated/prisma-nestjs-graphql/sale/sale-aggregate.args';
import { AggregateSale } from '../../generated/prisma-nestjs-graphql/sale/aggregate-sale.output';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => AggregateSale)
export class AggregateSalesResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(AggregateSalesResolver.name);
  }

  @Query(returns => AggregateSale)
  async aggregateSales(
    @Context() context,
    @Args('args', { type: () => SaleAggregateArgs }) args: SaleAggregateArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Sale')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
      };
      return await this.prisma.sale.aggregate(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.sale.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}