import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { SaleCreateInput } from '../../generated/prisma-nestjs-graphql/sale/sale-create.input';
import { Sale } from '../../generated/prisma-nestjs-graphql/sale/sale.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Sale)
export class CreateSaleResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(CreateSaleResolver.name);
  }

  @Mutation(returns => Sale)
  async createSale(
    @Context() context,
    @Args('saleCreateInput', { type: () => SaleCreateInput }) saleCreateInput: SaleCreateInput
  ) {
    try {
      if (!context.ability.can('create', 'Sale')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const saleQuery = {
        data: {
          ...saleCreateInput,
          createdBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          createdBy: true,
          saleDetail: {
            include: {
              product: true,
            }, 
          },
        },
      };

      return await this.prisma.sale.create(saleQuery);
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.sale.cannot.create', { lang: context.language }),
        error,
      );
    }
  }
}