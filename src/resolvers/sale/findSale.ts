import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { FindUniqueSaleArgs } from '../../generated/prisma-nestjs-graphql/sale/find-unique-sale.args';
import { Sale } from '../../generated/prisma-nestjs-graphql/sale/sale.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Sale)
export class FindSaleResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindSaleResolver.name);
  }

  @Query(returns => Sale)
  async findSale(
    @Context() context,
    @Args('args', { type: () => FindUniqueSaleArgs }) args: FindUniqueSaleArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Sale')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          saleDetail: true,
        },
      };
      const result = await this.prisma.sale.findUnique(query);
      if (!result) throw new ApiException('Not found', HttpStatus.NOT_FOUND, 'errors.not.found', {});
      
      return result;
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.sale.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}