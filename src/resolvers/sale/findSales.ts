import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { Sale } from '../../generated/prisma-nestjs-graphql/sale/sale.model';
import { FindManySaleArgs } from '../../generated/prisma-nestjs-graphql/sale/find-many-sale.args';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Sale)
export class FindSalesResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindSalesResolver.name);
  }

  @Query(returns => [Sale])
  async findSales(
    @Context() context,
    @Args('args', { type: () => FindManySaleArgs }) args: FindManySaleArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Sale')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          saleDetail: {
            include: {
              product: true,
            },
          },
          customer: true,
        },
      };

      return await this.prisma.sale.findMany(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.sale.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}