import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { SaleGroupByArgs } from '../../generated/prisma-nestjs-graphql/sale/sale-group-by.args';
import { SaleGroupBy } from '../../generated/prisma-nestjs-graphql/sale/sale-group-by.output';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => SaleGroupBy)
export class GroupBySalesResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(GroupBySalesResolver.name);
  }

  @Query(returns => [SaleGroupBy])
  async groupBySales(
    @Context() context,
    @Args('args', { type: () => SaleGroupByArgs }) args: SaleGroupByArgs,
  ) {
    try {
      if (!context.ability.can('read', 'Sale')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
      };
      // @ts-ignore
      return await this.prisma.sale.groupBy(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.sale.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}