import { Args, Context, Int, Mutation, Resolver } from '@nestjs/graphql';
import { SaleUpdateInput } from '../../generated/prisma-nestjs-graphql/sale/sale-update.input';
import { Sale } from '../../generated/prisma-nestjs-graphql/sale/sale.model';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => Sale)
export class UpdateSaleResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(UpdateSaleResolver.name);
  }

  @Mutation(returns => Sale)
  async updateSale(
    @Context() context,
    @Args('id', { type: () => Int }) id: number,
    @Args('body', { type: () => SaleUpdateInput }) body: SaleUpdateInput,
  ) {
    try {
      if (!context.ability.can('update', 'Sale')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );
      
      const query = {
        where: {
          id,
        },
        data: {
          ...body,
          updatedBy: {
            connect: {
              id: context.userId,
            },
          },
        },
        include: {
          updatedBy: true,
        },
      };

      return await this.prisma.sale.update(query)
    } catch (error) {
      if (error.code === 'P2002') throw this.logger.error(
        await this.i18n.translate('errors.data.repeated', { lang: context.language }),
        error,
      );
      throw this.logger.error(
        await this.i18n.translate('errors.sale.cannot.update', { lang: context.language }),
        error,
      );
    }
  }
}