import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { SaleDetail } from '../../generated/prisma-nestjs-graphql/sale-detail/sale-detail.model';
import { FindManySaleDetailArgs } from '../../generated/prisma-nestjs-graphql/sale-detail/find-many-sale-detail.args';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => SaleDetail)
export class FindSaleDetailResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(FindSaleDetailResolver.name);
  }

  @Query(returns => [SaleDetail])
  async findSaleDetail(
    @Context() context,
    @Args('args', { type: () => FindManySaleDetailArgs }) args: FindManySaleDetailArgs,
  ) {
    try {
      if (!context.ability.can('read', 'SaleDetail')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
        include: {
          createdBy: true,
          updatedBy: true,
          sale: true,
          product: true,
        },
      };

      return await this.prisma.saleDetail.findMany(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.sale.detail.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}