import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { SaleDetailGroupByArgs } from '../../generated/prisma-nestjs-graphql/sale-detail/sale-detail-group-by.args';
import { SaleDetailGroupBy } from '../../generated/prisma-nestjs-graphql/sale-detail/sale-detail-group-by.output';
import { HttpStatus } from '@nestjs/common';
import { ApiException } from '../../common/exception/api.exception';
import { PrismaService } from '../../common/prisma/prisma.service';
import { LoggerService } from '../../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';

@Resolver(of => SaleDetailGroupBy)
export class GroupBySaleDetailResolver {

  constructor(
    private prisma: PrismaService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(GroupBySaleDetailResolver.name);
  }

  @Query(returns => [SaleDetailGroupBy])
  async groupBySaleDetail(
    @Context() context,
    @Args('args', { type: () => SaleDetailGroupByArgs }) args: SaleDetailGroupByArgs,
  ) {
    try {
      if (!context.ability.can('read', 'SaleDetail')) throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        await this.i18n.translate('errors.forbidden', { lang: context.language }),
        {},
      );

      const query = {
        ...args,
      };
      // @ts-ignore
      return await this.prisma.saleDetail.groupBy(query);
    } catch (error) {
      throw this.logger.error(
        await this.i18n.translate('errors.sale.detail.cannot.find', { lang: context.language }),
        error,
      );
    }
  }
}